"""
-class definition

:date: 17.04.2013
:author: ggeorges
"""

# std-lib libraries
import weakref

# own resources
from simulation.equationgraph.common import GraphBase
from simulation.equationgraph.equation import EquationNode
from simulation.equationgraph.localsymbol import LocalSymbol
from simulation.equationgraph.supersymbol import SuperSymbol
from simulation.equationgraph.iterator import iter_graph
from simulation.equationgraph.conditioner import condition


# logging
import logging
logger = logging.getLogger(__name__)


class EquationGraph(GraphBase):

    """ Parent to all EquationNode and SumperSymbol objects

    Every equation-graph is hosted by an EquationGraph object. It owns the
    references to the constituting elements (equations and super-symbols -
    cross-references are all weakrefs) and provides certain management
    functionality, in particular graph creation and tracing of symbols and
    edges linking elements.

    The constructor only initializes the object; objects need to be inserted
    'manually' using this class's API.
    """

    def __init__(self):
        """ Creates a new EquationGraph - no arguments """

        # init defaults
        self.super_symbols = set()
        self.nodes = set()

    def add_equation(self, expression, *symbols):
        """ Create and add a new EquationNode to this EquationGraph

        :param:expression: the mathematical expression (either textual or a
            CAS object) - anything the CAS_interface can parse
        :param:symbol: any subsequent argument has to be a LocalSymbol instance
            representing one of the symbols used in the above expression
        :returns: the newly created EquationNode object """

        if len(symbols) == 1 and hasattr(symbols[0], '__iter__'):
            symbols = symbols[0]
        eq = EquationNode(self, expression, symbols)
        self.nodes.add(eq)
        return eq

    def add_supersymbol(self, differential_of=None, modulated=False,
                        monitored=False):
        """ Creates and adds a new SuperSymbol to this EquationGraph

        :param:SuperSymbol:differential_of: if a SuperSymbol instance is given
            along here, the newly created instance will be marked as time-
            derivative of 'differential_of'
        :param:bool:modulated: overwrites the 'modulated'-state of the symbol;
            if true, the symbol is assumed as being externally defined
            (typically this would be used for control signals)
        :returns: the newly created SuperSymbol object """

        if differential_of is None:
            sym = SuperSymbol(self, modulated=modulated, monitored=monitored)
        else:
            sym = SuperSymbol(self, differential_of=differential_of)
        self.super_symbols.add(sym)
        return sym

    def add_localsymbol(self, name, negative=False, dependent=False,
                        super_symbol=None, differential_order=0):
        """ Creates a new LocalSymbol and returns it

        This is a convenience method, so that one does not have to import the
        :py:class:`LocalSymbol` class when having a graph object. It merely
        calls the equation, specifies itself as graph and returns the symbol.
        The association to an equation must occur through
        :py:meth:`EquationNode.add_symbol`. """

        return LocalSymbol(self, name, negative=negative, dependent=dependent,
                           super_symbol=super_symbol,
                           differential_order=differential_order)

    def get_node_associated_to_symbol(self, symbol, super_symbol=False):
        """ Returns the node connected to the provided 'symbol'

        This method retrieves the `EquationNode` or `SuperSymbol` instance
        linked to the provided `LocalSymbol` instance 'symbol'. If there is none
        , a :py:exc:`KeyError` will be thrown.

        This became necessary, as to overcome the problem of circular
        referencing, :py:class:`LocalSymbol` instances no longer have a link
        back to their respective equation/super-symbol, but instead use their
        link back to the :py:class:`EquationGrah` instance to find the node,
         whose 'symbols'-set features them.

        :param:LocalSymbol:symbol: the symbol to look for
        :param:bool:super_symbol: if True (default: False) the method will
            return the :py:class:`SuperSymbol`instead of the equation that
            symbol is related to.
        :returns: :py:class:`SuperSymbol` or :py:class:`EquationNode`
        """

        nodes = self.super_symbols if super_symbol else self.nodes
        for node in nodes:
            if symbol in node.symbols:
                return node
        else:
            raise KeyError()

    def get_differential_of_super_symbol(self, symbol, create_if_missing=False):
        """ Returns the differential super-symbol of the provided super-symbol

        :param:SuperSymbol:symbol: the super-symbol to find the derivative for
        :param:bool:create_if_missing: if True (default: False), a differential
            super-symbol is created using
            :py:meth:`EquationGraph.add_supersymbol` if none exists yet.
        :returns: :py:class:`SuperSymbol` or None if no differential was set and
            :arg:`create_if_missing` is set to False """

        symbols = (s for s in self.super_symbols)
        symbols = (s for s in symbols if s.is_differential())
        symbols = (s for s in symbols if s.integral == symbol)
        try:
            return next(symbols)
        except StopIteration:
            if create_if_missing:
                return self.add_supersymbol(differential_of=symbols)
            return None

    def iter_sources(self):
        """ Iterator to the source super-symbols of the associated graph """
        return (s for s in self.super_symbols if s.is_source() and s.symbols)

    def has_undetermined_causalities(self):
        """ Returns if this graph has one or more LocalSymbols with undetermined
            causalties """
        symbols = (s for n in self.nodes for s in n.symbols)
        return any(s.dependent is None for s in symbols)

    def is_conditioned(self):
        """ Returns True if all causal links are properly established """

        # check the solution
        for node in list(self.nodes) + list(self.super_symbols):

            # there mustn't be any undetermined symbols
            if any(s.dependent is None for s in node.symbols):
                return False

            # there may be at most one dependent symbol per node
            n_dep = 0 if node.is_super_symbol() and node.is_source() else 1
            if sum(s.dependent for s in node.symbols) != n_dep:
                return False

        return True

    def condition(self):
        """ Causally conditions the graph """
        condition(self)

    def get_simulation_model(self):
        """ Returns the :py:class:`SimulationModel` object of this graph """
        return SimulationModel(self)
