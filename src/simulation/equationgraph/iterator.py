def iter_graph(starting_with, depth_first=False, include_loops=False,
               loops=None, none_is_skip=False):
    """ Iterates over a graph, given the starting node 'starting-with'

    This versatile generator allows to go through the graph using either the
    depth-first or breadth-first pattern. However, it doesn't yield nodes,
    but instead a tuple of neighbouring nodes, to which we could go.
    The calling instance should pick one, process as required and then
    return it to the generator using Python's :meth:`send` mechanism. The
    generator will then continue with that particular node. If a value
    outside of the provided alternatives (including None) is provided, the
    first item in the order of graph creation will be selected.

    :param:GraphElement:starting_with: the starting node - be careful, nodes
        within loops can cause trouble.
    :param:bool:depth_first: when True (default: False), switches to the
        depth-first pattern instead of the default breadth-first pattern.
    :param:bool:include_loops: if True, the yielded neighbouring nodes also
        include already discovered nodes. This can be used to track loops in
        a graph. The usage pattern remains unchanged, also in that case:
        simply return the 'looping' node; the method will ignore it and
        move on to the next nodes.
    :param:list:loops: kind of a hack, allows to access the 'loops'-array,
        storing tuples of elements building a loop. Simply pass a list
        (declared before the function call!). As this only makes sense with
        loops enabled and depth_first search pattern, sending a list through
        loops sets depth_first=True and include_loops=True. While this could
        also be implemented by tracing the yielded options, it would be
        inefficient as basically, the stack needs to be recreated. """

    # initialize stack and tracking arrays
    stack = [starting_with]
    breadcrumbs = set()
    discovered = set([starting_with])
    explored = set()

    # loop-tracing works only with DFS and include_loops enabled
    if hasattr(loops, 'append'):
        depth_first = True
        include_loops = True
    else:
        loops = None

    # now go and loop
    while stack:

        # work the stack
        node = stack[-1] if depth_first else stack[0]

        # get next nodes over unvisited edges (=LocalSymbol)
        options = (s for s in node.symbols if not s in breadcrumbs)
        if node.is_equation():
            options = (s.super_symbol for s in options)
        else:
            options = (s.equation for s in options)

        # remove explored, and discovered unless 'include_loops'
        options = (s for s in options if s not in explored)
        if not include_loops:
            options = (s for s in options if s not in discovered)
        options = list(options)

        while options:

            # send options back and see what the user selects
            option = yield (node, tuple(options))
            try:
                options.remove(option)
            except ValueError:
                if stack != [starting_with] and none_is_skip and option is None:

                    # do not go any further - equal to: no options
                    options = []
                    continue
                else:
                    option = options.pop(0)

            # add to stack and put a breadcrumb
            if option in discovered:
                if loops is not None and option in stack:
                    loop = tuple(stack[stack.index(option):])
                    loops.append(loop)
            else:
                stack.append(option)
                discovered.add(option)

            breadcrumbs.add(option.get_connecting_symbol(node))

            if depth_first:
                break

        else:
            # remove from stack and mark as explored
            explored.add(node)
            stack.remove(node)
