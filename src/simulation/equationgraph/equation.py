from simulation.equationgraph.elements import GraphNode

# need the CAS here
import core.cas.sympy_interface
_cas = core.cas.sympy_interface.interface()


class EquationNode(GraphNode):

    _expression = None
    symbols = None

    def __init__(self, graph, expression, symbols=None):

        # set mandatory args
        super(EquationNode, self).__init__(graph)
        self.expression = expression

        # get all symbols
        if not hasattr(symbols, '__iter__'):
            return
        for symbol in symbols:
            self.add_localsymbol(symbol)

    def is_equation(self):
        return True

    def __str__(self):
        return str(self.universal_expression)

    @property
    def expression(self):
        return self._expression

    @expression.setter
    def expression(self, value):
        if not hasattr(value, 'subs'):
            value = _cas.parse_toEquation(value)
        self._expression = value

    @property
    def universal_expression(self):

        # todo: make derivative go before its integral symbol
        def sort_subs(subs):
            subs_ = []
            while subs:
                for s in subs[1:]:
                    if s[0] == subs[0][1] or (subs[0][0] in s[0]):
                        idx = subs.index(s)
                        subs[0], subs[idx] = (subs[idx], subs[0])
                        break
                else:
                    subs_.append(subs[0])
                    subs = subs[1:]
            return subs_

        # get symbols, and rearrange them if necessary
        orig, rep = zip(*(s.get_universal_subs() for s in self.symbols))
        subs = zip(orig, rep)
        subs = sort_subs(subs)

        # make sure there are no conflicts -> don't replace the replacement
        # problems with derivatives are caught by using string-compare of the
        # original, so that Derivative(s0(t), t) is also matched
        orig, rep = zip(*subs)
        for r in rep:
            for o in orig:
                if r not in o:
                    continue
                if rep.index(r) >= orig.index(o):
                    continue
                self.log_error("Naming error {}", str(r))
                raise RuntimeError("naming conflict")

        subs = ((_cas.parse_toExpression(r) for r in s) for s in subs)
        return self.expression.subs(subs)

    def resolve(self):
        expr = self.universal_expression
        if not hasattr(self.get_dependent_symbol(), 'super_symbol'):
            raise RuntimeError()
        symbol = self.get_dependent_symbol().super_symbol
        sol = _cas.solve(expr, str(symbol) + "(t)")
        if not sol and len(sol) != 1:
            raise RuntimeError()
        return sol[0]

    def has_derivative(self):
        return any(s.is_differential() for s in self.symbols)

    def add_localsymbol(self, symbol, negative=False, dependent=False,
                        super_symbol=None, differential_order=0):
        if not hasattr(symbol, 'equation'):
            add = self.graph.add_localsymbol
            symbol = add(str(symbol), negative=negative,
                         dependent=dependent, super_symbol=super_symbol,
                         differential_order=differential_order)
        if symbol.equation is not None:
            raise RuntimeError("symbol already associated")
        self.symbols.add(symbol)

        return symbol
