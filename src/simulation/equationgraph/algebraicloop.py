import logging
logger = logging.getLogger(__name__)
from .iterator import iter_graph
import core.cas.sympy_interface
_cas = core.cas.sympy_interface.interface()


def remove_loops(graph):
    buster = LoopBuster(graph)
    buster.remove_loops()


class LoopBuster(object):

    def __init__(self, graph):
        self.loops = self._make_loop_array(graph)
        self.graph = graph

    def remove_loops(self):
        for loop in self.loops:
            logger.info("removing algebraic loop: {}".format(str(loop)))
            loop.resolve(self.graph)

    def _make_loop_array(self, graph):

        known_cycles = []
        for cycle in self._iter_cycles(graph):

            # a source is enough to skip a cycle
            if cycle.has_sources():
                continue

            # if there are external, dependent equations, they may yet fall
            # into another cycle
            # merge interleaved cycles
            for known_cycle in known_cycles:
                if cycle.has_intersection_with(known_cycle):
                    known_cycle.update(cycle)
                    break
            else:
                known_cycles.append(cycle)

        # now that we have all the information, real causal cycles can be
        # identified as those that have no external equations NOW (where
        # cycles are complete)

        if not known_cycles or all(c.has_external_dependent_equations()
                                   for c in known_cycles):
            logger.info("no algebraic loops detected")

        return [c for c in known_cycles
                if not c.has_external_dependent_equations()]

    def _iter_cycles(self, graph):

        loops = []

        for source in graph.iter_sources():

            visited_local = [source]
            visited = set()

            # get loops using the 'loop' array by-ref technique
            # TODO: implement using a local stack or something
            graph_iter = iter_graph(source, depth_first=True,
                                    include_loops=True, none_is_skip=True)
            current_node, options = graph_iter.next()

            while options:

                if visited_local[-1] is not current_node:
                    if current_node in visited_local:
                        idx = visited_local.index(current_node)
                        visited_local = visited_local[:idx + 1]
                    else:
                        visited_local = []

                for next_node in options:
                    symbol = current_node.get_connecting_symbol(next_node)
                    if symbol in visited:
                        continue
                    if current_node.is_super_symbol():
                        if not symbol.dependent:
                            break
                    elif current_node.is_equation():
                        if symbol.dependent:
                            break
                else:
                    if current_node.is_source():
                        next_node = iter(current_node.symbols).next().equation
                        symbol = current_node.get_connecting_symbol(next_node)
                    else:
                        next_node = None
                        symbol = None

                if symbol is not None:
                    visited.add(symbol)

                if next_node is not None:
                    if next_node in visited_local:
                        loop = visited_local[visited_local.index(next_node):]
                        yield GraphLoop(loop)

                    visited_local.append(next_node)

                try:
                    current_node, options = graph_iter.send(next_node)
                except StopIteration:
                    break


class GraphLoop(set):

    def __init__(self, nodes):
        super(GraphLoop, self).__init__(nodes)

    def __str__(self):
        return " -> ".join(str(s) for s in self)

    def has_sources(self):
        nodes = (n for n in self if n.is_super_symbol())
        return any(n.is_source() for n in nodes)

    def has_external_dependent_equations(self):

        nodes = (s for s in self if s.is_super_symbol())
        for node in nodes:

            # got a symbol defined outside the loop? not a causal loop
            symbol = node.get_dependent_symbol()
            if symbol is None:
                raise RuntimeError("graph not conditioned")
            if symbol.equation not in self:
                return True

        # seems like a causal loop
        return False

    def has_intersection_with(self, loop):
        if not hasattr(loop, '__contains__'):
            raise TypeError()

        # cycles intersect if they share more than one node
        # TODO: could be a wrong assumption
        return sum(n in loop for n in self) > 1

    def iter_inputs(self):

        # any super-symbol not within the loop, that is entering any of the
        # loop's equation through an independent symbol
        symbols = (s for s in self if s.is_equation())
        symbols = (s for n in symbols for s in n.iter_independent_symbols())
        symbols = (s.super_symbol for s in symbols)
        symbols = (s for s in symbols if not s in self)
        return symbols

    def iter_outputs(self):

        # any super-symbol within the loop that enters an external equation
        # as an independent quantity
        symbols = (s for s in self if s.is_super_symbol())
        for symbol in symbols:

            # sinks must be resolved
            if symbol.is_sink():
                yield symbol
                continue

            # any symbol used outside must be resolved
            syms = symbol.iter_independent_symbols()
            if any(s.equation not in self for s in syms):
                yield symbol

    def resolve(self, graph):

        # some required local copies
        outgoing = set(self.iter_outputs())
        incoming = set(self.iter_inputs())
        expressions = [s.universal_expression for s in self if s.is_equation()]

        # consistency checking
        if len(expressions) < len(outgoing):
            raise EquationGraphError("can't resolve algebraic loop")

        # eliminate non in/outgoing symbols
        useless = (s for s in self if s.is_super_symbol())
        useless = (s for s in useless if not s in incoming)
        useless = set(s for s in useless if not s in outgoing)

        # eliminate useless, then solve for outgoing
        x = ["{}(t)".format(str(s)) for s in useless]
        subs, expressions = _cas.eliminate(expressions, x)
        x = ["{}(t)".format(str(s)) for s in outgoing]
        expressions = _cas.solve_equations(expressions, x)

        # create new equation-nodes, mark its dependent symbol as such
        # and finally, link it instead of the original nodes
        # TODO: differentials are probably mishandled
        remaining = set()
        symbols = incoming
        symbols.update(outgoing)
        for symbol, expression in expressions.iteritems():
            symbol = symbol.replace("(t)", "")
            eq = graph.add_equation(expression)
            syms = _cas.detectSymbols(expression, symbols, omit_constants=True)
            for sym in syms:
                remaining.add(sym)
                eq.add_localsymbol(sym.universal_name,
                                   super_symbol=sym,
                                   dependent=(symbol == sym.universal_name))

        # kill everything that's not remaining
        for node in self:
            if node in remaining:
                continue
            graph.super_symbols.discard(node)
            graph.nodes.discard(node)

        # kill orphaned nodes explicitly (necessary if there are ext. refs)
        for node in remaining:
            node.kill_orphaned_symbols()
