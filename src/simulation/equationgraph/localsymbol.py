from simulation.equationgraph.elements import GraphElement


class LocalSymbol(GraphElement):

    """ Represents symbols as used within the associated equation

    The idea: equations use symbols, as defined
    through the super - symbols. The local - symbol objects are merely pointers,
    indicating which super - symbol is used in the associated equation. Beyond
    this crucial aspect, it determines how the super - symbol is being used:
    for instance whether or not it is the dependent variable of the associated
    equation, or whether its sign has to be inverted to match the local sign
    convention. """

    def __init__(self, graph, name, negative=False, dependent=False,
                 super_symbol=None, differential_order=0):
        """ Initializes a LocalSymbol definition

        :param:str:name: the name by which the symbol is locally known(in the
            actual mathematical expression)
        :param:bool:negative: if True, then the sign of the linked super - symbol
            is inverted(by default(negative=False), the assumption is that
            positive power means energy flows in)
        :param:bool:dependent: if True, determines this symbol as the dependent
            quantity of the equation. Remember, that both equations and
            super - symbols can never be linked to more than one dependent symbol.
        :param:bool:derivative: f True, this means that this particular symbol
            is used as a time - derivative within the associated equation(if a
            symbol is used both in its differential and integral form, then
            have to be two: py: class: `LocalSymbol` instances)
        :param:EquationNode:eq: optional parameter, that allows to associate a
            symbol to an equation at initialization
        :param:SuperSymbol:super_symbol:optional parameter, that when set
            links the newly created LocalSymbol to the porvided SuperSymbol
        """

        # initialize mandatory variables
        super(LocalSymbol, self).__init__(graph)
        self._name = name

        # set instance variables so that prop-methods don't have to
        self._negative = None
        self._dependent = None
        if super_symbol.is_differential() and differential_order == 0:
            differential_order = 1
            self.log_warn("set differential_order = 1 on {}".format(name))
        self._derivative = int(differential_order)

        # overwrite using setters
        self.negative = negative
        self.dependent = dependent
        if not super_symbol is None:
            self.super_symbol = super_symbol

    def __str__(self):
        return self.name

    @property
    def name(self):
        """ The name by which the super - symbol is known locally (in the eq.) """
        return self._name

    @name.setter
    def name(self, name):
        raise RuntimeError("symbols may not be renamed")

    @property
    def negative(self):
        """ Whether or not the sign of the super - symbol has to be inverted """
        return self._negative

    @negative.setter
    def negative(self, value):
        self._negative = bool(value)

    @property
    def dependent(self):
        """ Whether or not this is the dependent variable of the equation """
        return self._dependent

    @dependent.setter
    def dependent(self, value):
        if not value is None:
            value = bool(value)
        self._dependent = value

    @property
    def equation(self):
        """ The associated equation """
        try:
            get_node = self.graph.get_node_associated_to_symbol
            return get_node(self, super_symbol=False)
        except KeyError:
            return None

    @equation.setter
    def equation(self, equation):
        """ Links a symbol to an equation

        This method is not meant to be called by the user; instead it allows
        postponing the equation - symbol association until after symbol creation,
        so that symbols instances may be created before creating the equation,
        allowing them to be piped into :py:meth:`EquationGraph.add_equation`
        as arguments.
        """

        if not hasattr(equation, 'add_symbol'):
            raise TypeError()
        equation.add_symbol(self)

    @property
    def super_symbol(self):
        """ The represented SuperSymbol """
        try:
            get_node = self.graph.get_node_associated_to_symbol
            return get_node(self, super_symbol=True)
        except KeyError:
            return None

    @super_symbol.setter
    def super_symbol(self, super_symbol):
        """ Sets this symbol's super - symbol.

        This can be used to link a super - symbol against a local symbol; note
        that technically, there is no reference back to the super - symbol though.
        :py:meth:`EquationGraph.get_node_from` is used to retrieve the
        corresponding super - symbol. Actual linking is done within the
        super - symbol's 'symbols' array; the setter merely defers to that. """

        if (not hasattr(super_symbol, 'is_differential')
                or not hasattr(super_symbol, 'register_local_symbol')):
            raise TypeError()

        if (self.is_differential() != super_symbol.is_differential()):
            raise ValueError("integral/differential super-symbol/symbol mixup")
        super_symbol.register_local_symbol(self)

    @super_symbol.deleter
    def super_symbol(self):
        """ Resets the symbol's association to a SuperSymbol """

        if not hasattr(self.super_symbol, 'forget_local_symbol'):
            return
        self.super_symbol.forget_local_symbol(symbol)

    def is_dependent(self):
        """ Returns True if this is a dependent variable

        Using this method is exactly the same as using provided property
        (attribute: py: attr: `dependent`), except that if dependence was not
        explicitly imposed(: py: attr: `dependent` is None) this will raise an
        :py:exc:`UndeterminedCausalityError` instead of returning None """
        raise NotImplementedError()
        if self.dependent is None:
            raise UndeterminedCausalityError()
        return self.dependent

    @property
    def differential_order(self):
        """ E.g. differential_order=1 indicates the first derivative """
        return self._derivative

    def is_differential(self):
        """ Returns True if this symbol is used as a time - derivative

        This returns exactly the same value as accesing the
        :py:attr:`derivative` attribute and is only included for consistency
        with the above :py:meth:`is_dependent` command. """

        return self.differential_order > 0

    def get_universal_subs(self):
        name = self.name + "(t)"
        if self.is_differential():
            name += ".diff(t)" * self.differential_order
        return (name, self.super_symbol.universal_name + "(t)")
