from core.common import LascavesimException, LascavesimBase


class EquationGraphError(LascavesimException):

    """ There was a problem with the graph """
    pass


class ParallelGraphsError(EquationGraphError):
    pass


class UndeterminedCausalityError(EquationGraphError):

    """ Indicates that some causality has not been defined """
    pass


class ConflictingCausalitiesError(EquationGraphError):

    def __init__(self, context, *args):
        self.context = context
        super(ConflictingCausalitiesError, self).__init__(*args)


class GraphBase(LascavesimBase):
    pass
