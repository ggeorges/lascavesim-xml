import logging
logger = logging.getLogger(__name__)
from core.common import LascavesimException, LascavesimBase
from iterator import iter_graph
import simulation.equationgraph
import core.cas.sympy_interface
_cas = core.cas.sympy_interface.interface()
import collections


class CantConditionGraphError(LascavesimException):

    """ Risen when condition fails for a structural reason """
    pass


class ConflictingCausalitiesError(LascavesimException):

    def __init__(self, context):
        self.context = context


def condition(graph, max_iter=0):

    clean_graph(graph)

    # create the initial stack
    symbols = set(s for n in graph.nodes for s in n.symbols)
    stack = dict((s, s.dependent) for s in symbols)

    # pick a source
    sources = set(s for s in graph.super_symbols if s.is_source() and s.symbols)
    if not sources:
        raise CantConditionGraphError("graph has no sources")
    for source in sources:
        if not source.is_differential():
            break

    # enter the master-conditioner
    master = Conditioner(source, stack)
    try:
        solution = master.process()
    except ConflictingCausalitiesError as e:
        logger.error("casualties could not be determined because of "
                     + str(e.context))
        for symbol, causality in master.stack.iteritems():
            symbol.dependent = causality
        raise CantConditionGraphError()

    # apply the derived solution
    for symbol, causality in solution.iteritems():
        symbol.dependent = causality

    # just to be certain, let's check again
    if not graph.is_conditioned():
        raise CantConditionGraphError()


class Conditioner(object):

    def __init__(self, source, stack):
        self.stack = stack
        self.alternative_routes = []
        self.init_iterator(source)

    def init_iterator(self, source):

        # first iteration must preceding the loop
        self.iterator = iter_graph(source, depth_first=False,
                                   include_loops=True)
        try:
            self.current_node, self.options = self.iterator.next()
        except StopIteration:
            logger.error("source {} leads nowhere".format(str(source)))
            raise RuntimeError()

        # nowhere to go?
        if not self.options:
            raise NotImplementedError("this should not happen actually")

    def process(self):

        while True:

            a = str(self.current_node)
            b = ", ".join(str(s) for s in self.options)
            logger.info("iterator {} -> {}".format(a, b))

            # in breadth-first pattern, we get to order the progression, i.e.
            # we pick out the next node within one level, until there are no
            # more nodes at that level; the subsequent depth progression will
            # occur in the order we sent the proposed options back to the
            # iterator; as edges are visited only once, it is enough to look at
            # them each at a time
            # get the next node (see function docs for details)
            next_node = self.chose_next_node()

            # do that once
            symbol = next_node.get_connecting_symbol(self.current_node)

            # infer the causality of the corresponding symbol; when not possible
            # we are at a cross-roads -> assume that next_node is the dependent
            # symbol at the current equation
            if not self.could_adjust_causality(next_node):

                # if there are other choices, keep a record
                gcs = self.current_node.get_connecting_symbol
                alt = (o for o in self.options if o != next_node)
                alt = (o for o in alt if self.stack[gcs(o)] is None)
                alt = list(DifferentRoute(self, o) for o in alt)
                self.alternative_routes += alt

                # take the proposed route (from now on, we can't come back here)
                self.adjust_symbol_causality(symbol, True)

            # now, make sure the move was legal
            assert self.stack[symbol] is not None

            # test at both ends to see if there are errors -> raise exception
            try:
                for node in (self.current_node, next_node):

                    # the required number of dependent symbols depends on
                    # whether a corresponding super-symbol is a source or not
                    n_dep_max = 0 if node.is_source() else 1

                    # the effective number of dependent symbols must not be >
                    n_dep = sum(bool(self.stack[s]) for s in node.symbols)
                    if n_dep > n_dep_max:
                        logger.info("{} has {} > {} dependent symbols"
                                    .format(str(node), n_dep, n_dep_max))
                        raise ConflictingCausalitiesError(node)

                    # if there are no more undefined causalities, there have to be
                    # exactly n_dep_max dependent symbols
                    partial = any(self.stack[s] is None for s in node.symbols)
                    if not partial and n_dep != n_dep_max:
                        logger.info("{} has {} != {} dependent symbols"
                                    .format(str(node), n_dep, n_dep_max))
                        raise ConflictingCausalitiesError(node)

            except ConflictingCausalitiesError as e:
                # so we only arrive here when 'break' in the above for-loop was hit,
                # i.e. the move we just undertook is illegal.  In that case,
                # the only option left is to try out alternative route (the route
                # taken is a dead-end). Alternative routes are processed until
                # either one converges, or there are none left. In that case an
                # exception is thrown, causing the return to the calling entity
                # (which may be another conditioner, checking us out as alternative
                # route, or the top-level condition function, in which case the
                # graph can't be unconditioned)
                for route in self.alternative_routes:
                    logger.info("going with alternative route")
                    try:
                        return route.process()
                    except ConflictingCausalitiesError:
                        logger.info("alternative route failed")
                        continue

                # nothing worked; re-raise the exception to get a context
                logger.info("no more alternatives to try")
                raise e

            else:
                # no 'breaks' in the for-loop, so the move was allowed; now we
                # try to continue with the iterator which either works or throws
                # 'StopIteration' when we're through the graph
                try:
                    ans = self.iterator.send(next_node)
                except StopIteration:
                    return self.stack

                self.current_node, self.options = ans
                continue

    def check_nodes(self, node):
        # the required number of dependent symbols depends on whether a
        # corresponding super-symbol is a source or not
        n_dep_max = 0 if node.is_source() else 1

        # the effective number of dependent symbols must not be exceeded
        n_dep = sum(bool(self.stack[s]) for s in node.symbols)
        if n_dep > n_dep_max:
            return False

        # if there are no more undefined causalities, there have to be
        # exactly n_dep_max dependent symbols
        fully_defined = any(self.stack[s] is None for s in node.symbols)
        if fully_defined and n_dep != n_dep_max:
            return False

        return True

    def could_adjust_causality(self, next_node):

        # make sure causality needs adjusting
        symbol = next_node.get_connecting_symbol(self.current_node)
        if self.stack[symbol] is not None:
            return True

        # if it's a source, 'symbol' is independent
        if self.current_node.is_source():
            self.adjust_symbol_causality(symbol, False)
            return True

        # if it's got a dependent symbol (which is certainly not 'symbol' which
        # is known to be undetermined), 'symbol' is independent
        if any(self.stack[s] for s in self.current_node.symbols):
            self.adjust_symbol_causality(symbol, False)
            return True

        # if we're the only undetermined symbol in an otherwise independent set,
        # (or if we're really the only symbol) then we are the dependent symbol
        if all(self.stack[s] is False for s in self.current_node.symbols
               if s != symbol):
            self.adjust_symbol_causality(symbol, True)
            return True

        # none of the matching rules apply -> there's a choice here
        return False

    def chose_next_node(self):

        if self.current_node.is_super_symbol():
            return self.chose_from_super_symbol()
        elif self.current_node.is_equation():
            return self.chose_from_equation()
        raise RuntimeError()

    def chose_from_super_symbol(self):
        """ Select the equation to visit next """

        # order by number of connected undetermined symbols -> this may mix
        # dependent, independent and undetermined connections together
        count_indep = lambda o: sum(self.stack[s] is None for s in o.symbols)
        independent = min(self.options, key=count_indep)
        return independent

    def chose_from_equation(self):
        """ Select the super-symbol to move to next """

        symbols = list(o.get_connecting_symbol(self.current_node)
                       for o in self.options)

        # always give priority to the dependent symbol
        try:
            dependent = (s.super_symbol for s in symbols if self.stack[s])
            return dependent.next()
        except StopIteration:
            pass

        # now, there have to be undetermined symbols or this doesn't work
        undetermined = (s for s in symbols if self.stack[s] is None)
        undetermined = list(s.super_symbol for s in undetermined)

        if undetermined:

            # if there is only one possibility, return immediately to gain speed
            if len(undetermined) == 1:
                return undetermined[0]

            # next order the remaining symbols: if they appear as isolated symbol
            # on the left side of the equation, they get 2 points; 1 for being
            # isolated on the right and one additional point if there is no
            # foreseeable at the immediate neighbour
            isolated = list(self.iter_isolated_symbols())
            max_score = -1
            winning_node = None
            for node in undetermined:
                symbol = self.current_node.get_connecting_symbol(node)
                score = int(symbol in isolated)
                if isolated:
                    score += int(symbol == isolated[0])
                score += int(not any(self.stack[s] for s in node.symbols))
                if score > max_score:
                    max_score = score
                    winning_node = node

            return winning_node

        # now there are only independent symbols remaining
        assert all(self.stack[s] is False for s in symbols)
        return self.options[0]

    def adjust_symbol_causality(self, symbol, causality):
        """ Resets symbol's causality to 'causality' """
        if not hasattr(symbol, 'dependent'):
            raise TypeError()
        self.stack[symbol] = causality

    def iter_isolated_symbols(self):
        symbols = set(o.get_connecting_symbol(self.current_node)
                      for o in self.options)
        return iter_isolated_symbols(self.current_node, symbols)


class DifferentRoute(Conditioner):

    def __init__(self, predecessor, instead_go_with):

        logger.info("alternative route form {} to {}"
                    .format(str(predecessor.current_node),
                            str(instead_go_with)))

        # copy the stack
        stack = dict(predecessor.stack.iteritems())

        # make the new iterator start with the different node
        super(DifferentRoute, self).__init__(instead_go_with, stack)

        # adjust stack so that it includes the decision
        symbols = predecessor.current_node.symbols
        symbols = (s for s in symbols if stack[s] is None)
        for symbol in symbols:
            self.adjust_symbol_causality(symbol, False)
        symbol = instead_go_with.get_connecting_symbol(predecessor.current_node)
        self.adjust_symbol_causality(symbol, True)


def clean_graph(graph, sources=None, sinks=None):
    """ Reset graph to a valid state

    This method sets the symbol-dependence on known sources and sinks and
    resets the dependence property to 'undetermined' (dependent=None) for any
    normal (non-sink, non-source) equation/super-symbol, if it has either more
    than one dependent-symbol or all of them are independent. """

    if sources is None or sinks is None:
        sources, sinks = get_sources_and_sinks(graph)

    # adjust symbols
    for symbol in list(graph.super_symbols) + list(graph.nodes):
        if symbol in sources:
            for s in symbol.symbols:
                s.dependent = False
            continue
        if symbol in sinks:
            for s in symbol.symbols:
                s.dependent = True
            continue

        # reset connections where there is a problem -> either more than one
        # dependent symbol or all, if all are detemrined = not None, where not
        # any node is dependent -> sources and sinks are skipped before
        if (sum(bool(s.dependent) for s in symbol.symbols) > 1 or
            (not any(s.dependent is None for s in symbol.symbols) and
                all(not s.dependent for s in symbol.symbols))):
            for s in symbol.symbols:
                s.dependent = None


def get_sources_and_sinks(graph):
    """ Returns the sources and sinks of a graph (as a tuple)

    This method returns a tuple of two frozensets, where the first one is the
    collection of all sources, and the second the collection of all sinks of the
    provided graph. This method doesn't rely on the LocalSymbol, but directly
    on structural information to derive a LocalSymbol's status. """

        #  get all connected sources
    sources = (s for s in graph.super_symbols if s.is_source())
    sources = (s for s in sources if s.is_modulated()
               or (not s.is_differential() and s.differential is not None))
    sources = frozenset(s for s in sources if s.symbols)
    if not sources:
        raise CantConditionGraphError("graph has no sources")
    logger.debug("sources: " + ", ".join(str(s) for s in sources))

    # enforce causality on single-connection sink terms -> here we assume
    # that anything one-signalled that is not explicitly defined as source
    # (i.e. modulated) is a sink
    sinks = (s for s in graph.super_symbols if len(s.symbols) == 1)
    sinks = (s for s in sinks if not s.is_modulated())
    sinks = (s for s in sinks if not s in sources)
    sinks = (s for s in sinks if not s.is_differential()
             or (s.is_differential() and s.integral in sources))
    sinks = frozenset(sinks)
    logger.debug("sinks: " + ", ".join(str(s) for s in sinks))

    return (sources, sinks)


def iter_isolated_symbols(node, syms):

    def process_args(expression, syms):
        if not hasattr(expression, 'args'):
            raise TypeError()
        for arg in expression.args:
            sym = list(_cas.detectSymbols(arg, syms))
            if sym and len(sym) == 1:
                yield sym.pop()
        raise StopIteration()

    if node.is_equation():
        if not hasattr(node.expression, 'args'):
            raise TypeError()
        for sym in process_args(node.universal_expression, syms):
            yield sym

    if node.is_super_symbol():
        equations = (s.equation for s in node.symbols)
        for eq in equations:
            try:
                for sym in process_args(eq.universal_expression, syms):
                    yield sym

            except StopIteration:
                continue
