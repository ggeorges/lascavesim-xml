# std-lib libraries
import weakref

# own resources
from simulation.equationgraph.common import GraphBase


class GraphElement(GraphBase):

    """ An element that belongs to an EquationGraph

    All elements of an equation-graph share the capability of linking back to
    the hosting :py:class:`EquationGraph` instance. This class provides the
    corresponding interface and transparently implements the reference back
    as a weakref. """

    def __init__(self, graph):
        """ Initializes a graph-element to point back 'graph'

        :param:EquationGraph:graph: the hosting :py:class:`EquationGraph` """
        self._graph = weakref.ref(graph)

    @property
    def graph(self):
        """ Link back to the hosting EquationGraph instance """

        if not hasattr(self._graph, '__call__'):
            raise RuntimeError("orphaned node")
        graph = self._graph()
        if not hasattr(graph, 'get_node_associated_to_symbol'):
            raise RuntimeError("parent node is dead")
        return self._graph()


class GraphNode(GraphElement):

    def __init__(self, graph):
        self.reset_symbols()
        super(GraphNode, self).__init__(graph)

    def reset_symbols(self, symbols=None):
        symbols = [] if not hasattr(symbols, '__iter__') else symbols
        self.symbols = set(symbols)

    def is_super_symbol(self):
        return False

    def is_equation(self):
        return False

    def is_source(self):
        return False

    def is_sink(self):
        return False

    def get_dependent_symbol(self):
        """ Returns the (only allowed) dependent symbol of this node """
        try:
            return (s for s in self.symbols if s.dependent).next()
        except StopIteration:
            return None

    def iter_independent_symbols(self):
        """ Iterator over all non - dependent symbols  within this node"""
        symbols = (s for s in self.symbols if s.dependent is not None)
        return (s for s in symbols if not s.dependent)

    def iter_undetermined_symbols(self):
        """ Iterator over all symbols with undetermined causalities """
        return (s for s in self.symbols if s.dependent is None)

    def _has_no_symbols_with_dependence(self, dependence):
        return not any(s.dependent is dependence for s in self.symbols)

    def has_no_dependent_symbols(self):
        return self._has_no_symbols_with_dependence(True)

    def has_no_independent_symbols(self):
        return self._has_no_symbols_with_dependence(False)

    def has_no_undetermined_symbols(self):
        return self._has_no_symbols_with_dependence(None)

    def get_connecting_symbol(self, node):
        """ Returns the LocalSymbol linking this node (self) to 'node' """
        if not hasattr(node, 'symbols'):
            raise TypeError()

        try:
            return (s for s in self.symbols if s in node.symbols).next()
        except StopIteration:
            return None

    def kill_orphaned_symbols(self):
        self.reset_symbols(s for s in self.symbols if s.equation is not None)
