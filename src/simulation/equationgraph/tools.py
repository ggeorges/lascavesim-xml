def convert_to_networkx_graph(graph, dont_plot=False):
    try:
        import networkx as nx
    except ImportError:
        raise RuntimeError("networkx not installed; function not available")

    # create graph
    G = nx.DiGraph()

    # add nodes and edges
    for node in graph.nodes:
        for symbol in node.symbols:
            a = str(node.universal_expression)
            b = str(symbol.super_symbol)
            if symbol.dependent is None:
                G.add_edge(a, b, weight=1)
                G.add_edge(b, a, weight=1)
                pass
            elif symbol.dependent:
                G.add_edge(a, b, weight=1)
            else:
                G.add_edge(b, a, weight=1)

    if dont_plot:
        return G

    import matplotlib.pyplot as plt
    nx.draw(G)
    plt.show()

    return G
