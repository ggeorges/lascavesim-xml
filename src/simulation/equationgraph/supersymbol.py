from simulation.equationgraph.elements import GraphNode

import weakref


class SuperSymbol(GraphNode):

    def __init__(self, graph, modulated=False, monitored=False,
                 differential_of=None):

        # mandatory arguments
        super(SuperSymbol, self).__init__(graph)

        # optional arguments
        self._modulated = bool(modulated)
        self._monitored = bool(monitored)

        # get the index (simply number of super_symbols)
        self.index = len(self.graph.super_symbols)

        # are we a differential of somebody?
        self._integral = None
        if differential_of is not None:
            if not hasattr(differential_of, 'get_differential'):
                raise TypeError("not a SuperSymbol")
            self._integral = weakref.ref(differential_of)

    def reset_symbols(self, symbols=None):
        symbols = [] if not hasattr(symbols, '__iter__') else symbols
        self.symbols = weakref.WeakSet(symbols)

    def is_super_symbol(self):
        return True

    def __str__(self):
        return self.universal_name

    @property
    def integral(self):
        if not hasattr(self._integral, '__call__'):
            return None
        return self._integral()

    def is_differential(self):
        return self.integral is not None

    def get_differential(self, create_if_missing=True):
        s = self.graph.get_differential_of_super_symbol(self, create_if_missing)
        return s

    @property
    def differential(self):
        return self.get_differential(create_if_missing=False)

    def has_differential(self):
        return self.differential is not None

    def is_modulated(self):
        return self._modulated

    def is_monitored(self):
        return self._monitored

    def register_local_symbol(self, symbol):
        """ Associates a LocalSymbol to this instance """

        # append to local array
        if not hasattr(symbol, 'super_symbol'):
            raise TypeError()
        self.symbols.add(symbol)

    def register_local_symbols(self, *symbols):
        """ Associates multiple LocalSymbol - s to this instance """
        if not symbols:
            return
        for symbol in symbols:
            self.register_local_symbol(symbol)

    def forget_local_symbol(self, symbol):
        """ Disassociates a linked LocalSymbol """

        # some type-safety
        if not hasattr(symbol, 'super_symbol'):
            raise TypeError()

        # remove symbol first, then call 'reset' -> prevents race conditions,
        # well we only come back one, but this method will probably nevery be
        # used anyway
        try:
            self.symbols.remove(symbol)
            if symbol.super_symbol is not None:
                del symbol.super_symbol
        except KeyError:
            return

    def is_source(self):
        """ Returns True if this is a signal source

        Source symbols are symbols "emitting" a signal without receiving one
        (i.e. they are exclusively linked to independent equation symbols). In
        principle the 'is_source' status is determined by looking at the linked
        equation symbols. But as this method is also used for conditioning
        graphs with undetermined causalities, this behavior an be overriden:
            * first of all, if a super - symbol is explicitly marked as modulated
              this method will always return False(see: meth: `is_modulated`)
            * the integral counterpart to differential symbols(i.e. the
              'regular' symbol passed as the 'differential_of' argument to: meth: `add_supersymbol`) is always a source, unless there is an
              explicit connection to a dependent quantity. This is important for
              the case of "overriden states", where two apparently independent
              states are actually one and the same. This typically occurs with
              speed in rigidly coupled mechanical systems and voltage in
              inductance - free circuits."""

        # modulated symbols are always sources
        if self.is_modulated():
            return True

        # if we're the solution of an ODE, we are a source
        if self.has_differential() and self.has_no_dependent_symbols():
            return True

        # if we are a differential, and our integral is overwritten (i.e. there
        # is a dependent symbol attached to it), then we are in fact a source
        if (self.is_differential()
                and not self.integral.has_no_dependent_symbols()):
            return True

        return False

    def is_sink(self):

        # if our integral is a source, then we must be a sink
        if self.is_differential() and self.integral.is_source():
            return True

        # otherwise it's just a matter of connected signals
        return all(not s.dependent for s in self.symbols
                   if s.dependent is not None)

    def has_derivative_source(self):
        """ Returns true if this symbol's source is a derivative

        Each super - symbol has exactly one source symbol(see
        :py:meth:`get_source` for more information). This method checks whether
        the crresponding symbol is a derivative. If the causality of the
        super - symbol is unresolved, a:py:exc:`UndeterminedCausalityError` will
        be thrown. """

        source = self.get_dependent_symbol()
        return source.is_derivative()

    @property
    def universal_name(self):
        return "s{}".format(self.index)
