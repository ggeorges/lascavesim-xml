import weakref
import core.common


class Simulation(core.common.LascavesimBase):

    def __init__(self, graph):

        # init local variables, including anti-windup protection
        known = set(s for s in graph.super_symbols if s.is_source())
        anti_wind_up = [1e6, 1e6, 1e6, 1e6]
        nodes = set(graph.nodes)
        self.steps = list()

        while nodes:

            # find solvable equations and the gained symbols
            computable = list(n for n in nodes
                              if all(s.super_symbol in known
                                     for s in n.iter_independent_symbols()))
            if not computable:
                raise RuntimeError("not enough equations or hidden loop")

            # solve for unknown
            level = SimulationStep(computable)
            self.steps.append(level)

            # adjust tracking arrays
            nodes = set(n for n in nodes if n not in computable)
            symbols = (n.get_dependent_symbol() for n in computable)
            symbols = (s.super_symbol for s in symbols)
            known.update(symbols)

            # anti-windup -> if there was no change in the size of len(nodes)
            # for [len of initial anti_wind_up] cycles, break free
            if anti_wind_up.pop(0) <= len(nodes):
                raise RuntimeError("hidden loop")
            anti_wind_up.append(len(nodes))


class SimulationStep(core.common.LascavesimBase):

    def __init__(self, equation_nodes):
        self.equation_nodes = weakref.WeakSet(equation_nodes)

    def needs(self):
        symbols = set(s.super_symbol for n in self.equation_nodes
                      for s in n.iter_independent_symbols())
        return symbols

    def yields(self):
        symbols = set(n.get_dependent_symbol().super_symbol
                      for n in self.equation_nodes)
        return symbols


class Export2(core.common.LascavesimBase):
    pass


class Export2Python(Export2):

    def __init__(self, model):
        for step in model.steps:
            self.print_export(step)

    def print_export(self, step):
        text = ("{} = {}".format(eq.get_dependent_symbol().super_symbol,
                                 eq.resolve()) for eq in step.equation_nodes)
        text = "\n".join(text)
        print text.replace("(t)", "")
