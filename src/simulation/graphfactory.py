from simulation.equationgraph import EquationGraph
import logging
import core.common
logger = logging.getLogger(__name__)


class ConversionError(core.common.LascavesimException):
    pass


def equationgraph_factory(system):
    """ Creates an EquationGraph form a LaScaVeSim object tree """

    # create new graph object
    graph = EquationGraph()

    # generate an equation-node per equation, and add its symbols
    if hasattr(system, 'component'):
        aspects = set(a for c in system.component for a in c.aspect)
        isolation_mode = False
    elif hasattr(system, 'aspect'):
        aspects = set(a for a in system.aspect)
        isolation_mode = True
    elif hasattr(system, 'equation') and hasattr(system, 'symbol'):
        aspects = set([system])
        isolation_mode = True
    else:
        raise TypeError()

    equations = (e for a in aspects for e in a.equation)
    symbols = (s for a in aspects for s in a.symbol if s.isVariable())

    # create the symbol registry
    symbol_registry = _get_symbol_registry(symbols, graph, isolation_mode)

    for eq in equations:
        symbol_iter = _get_symbol_iter(eq, symbol_registry, graph)
        equation = graph.add_equation(eq.expression, symbol_iter)

    # check for unconnected symbols or equations
    for node in graph.super_symbols:

        if node.symbols:
            continue
        if node.is_differential():
            logger.critical("a differential was not connected")
            raise EquationGraphError("unconnected derivative")

        msg = (k for k, v in symbol_registry.iteritems() if v[0] == node)
        msg = ((s.name, s._xml_fileId, s._xml_lineNumber) for s in msg)
        msg = ("{0} as defined on line {2} of {1}".format(*m) for m in msg)

        if node.differential is None:

            # inform the user
            msg = "\n".join("  * {}".format(m) for m in msg)
            logger.warn("the following symbol(s) will be ignored because they "
                        "seem unused:\n{}".format(msg))
            # graph.super_symbols.remove(node)

        else:
            logger.warn("{} is a 'tank-variable'; it may be more efficient to "
                        "reallocate integrating the corresponding variable "
                        "to post-processing".format(msg.next()))

    return graph


def _get_symbol_registry(symbols, graph, isolation_mode=False):
    """ Matches LaScaVeSim-symbols to graph super-symbols """

    # this creates a new SuperSymbol instance for each shared variable of a
    # LaScaVeSim model. Beware that DifferentialSuperSymbols are added later, in
    # _get_symbol_iter_, when there is a LaScaVeSim Equation object (the latter
    # makes it easy to determine whether a symbol is used in its normal or
    # differential form)

    symbol_registry = {}
    for symbol in symbols:

        # check for existing symbols (also by equivalence)
        math_symbol = symbol_registry.get(symbol, None)
        for equiv_symbol in symbol.getEquivalentSymbols(isolation_mode):
            try:
                math_symbol, count = symbol_registry[equiv_symbol]
            except KeyError:
                continue
            break

        # if there was none, create one
        if math_symbol is None:

            # unconnected ports -> sources/sinks
            mon = False
            mod = False
            if (symbol.hasAssociatedInputSignals()
                    and symbol.crossesSystemBoundary()):
                mod = True
                logger.debug("modulated, because it crosses bounds: {}"
                             .format(str(symbol)))
            elif symbol.isSource() or symbol.isSink():
                port = symbol.getCausalPort()
                if (hasattr(port, 'isConnected') and not port.isConnected()):
                    logger.warn("unconnected port detected - will be "
                                "treated as a modulated flow/effort "
                                "source")
                    mod = symbol.isSink()
                    mon = symbol.isSource()

            math_symbol = graph.add_supersymbol(modulated=mod, monitored=mon)
            count = 0

        # and register
        symbol_registry[symbol] = (math_symbol, count + 1)

    return symbol_registry


def _get_symbol_iter(equation, symbol_registry, graph):
    """ Returns an iterator, yielding the LocalSymbols for 'equation' """

    # yields a LocalSymbol for each symbol figuring in the provided Lascavesim
    # Equation object 'equation'. Note that if the time-derivative of a symbol
    # is used, a new DifferentialSuperSymbol is created (therefore requiring
    # the graph as parameter)

    syms = equation.getInvolvedSymbols
    try:
        derivatives = syms(only_derivatives=True)
        symbols = syms(omit_constants=True, exclude_derivatives=True)
    except KeyError:
        msg = "the equation declared on line {} of {}"
        msg = msg.format(equation._xml_lineNumber, equation._xml_fileId)
        logger.critical("missing symbol declaration(s) for " + msg)
        raise ConversionError("incorrect definition")

    all_symbols = set(list(derivatives) + list(symbols))
    m1 = ", ".join(str(s) for s in symbols)
    m2 = ", ".join("d" + str(s) for s in derivatives)
    logger.debug("{} -> {}, {}".format(str(equation.expression), m1, m2))

    for symbol in all_symbols:

        # get the super symbol
        super_symbol, count = symbol_registry[symbol]

        # get 'negative'-property
        negative = False

        # get dependence
        dependent = None
        if symbol.isSource():
            dependent = True
        if symbol.isSink() and not symbol.isSource():
            dependent = False

        if symbol in derivatives:

            # BEWARE: differential super-symbols are created here
            # note that the constructor takes care of assigning the differential
            # property of the corresponding integral symbol
            # using the graph-method ensures that all super-symbols get stored
            # in the graph's super-symbol set, without having to extract them
            # after the fact
            ds = super_symbol.differential
            if ds is None:
                ds = graph.add_supersymbol(differential_of=super_symbol)

            # TODO: check this
            # overwrite dependency on derivatives so that it is always set
            yield graph.add_localsymbol(symbol.name, negative=negative,
                                        dependent=None, super_symbol=ds,
                                        differential_order=1)

        if symbol in symbols:

            # if there's both the symbol and the derivative in the same equation
            # then the symbol is always independent
            if symbol in derivatives:
                dependent = False
            elif super_symbol.differential is not None and dependent:
                logger.warn("overwritten thing {}".format(symbol))

            # multiple dependencies may be conflicting -> use conditioning
            yield graph.add_localsymbol(symbol.name, negative=negative,
                                        dependent=dependent,
                                        super_symbol=super_symbol)
