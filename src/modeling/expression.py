"""
Provides the basic mathematical modeling classes

The foundation of all LaScaVeSim models are mathematical expression, either in
the form of equations or inequalities (bounds). All of those are built on
(sub-classed from) :py:class:`Expression`, whose primary role is to  interface
with the :py:class:`CAS_backend` to do things like extract mathematical symbols
(:py:meth:`getInvolvedSymbols`). Expression-objects are to be used within the
context of a :py:class:`MathematicalNamespace`-subclass. It collects
:py:class:`Expression` instances and manages the contained symbols (symbols of
the same name within a :py:class:`MathematicalNamespace` are considered
identical).

In other words, :py:class:`Expression` and :py:class:`MathematicalNamespace`
instances only work in conjunction.

:author: ggeorges
"""

from core.xml.persistence import xmlize, XMLpersistentObject
import core.equationhandling


class MathematicalNamespace(core.equationhandling.MathEquationSystem):
    """ Common functionality for mathematical-namespaces

    Within a mathematical namespace, symbols of the same name are
    identical. While :py:class:`Expression` objects manage the actual
    mathematical expression, this class manages the symbols that may be used
    in more than one equation at a time.

    :py:class:`MathematicalNamespace` is meant to be sub-classed by other
    classes, such as e.g. aspects. The relationship between such instances and
    the :py:class:`Expression`-instance has to be managed by the first, meaning
    the current implementation only provides the required funcitonlity to
    extract and collect symbols, not expressions.
    """
    pass

    # in the end, it turned out that this could just be an alias for 
    # MathEquationSystem; the class has been kept though to prevent changes
    # to other modules.


@xmlize(None, textProperty="expression",
        namespace="http://n.ethz.ch/~ggeorges/lascavesim")
class Expression(XMLpersistentObject, core.equationhandling.MathExpression):
    """ Common functionality of mathematical expressions

    Only serves as a base class for the :py:class:`Equation` and
    :py:class:`Bound` types; there is no need to ever create an object of this
    type directly. """

    def __init__(self, expression):
        XMLpersistentObject.__init__(self)
        core.equationhandling.MathExpression.__init__(self, expression)


@xmlize('eq')
class Equation(Expression):
    """ Represents a mathematical equations

    An Equation object is a mathematical expression with an explicit equality
    operation. Textual representations can either implicitly specify a '==' or
    will have '==0' appended by default. """

    def __init__(self, expression):
        expr = self._CAS().parse_toEquation(expression)
        super(Equation, self).__init__(expr)


@xmlize("bound", textProperty="expression",
        namespace="http://n.ethz.ch/~ggeorges/lascavesim")
class Bound(XMLpersistentObject):
    """ Models constraints on any set of symbols

    The 'bound' object stores one upper and one lower bound condition on a
    symbol. The expression, upper and lower bound may be any combination of
    symbols and constants used within the parent aspect. """

    upper = None
    lower = None

    def __init__(self, expression, upper=None, lower=None, strict_upper=None,
                 strict_lower=None):
        if all(s is None for s in (upper, lower, strict_upper, strict_lower)):
            self.log_warn("bound-tag has no effect as it fails to specify"
                          " any upper or lower limits")
        if not upper is None:
            self.upper = Inequality(expression, upper, self)
        if not strict_upper is None:
            if not self.upper is None:
                self.log_warn("upper bound overwritten by strict-upper bound")
            self.upper = Inequality(expression, strict_upper, self, True)
        if not lower is None:
            self.lower = Inequality(lower, expression, self)
        if not strict_lower is None:
            if not self.lower is None:
                self.log_warn("lower bound overwritten by strict-lower bound")
            self.lower = Inequality(strict_lower, expression, self, True)

    def getInequalities(self):
        """ Returns the corresponding inequalities

        A bound is actually just a set of two inequalities, one for the upper,
        and one for the lower boundary, each represented by its own
        :py:class:`Inequality` instance. This function returns a tuple
        containing the lower and upper inequalities (in that order). """
        return (self.lower, self.upper)


class Inequality(core.equationhandling.MathExpression):
    """ Represents an inequality

    This object describes an inequality, either strict or relaxed, of the form
    :py:arg:`rhs` >/>= :py:arg:`lhs`. To express a > b rhs will thus need to be
    a. Note that by default, relaxed inequalities (>=) are assumed; this can
    be overriden using :py:arg:`strict`. """

    _linkBack = None
    strict = None

    def __init__(self, lhs, rhs, _parent, strict=False):
        """ Initializes an inequality

        :param str lhs: a textual representation of the mathematical expression
            of lhs, where lhs > rhs (lhs stands for left hand side)
        :param str rhs: a textual representation of the mathematical expression
            of rhs, where lhs > rhs (rhs stands for right hand side)
        :param Bound _parent: the bound the inequality belongs to
        :param bool strict: (default: False), changes the inequality operator
            form ">=" to ">"

        :throws: syntax erros will cause math-parser errors """

        lhs, rhs = tuple(self._CAS().parse_toExpression(s) for s in (lhs, rhs))
        core.equationhandling.MathExpression.__init__(self, rhs - lhs)
        self._linkBack = _parent
        self.strict = strict

    def getParent(self):
        """ Returns the associated :py:class:`Bound`-instance """
        return self._linkBack.getParent()
