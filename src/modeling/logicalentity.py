'''
Created on 26.11.2012

@author: ggeorges
'''

from core.xml.persistence import (xmlize, XMLpersistentObject,
                                  XMLImportAttributeHandler,
                                  XPointerSyntaxError)
from .expression import Bound, Equation, MathematicalNamespace
from .quantity import Variable, Constant, Property
from .ports import PhysicalPort, LogicalPort, PortConnection
import core.xml.parser
import modeling.parameters
import modeling.expression


class ExternalEntity(XMLImportAttributeHandler):

    def __init__(self, attr, template):
        ns = "http://n.ethz.ch/~ggeorges/lascavesim/external.xsd"
        super(ExternalEntity, self).__init__(ns, attr)
        self.template = template

    def xpointer_template(self, parser):
        if not hasattr(parser.object, 'name'):
            raise RuntimeError()
        return self.template.format(parser.object.name)

    def parserModifier(self, parser, value):
        try:
            super(ExternalEntity, self).parserModifier(parser, value)
        except XPointerSyntaxError:
            # try adding a correct x-pointer instance
            value += '#xpointer({})'.format(self.xpointer_template(parser))
            super(ExternalEntity, self).parserModifier(parser, value)


@xmlize('doc', textProperty=core.xml.parser.TextPropertyDescription('text', []))
class Documentation(XMLpersistentObject):

    def __init__(self, text):
        self.text = text


@xmlize(None, namespace="http://n.ethz.ch/~ggeorges/lascavesim")
class LogicalEntity(XMLpersistentObject):

    description = None
    name = None

    def __init__(self, name, description=""):
        self.name = name
        self.description = description
        XMLpersistentObject.__init__(self)

    def get(self, name, node_type):
        pass


@xmlize('aspect', children=(('equation', Equation), ('bound', Bound),
                            ('symbol', Variable, Constant)))
class Aspect(LogicalEntity, MathematicalNamespace):

    def addEquation(self, expr):
        """ Appends an equation

        Convenience method, to add equations from text. Creates the object (if
        it isn't an Equation already) and appends it using the :py:meth:`adopt`
        method. It also calls :py:meth:`getInvolvedSymbols` on the newly
        created equation with its :py:arg:`createUnknownSymbols` set to True,
        so that not yet defined symbols are automagically created.

        :param str expr: a textual representation of an equation """

        # create if necessary
        import modeling.expression
        if isinstance(expr, str):
            expr = modeling.expression.Equation(expr)
        if not isinstance(expr, modeling.expression.Equation):
            raise TypeError('expr must be of type Equation')

        # create symbols
        expr.getInvolvedSymbols(createUnknownSymbols=True)

        # add to the mix
        self.adopt(expr)
        return expr

    def extractEquations(self, dont_flatten=False):
        """ Returns all associated equations and bounds

        This function returns all the associated equation objects as a list of
        (references to the original) :py:class:`modeling.expression.Equation`
        and :py:class:`modeling.expression.Bound` objects. """

        return [eq for eq in self.equation] + [bnd for bnd in self.bound]

    def getCompactExpressions(self):
        exprs = [eq.expression for eq in self.equation]
        cas = self.equation[0]._CAS()
        subs, exprs = cas.eliminate(exprs, self.getSubstituteVariables())
        return [modeling.expression.Equation(str(expr)) for expr in exprs]

    def getSubstituteVariables(self):
        """ Returns a list of non-connected variables and non-states

        Within components, variables can be used as a substitute for a complex
        expression. While handy, they have no meaning for the simulation. This
        function returns a list of all variables that are linked to a signal or
        are a state (they are derived by time) """

        variables = set()
        for var in self.symbol:
            if not var.isVariable():
                continue
            if var.isState():
                continue
            if var.isLinked():
                continue
            variables.add(var)
        return variables


class PhysicalEntity(LogicalEntity):

    def isSystem(self):
        return False

    def extractEquations(self, subtype):
        """ Extracts all contained equations

        Calls :py:meth:`extractEquations` on all objects stashed away in an
        :py:class:`core.xml.XMLpersistenList`, as specified by the
        :py:arg:`subtype` argument. Note that the latter may also be a list
        of names, pointing to different collections. The method will be
        called using polymorphism. The :py:class:`Aspect` class will overwrite
        the default behavior and do the actual equation extraction instead.

        :param subtype: variable name(s) to get equations from
        :type subtype: str or list of str """

        # extract all equations
        equations = []

        # allow several properties to be combined
        if hasattr(subtype, '__iter__'):
            for subtyp in subtype:
                equations += PhysicalEntity.extractEquations(self, subtyp)
            return equations

        # work on individual properties
        for aspect in getattr(self, subtype):
            equations += aspect.extractEquations()
        return equations


# TODO: remove on next version
@xmlize('port', namespace="http://n.ethz.ch/~ggeorges/lascavesim")
class Port(XMLpersistentObject):

    def __init__(self, name, type=None, positive_direction=None, causal=None):
        self.log_error("<port> tag no longer suppoerted; use <logical-port> "
                       "resp. <physical-port> instead; declaration will be "
                       "ignored")


# TODO: remove on next version
@xmlize('junction', namespace="http://n.ethz.ch/~ggeorges/lascavesim")
class Junction(XMLpersistentObject):

    def __init__(self, name, type=None, positive_direction=None, causal=None):
        self.log_error("<junction> tag no longer suppoerted; use "
                       "<port-connection> instead")

# TODO: remove on next version


@xmlize('portconnection', namespace="http://n.ethz.ch/~ggeorges/lascavesim")
class Junction(XMLpersistentObject):

    def __init__(self, name, type=None, positive_direction=None, causal=None):
        self.log_error("<junction> tag no longer suppoerted; use "
                       "<port-connection> instead")


@xmlize(
    'component', children=(Aspect, ('port', Port, LogicalPort, PhysicalPort), Property, modeling.parameters.LookupTable),
    attribute_handlers=[ExternalEntity('component',
                                       '//lascavesim/system/component[@name="{}"]/*')])
class Component(PhysicalEntity):

    def extractEquations(self):
        """ Extracts all contained equations

        This obtains all the equations of all contained aspects and returns
        them as a list of objects. """
        return PhysicalEntity.extractEquations(self, 'aspect')


@xmlize('assembly', children=(Component, ('port', Port, LogicalPort,
                                          PhysicalPort), PortConnection),
        attribute_handlers=[ExternalEntity('assembly',
                                           '//lascavesim/system/assembly[@name="{}"]/*')])
class Assembly(PhysicalEntity):

    def extractEquations(self):
        """ Extracts all contained equations

        This obtains all the equations of all contained aspects and returns
        them as a list of objects. """
        return PhysicalEntity.extractEquations(self, 'component')


@xmlize('system', children=(PortConnection, Documentation, ('port', Port,
                                                            LogicalPort,
                                                            PhysicalPort),
                            ('component', Assembly, Component)),
        attribute_handlers=[ExternalEntity('system',
                                           '//lascavesim/system[@name="{}"]/*')])
class System(PhysicalEntity):

    def extractEquations(self):
        """ Extracts all contained equations

        This obtains all the equations of all contained aspects and returns
        them as a list of objects. """
        return PhysicalEntity.extractEquations(self, 'component')

    def isSystem(self):
        return True
