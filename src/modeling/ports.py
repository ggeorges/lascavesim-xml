'''
Created on 26.11.2012

@author: ggeorges
'''

from core.xml.persistence import XMLpersistentObject, xmlize, XMLConfigError
from core.common import LascavesimException
from quantity import Input, Output, Effort, Flow
from core.xml.delayed import TwoPartObjectReference
import re


class SignalMismatchError(LascavesimException):
    pass


@xmlize(namespace="http://n.ethz.ch/~ggeorges/lascavesim")
class Port(XMLpersistentObject):

    portType = None
    connection = None

    def __init__(self, name):

        # remember our name (need a name!)
        self.name = str(name).strip()

        # now add lists and stuff
        XMLpersistentObject.__init__(self)
        self.connection = None

    def isConnected(self):
        return not self.connection is None

    def setConnection(self, connection):
        """ Overwrite the existing connection object

        This function is used by the PortConnection object to establish a link
        back to itself. Shouldn't be used directly on already initialized
        objects! """
        if not hasattr(connection, 'connectPort'):
            raise TypeError('connection must be a PortConnection')
        if not self.connection is None:
            raise XMLConfigError(self, "port already connected")
        self.connection = connection

    def isPhysical(self):
        return False

    def isLogical(self):
        return False

    def isSystemPort(self):
        """ Returns true if this is a system port """
        return self.getParent().isSystem()

    def getEquivalentSignals(self, signal, isolation_mode):
        """ Returns a set of equivalent signals

        Given a :py:class:`modeling.quantity.Signal` or a signal name (str)
        this function returns a set with signals of the same name, attached
        to connected ports.

        :param modeling.quantity.Signal signal: the signal to check """

        # not connected -> return signal if a member
        if not self.isConnected() or isolation_mode:
            try:
                return set((self.getSignalByName(signal),))
            except KeyError:
                return set()
        return self.connection.getEquivalentSignals(signal)

    def getSignalByName(self, name):
        """ Retrieves a signal by name

        within a port, signal names have to be unique. Therefore this method
        either returns the associated signal matching :py:attr:`name` or it
        raises a :py:exc:`KeyError`.

        Note that this method only works on the local scope. Use
        :py:meth:`getEquivalentSignals` to also scan remote ports.

        :param str name: a signal name to look for. """

        # also allow passing signals
        name = name.name if hasattr(name, 'name') else str(name)

        for signal in self.signal:
            if signal.name == name:
                return signal
        raise KeyError()

    def requireSignalsMatchPort(self, port):
        """ Throws an exception if this port can't be connected to 'port' """

        if not hasattr(port, 'getSignalByName'):
            raise TypeError("not a port")

        for signal in self.signal:
            # signals must match by name, but only physical signals are
            # required to be present in all connected ports
            try:
                remote_signal = port.getSignalByName(signal.name)
            except KeyError:
                if signal.isLogical():
                    continue
                raise PortMismatchError(port, self, signal, 0)

            # physical/logical mustn't mix
            if remote_signal.isPhysical() != signal.isPhysical():
                raise PortMismatchError(port, self, signal, 1)
            if remote_signal.isLogical() != signal.isLogical():
                raise PortMismatchError(port, self, signal, 1)

            # check effort/flow match
            if (signal.isPhysical() and
                ((signal.isEffort() != remote_signal.isEffort()) or
                 (signal.isFlow() != remote_signal.isFlow()))):
                raise PortMismatchError(port, self, signal, 2)

            # there can only be one output per logical signal
            # this only compares ports 1:1, so conflicts may arise if not all
            # ports in a bus are considered
            if (signal.isLogical() and signal.isOutput()
                    and remote_signal.isOutput()):
                raise PortMismatchError(port, self, signal, 3)


@xmlize('physical-port', children=(('signal', Effort, Flow),))
class PhysicalPort(Port):

    def __init__(self, name):
        super(PhysicalPort, self).__init__(name)

    def isPhysical(self):
        return True

    def isSourceOfEffort(self):
        for s in self.signal:
            if s.isEffort() and s.isSource():
                return True
        return False

    def isSourceOfFlow(self):
        for s in self.signal:
            if s.isFlow() and s.isSource():
                return True
        return False

    def preAdoptionHook(self, signal):

        # adoption should still be a matter of xml structure; so just complain
        # if there is something wrong with the signals
        for attr in ('isEffort', 'isFlow', 'isPhysical'):
            if not (hasattr(signal, attr)):
                return
        if signal.isEffort() and any(s.isEffort() for s in self.signal):
            raise SignalMismatchError("there is already another effort")
        if signal.isFlow() and any(s.isFlow() for s in self.signal):
            raise SignalMismatchError("there is already another flow")


@xmlize('logical-port', children=(('signal', Input, Output),))
class LogicalPort(Port):

    def isLogical(self):
        return True


@xmlize('port', textProperty="reference",
        namespace="http://n.ethz.ch/~ggeorges/lascavesim/ref")
class PortReference(TwoPartObjectReference):

    def __init__(self, reference, component=None):

        # capture reference to the system-"component":
        if not "." in reference and component is None:
            TwoPartObjectReference.__init__(self, reference, None, 'system',
                                           ('port',))
            return
        TwoPartObjectReference.__init__(self, reference, component, 'system',
                                        ('component', 'port'))

    def link(self, connection, port):
        try:
            connection.connectPort(port)
        except PortMismatchError as e:
            self.log_error(e.message)


@xmlize('port-connection', children=(PortReference,),
        namespace="http://n.ethz.ch/~ggeorges/lascavesim")
class PortConnection(XMLpersistentObject):

    ports = None

    def __init__(self):

        # dim port collection
        self.ports = set()

    def connectPort(self, port):

        # prevent duplicates / race conditions
        if port in self.ports:
            return

        # physical ports combine pairwise only
        if sum(p.isPhysical() for p in self.ports) >= 2:
            raise PortMismatchError(port, reason=5)

        # check the port against all existing ports
        for existing_port in self.ports:
            existing_port.requireSignalsMatchPort(port)

        # the signal balance must only match at the end, so this is all the
        # consistency checking we can do

        # add to the collection and make it point back to here
        self.ports.add(port)
        port.setConnection(self)

    def removePort(self, port):
        if port in self.ports:
            self.ports.remove(port)
        if not self.ports:
            del self

    def getEquivalentSignals(self, given_signal):
        """ Finds the equivalent connected signals

        The method scans all connected ports for signals of the same name. It
        returns a :py:class:`set` instance. The provided signal can either be
        a :py:class:`modeling.quantity.Signal` instance or the name of a
        signal (str). """

        # the answers
        equivalent = set()

        # try to get a signal, then look for equivalence by symbol
        for port in self.ports:
            try:
                signal = port.getSignalByName(given_signal)
                equivalent.add(signal)
            except KeyError:
                pass

        return set(equivalent)

    def isLogical(self):
        return self._jtype.isLogical()

    def isPhysical(self):
        return self._jtype.isPhysical()


class PortMismatchError(LascavesimException):

    port = None
    junction = None
    conflicting_port = None

    def __init__(self, port, conflicting_port=None, culprit=None, reason=None):

        # messages
        reasons = [
            "{culprit}-signal has no counter-part in port {port}",
            "logical and physical ports cannot interconnect",
            "the {culprit}-signal is conflicting with the other port (see "
            "declarations on lines {port_line} and {conflicting_port_line})",
            "{culprit}-signal causality definition conflicts",
            "{culprit} used as input and output",
            "physical port connections cannot involve more than two ports"]

        # gather text
        kw = {'culprit': '', 'port': '', 'conflicting_port': ''}
        for n, o in (('culprit', culprit), ('port', port),
                     ('conflicting_port', conflicting_port)):
            if hasattr(o, 'getName'):
                kw[n] = o.getName("component")
                kw[n + "_line"] = o._xml_lineNumber

        try:
            self.message = reasons[reason].format(**kw)
        except KeyError:
            self.message = "port is incompatible with connection"
