'''
Created on 26.11.2012

@author: ggeorges
'''

from core.xml.persistence import (xmlize, XMLpersistentObject,
                                  XMLAttributeHandler)
from core.xml.delayed import ObjectReference, TwoPartObjectReference, DelayedAction
import core.equationhandling
import modeling.parameters
import re


# top-level class: "Quantity" -------------------------------------------------

@xmlize(namespace="http://n.ethz.ch/~ggeorges/lascavesim")
class Quantity(XMLpersistentObject):

    """ A named entity representing a physical quantity """

    unit = None
    description = None
    value = None

    def __str__(self):
        return self.name

    def __init__(self, name, unit=None, description="", value=None):
        self.name = name
        self.unit = unit
        self.description = description
        self.value = value
        XMLpersistentObject.__init__(self)

    def isSignal(self):
        return False

    def isSymbol(self):
        return False


@xmlize('signal', textProperty='reference',
        namespace='http://n.ethz.ch/~ggeorges/lascavesim/ref')
class SignalReference(TwoPartObjectReference):

    def __init__(self, reference, port=None):
        TwoPartObjectReference.__init__(self, reference, port, 'component',
                                        ('port', 'signal'))

    def link(self, variable, signal):
        variable.linkToSignal(signal)

# derived class: "Property" --------------------------------------------------


@xmlize('property')
class Property(Quantity):
    linkedConstant = None

    def __init__(self, name, unit=None, description="", value=None,
                 min=None, max=None, type=None):

        Quantity.__init__(self, name, unit, description, value)
        self.min = min
        self.max = max
        self.data_type = type


@xmlize('property', textProperty='reference',
        namespace='http://n.ethz.ch/~ggeorges/lascavesim/ref')
class PropertyReference(ObjectReference):

    def __init__(self, reference):

        # set the reference straight
        reference = str(reference)
        ObjectReference.__init__(self, reference, 'component', ('property',))

    def link(self, variable, signal):
        pass


# derived class: "Symbol" and its children (variable & constant) --------------

class Symbol(core.equationhandling.MathSymbol, Quantity):

    """ A mathematical symbol; exposes CAS symbols of equations """

    equations = None

    def __init__(self, name, unit=None, description="", value=None):
        Quantity.__init__(self, name, unit, description, value)
        self.equations = set()

    def isVariable(self):
        return False

    def isConstant(self):
        return False

    def isState(self):
        return False

    def getSymbolName(self, includeParent=False, makeDerivatived=False):
        txt = self.name
        if includeParent:
            parents = self.getParents('component')
            parents = [p.name for p in parents]
            txt = ".".join(parents) + "." + txt
        if makeDerivatived:
            txt = "Derivative({}, t)".format(txt)
        return txt

    def isNamed(self, name):
        return str(name).replace('(t)', '') == self.name

    def isSymbol(self):
        return True

    @staticmethod
    def createSymbol(name, unit, description, value):
        """ Creates a new symbol, variable or constant

        This function calls either the :py:class:`Variable` or
        :py:class:`Constant` constructor, depending on whether there is a
        '(t)' in name or not. Since both defer to the :py:class:`Symbol`
        constructors anyway, it accepts the same arguments.

        :param str name: the variable or constant's name (may include '(t)')
        :param str unit: name of the unit represented by this quantity
        :param str description: a description of what the symbol represents
        :param value: numeric value, mainly for constants
        """

        if '(t)' in name:
            return Variable(name, unit, description, value)
        else:
            return Constant(name, unit, description, value)


@xmlize('variable', children=(SignalReference,))
class Variable(Symbol):

    linkedSignals = None

    def __init__(self, name, unit="", description="", expression=None):

        # remove '(t)' in name
        name = name.replace('(t)', '')

        Symbol.__init__(self, name, unit, description, expression)
        self.linkedSignals = set()

    def getSymbolName(self, includeParent=False, makeDerivatived=False):
        txt = "{}(t)".format(Symbol.getSymbolName(self, includeParent,
                                                  makeDerivatived=False))
        if makeDerivatived:
            return "Derivative({}, t)".format(txt)
        return txt

    def linkToSignal(self, signal):
        """ Associates a signal to the current variable

        Given a Signal object, this method associates the signal object to the
        bound Variable (via :py:attr:`linkedSignals`). It also calls
        :py:meth:`Signal.linkToVariable` to establish a link back.

        Note that if the signal is already associated to another Variable,
        :py:meth:`Signal.linkToVariable` will cause a :py:exc:`RuntimeError`.

        :param Signal signal: the signal to link
        """

        # resolve "port.signal" textual references
        if isinstance(signal, str) and signal.count('.') == 1:

            # get parent component and check it is actually a component...
            component = self.getParents('component').pop(0)
            if not hasattr(component, 'get'):
                raise RuntimeError('invalid component node')

            # get the signal and be done -> KeyError othrwise
            signal = component.get(signal, 'signal')

        # check type
        if (not hasattr(signal, 'linkToVariable') or
            not hasattr(signal, 'isLinkedTo') or
            not hasattr(signal, 'isInput') or
            not hasattr(signal, 'isOutput') or
            not hasattr(signal, 'isFlow') or
                not hasattr(signal, 'isEffort')):
            raise TypeError('argument must point to a signal object')

        # make sure linking does not violate any signal-flow constraints
        if signal.isInput():
            if self.hasAssociatedPhysicalSignals():
                raise TypeError("physical variable can't have logical inputs")
            if self.hasAssociatedInputSignals():
                raise TypeError("variables can't have more than one input")
        elif signal.isPhysical():
            if self.hasAssociatedInputSignals():
                raise TypeError("variable already has an input; can't make it "
                                "physical no more")
            if signal.isEffort() and self.hasAssociatedFlowSignals():
                raise TypeError("flow-variable can't accept effort signals")
            elif signal.isFlow() and self.hasAssociatedEffortSignals():
                raise TypeError("effort-variable can't accept flow signals")

        # add remote - will cause an exception if already linked to another
        if not signal.isLinkedTo(self):
            signal.linkToVariable(self)

        self.linkedSignals.add(signal)

    def getCausalPort(self):
        """ Returns the port that (causally) defines a signal """

        port = (s for s in self.linkedSignals)
        if self.hasAssociatedPhysicalSignals():
            # TODO: might cause problem on "looped-through" symbols
            port = (s for s in port if s.isPhysical())
            port = (s for s in port if s.isSink() or s.isSource())
        elif self.hasAssociatedInputSignals():
            port = (s for s in port if s.isLogical())
            port = (s for s in port if s.isInput())
        else:
            return None

        port = (s.getParent() for s in port)
        try:
            return iter(port).next()
        except StopIteration:
            return None

    def hasAssociatedPhysicalSignals(self):
        return any(s.isPhysical() for s in self.linkedSignals)

    def hasAssociatedInputSignals(self):
        return any(s.isInput() for s in self.linkedSignals)

    def hasAssociatedEffortSignals(self):
        return any(s.isEffort() for s in self.linkedSignals)

    def hasAssociatedFlowSignals(self):
        return any(s.isFlow() for s in self.linkedSignals)

    def crossesSystemBoundary(self):
        """ Returns true if this variable is available at the system level

        Variables linked to signals bound to a system-port (i.e. a port bound
        to the system object) are available to the outside world. If there is
        one associated to this variable, this method returns True. """

        # look within all equivalent signals
        return any(signal.attachedToSystemPort() for signal
                   in self.getEquivalentSignals())

    def isSource(self):
        if not self.isLinked(False):
            return None
        return any(s.isSource() for s in self.linkedSignals if s.isPhysical())

    def isSink(self):
        if not self.isLinked(False):
            return None
        return any(s.isSink() for s in self.linkedSignals if s.isPhysical())

    def isVariable(self):
        return True

    def isLinked(self, validate_remote=True):
        """ Returns true if this variable is linked to any signal

        The methods checks the value of the Variable's :py:prop:`linkedSignals`
        property; if it points to an object with a 'isLinkedTo' method (this
        may also be a :py:class:`Variable` though, be careful) it uses it to c
        check whether that object points back to us. If not,  it is removed
        from the linked signals set.

        :param bool validate_remote: if True (default), the method checks
            whether the linked variable actually links back to us, and if not
            removes it from the linkedSignals array.
        """
        if self.linkedSignals is None:
            return False
        if (not hasattr(self.linkedSignals, '__iter__') or
                not hasattr(self.linkedSignals, 'remove')):
            return False
        if validate_remote:
            self.linkedSignals = set(signal for signal in self.linkedSignals if
                                     (hasattr(signal, 'isLinkedTo') and
                                      signal.isLinkedTo(self, False)))
        if not self.linkedSignals:
            return False
        return True

    def isLinkedTo(self, signal, validate_remote=True):
        """ Checks whether the object is associated to a given signal

        This first performs all the checks :py:meth:`isLinked` does, and then
        checks whether the signal provided via the :py:arg:`signal` argument
        is among the 'linked signals'.

        :param signal: the object to check for
        """

        if not hasattr(self.linkedSignals, '__contains__'):
            return False
        return signal in self.linkedSignals

    def getEquivalentSymbols(self, isolation_mode=False):

        # allow equivalent signals to be passed in (speed)
        variables = set([self])

        # collect all attached symbols
        for signal in self.getEquivalentSignals(isolation_mode):
            if signal.isLinked():
                variables.add(signal.linkedVariable)
        return variables

    def getEquivalentSignals(self, isolation_mode=False):

        # this solution needs 3 arrays: the result-array (equivalent) wich also
        # traces all signals seen; the signals array which stores the signals to
        # be processed in the current pass, and new_signals, which is used to
        # collect linked signals during a pass; after being filtered using
        # equivalent it becomes 'signals' for the next pass
        signals = self.linkedSignals
        equivalent = set()

        while signals:
            new_signals = set()
            for signal in signals:
                equivalent.add(signal)
                new_signals.update(signal.getEquivalentLocalSignals())
                new_signals.update(signal.getEquivalentRemoteSignals())
            signals = set(s for s in new_signals if not s in equivalent)
        return equivalent


@xmlize('lookup', textProperty='reference',
        namespace='http://n.ethz.ch/~ggeorges/lascavesim/ref')
class LookupReference(TwoPartObjectReference):

    def __init__(self, reference, table=None):
        TwoPartObjectReference.__init__(self, reference, table, 'component',
                                        ('lookup', 'field'))

    def link(self, constant, field):
        pass


@xmlize('value', textProperty='value',
        namespace='http://n.ethz.ch/~ggeorges/lascavesim')
class ConstantValue(DelayedAction):

    def __init__(self, value):
        DelayedAction.__init__(self, 'setValue', value)


@xmlize('constant', children=(PropertyReference, ConstantValue,
                              LookupReference,
                              modeling.parameters.ConstantExpression))
class Constant(Symbol):
    linkedProperty = None
    _value = None

    def isConstant(self):
        return True

    def setValue(self, value):
        self._value = value

    def resolve(self):
        return self._value


# derived class: "Signal" and its children (effort & flow) -------------------


class Signal(Quantity):

    """ A signal routes symbols through ports """

    linkedVariable = None
    origin = None

    def isSignal(self):
        return True

    def linkToVariable(self, symbol):

        # check the type
        if (not hasattr(symbol, 'linkToSignal') or
                not hasattr(symbol, 'isLinkedTo')):
            raise TypeError('symbol must be a Variable')

        # only if we are not already linked
        if not self.linkedVariable is None and self.linkedVariable != symbol:
            raise RuntimeError('Signal "{}" already linked to "{}"'.
                               format(str(self), str(self.linkedVariable)))

        # do the local linking
        self.linkedVariable = symbol

        # do the remote linking
        if not symbol.isLinkedTo(self):
            symbol.linkToSignal(self)

    def isLinked(self, validate_remote=True):
        """ Returns true if this signal is linked to any symbol

        The methods checks the value of the Signal's :py:prop:`linkedVariable`
        property; if it points to an object with a 'isLinkedTo' method (this
        may also be a :py:class:`Signal` though, be careful) it uses it to
        check whether that object points back to us. If not, the link is
        broken.
        """

        if self.linkedVariable is None:
            return False
        if not hasattr(self.linkedVariable, 'isLinkedTo'):
            return False
        if validate_remote and not self.linkedVariable.isLinkedTo(self, False):
            self.linkedVariable = None
            return False
        return True

    def isLinkedTo(self, symbol, validate_remote=True):
        """ Checks whether the object is associated to a given symbol

        This first performs all the checks :py:meth:`isLinked` does, and then
        checks whether the 'linked variable' is the same as the one provided
        via the :py:arg:`symbol` argument.

        :param symbol: the object to check for
        """

        if not hasattr(symbol, 'linkToSignal'):
            return False
        return self.linkedVariable == symbol

    def isEffort(self):
        return False

    def isFlow(self):
        return False

    def isOutput(self):
        return False

    def isInput(self):
        return False

    def isPhysical(self):
        return False

    def isLogical(self):
        return False

    def attachedToSystemPort(self):
        """ Returns true if this signal leads to a system-port

        Variables and signals connected to a system-port are available to the
        out-side world (i.e. the solver). If this signal is itself bound to a
        system port, or equivalent to one, this function will return true. """
        if not hasattr(self.getParent(), 'isSystemPort'):
            return False
        return self.getParent().isSystemPort()

    def getEquivalentRemoteSignals(self, isolation_mode=False):
        """ Returns a set of signals equivalent by port-connection """
        signals = self.getParent().getEquivalentSignals(self, isolation_mode)
        return set(s for s in signals if s != self)

    def getEquivalentLocalSignals(self):
        """ Returns a set of all signals equivalent by symbol """
        if not self.isLinked():
            return set()
        return set(s for s in self.linkedVariable.linkedSignals if s != self)


class PhysicalSignal(Signal):

    is_source = None
    is_sink = None

    def __init__(self, name, unit=None, description="",
                 causality="undetermined"):
        super(PhysicalSignal, self).__init__(name, unit, description, None)
        self.setCausality(causality)

    def setCausality(self, causality):
        """ Defines the logical flow of physical signals """

        causality = str(causality)

        self.is_source = False
        self.is_sink = False
        if re.match("^ *(source|output|dependent) *$", causality,
                    re.IGNORECASE):
            self.is_source = True
        elif re.match("^ *(sink|input|independent) *$", causality,
                      re.IGNORECASE):
            self.is_sink = True
        else:
            if not re.match("^ *(undetermined) *$", causality, re.IGNORECASE):
                self.log_warn("unrecognized value '{}' for attribute "
                              "causality; the signal will be reset to "
                              "'undetermined'".format(causality))

    def isSource(self):
        """ Returns if this signal is a source.

        Signals are considered the interface out of the port; thus 'source'
        means source of causality, i.e. information flows out through this
        signal, towards another port/component """

        return self.is_source

    def isSink(self):
        return self.is_sink

    def isUndetermined(self):
        return not self.is_source and not self.is_sink

    def isPhysical(self):
        return True

    def getOrigin(self):
        if self.isUndetermined():
            return None
        if self.isSource():
            return self.getParent('component')
        if self.isSink():
            return self.getParent().connection


@xmlize('effort')
class Effort(PhysicalSignal):

    def isEffort(self):
        return True


@xmlize('flow')
class Flow(PhysicalSignal):

    def isFlow(self):
        return True


class LogicalSignal(Signal):

    def isLogical(self):
        return True


@xmlize('input')
class Input(LogicalSignal):

    def isInput(self):
        return True


@xmlize('output')
class Output(LogicalSignal):

    def isOutput(self):
        return True
