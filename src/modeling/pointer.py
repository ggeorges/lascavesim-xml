"""
-class definition

:date: 20.03.2013
:author: ggeorges
"""
from core.xml.persistence import (xmlize, XMLpersistentObject,
                                  XMLAttributeHandler)
import os
import re


@xmlize("library", textProperty="path",
        namespace="http://n.ethz.ch/~ggeorges/lascavesim/external.xsd")
class ExternalLibrary(XMLpersistentObject):

    _libraries = None
    path = None
    name = None
    _isdir = None
    _path_variable = None

    def __init__(self, name, path):
        if not os.path.exists(path):
            self.log_error("'{}' does not exist; library alias won't work",
                           path)
        self.is_dir = os.path.isdir(path)
        self.name = name
        self.path = path
        if ExternalLibrary._libraries is None:
            ExternalLibrary._libraries = []
        ExternalLibrary._libraries.append(self)

    @classmethod
    def resolveXPointer(cls, pointer):

        # evaluate assuming xpointer syntax
        r = re.compile("([\w\-. {}:/\\]*)#xpointer\((.*)\)")
        matches = r.match(str(pointer))
        if matches is None:
            raise TypeError("xpointer syntax error: {}".format(pointer))
        fpath, xpath = matches.groups()
        fpath = os.path.normpath(fpath)

        # if the file exists, well great
        if os.path.isfile(fpath):
            return (fpath, xpath)



        if hasattr(ExternalLibrary._libraries, '__iter__'):
            for lib in ExternalLibrary._libraries:
                if (file in (lib.name, "({})".format(lib.name),
                            "lib({})".format(lib.name))):
                    file = lib.path


class ExternalEntity(XMLAttributeHandler):

    def __init__(self, attribute='import',
                 ns="http://n.ethz.ch/~ggeorges/lascavesim/external.xsd"):
        super(ExternalEntity, self).__init__(ns, "import")

    def parserModifier(self, parser, value):

        file, xpath = ExternalLibrary.resolveXPointer(value)

        try:
            with open(file, "r") as resource:
                self.log_info("importing {}".format(value))
                parser.enterSubDocumentMode(resource, xpath)
        except IOError:
            self.log_error("can't read {}".format(value))
