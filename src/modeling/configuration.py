'''
Created on 26.11.2012

@author: ggeorges
'''

from core.xml.persistence import xmlize, XMLpersistentObject, XMLAttributeHandler
from logicalentity import System
import os


@xmlize('library', textProperty='path',
        namespace="http://n.ethz.ch/~ggeorges/lascavesim")
class Library(XMLpersistentObject):
    path = None
    name = None

    def __init__(self, name="", path=""):
        XMLpersistentObject.__init__(self)
        self.setPath(path)
        self.name = name

    def setPath(self, path=None):

        # check args before allowing them
        if not os.path.isdir(str(path)):
            raise RuntimeError("'{}' does not point to a valid directory".
                               format(path))
        self.path = path


@xmlize('lascavesim', (Library, System),
        namespace="http://n.ethz.ch/~ggeorges/lascavesim", attribute_handlers=
        [XMLAttributeHandler("http://www.w3.org/2001/XMLSchema-instance",
                             "schemaLocation")])
class Lascavesim(XMLpersistentObject):

    version = None
    base_path = None

    def __init__(self, version=None, base_path=None):
        XMLpersistentObject.__init__(self)
        self.version = version
        self.base_path = os.getcwd() if base_path is None else base_path
