"""
-class definition

:date: 11.01.2013
:author: ggeorges
"""

from core.xml.persistence import xmlize, XMLpersistentObject


@xmlize('field', namespace="http://n.ethz.ch/~ggeorges/lascavesim")
class LookupField(XMLpersistentObject):

    name = None

    def __init__(self, name=""):
        self.name = name
        XMLpersistentObject.__init__(self)


@xmlize('lookup', children=(LookupField,),
        namespace="http://n.ethz.ch/~ggeorges/lascavesim")
class LookupTable(XMLpersistentObject):

    name = None

    def __init__(self, name=""):
        self.name = name
        XMLpersistentObject.__init__(self)


@xmlize('expr', textProperty="expr",
        namespace="http://n.ethz.ch/~ggeorges/lascavesim")
class ConstantExpression(XMLpersistentObject):

    expr = None

    def __init__(self, expr=""):
        self.expr = expr
        XMLpersistentObject.__init__(self)



