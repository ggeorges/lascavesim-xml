"""
-class definition

:date: 14.03.2013
:author: ggeorges
"""

import logging
import os
import sys
import core.xml.parser2
from core.xml.delayed import ActionStack
import core.cas.sympy_interface
import core.equationhandling
from modeling.configuration import Lascavesim
from simulation.graphfactory import equationgraph_factory
from simulation.equationgraph.tools import convert_to_networkx_graph


def parseFile(source):
    with open(source, "r") as resource:
        return parse(resource)


def getSystem(source, id=0):
    doc = parseFile(source)
    if not hasattr(doc, 'system'):
        raise RuntimeError("document contains no system")
    try:
        return doc.system[id]
    except KeyError:
        raise RuntimeError("no system named (or positioned at) {}".format(id))


def setup_cas():
    cas = core.cas.sympy_interface.interface()
    core.equationhandling.CASenabledObject._cas = cas


def parse(source):

    setup_cas()

    # read main configuration file
    stack = ActionStack()
    doc = core.xml.parser2.XMLparser_document(source, Lascavesim,
                                              systemId=source.name,
                                              actionStack=stack)
    stack.execute()
    if not hasattr(doc, 'object'):
        raise RuntimeError("unspecified error; parsing failed")
    return doc.object


def print_version():
    print "LaScaVeSim version 0.00"
