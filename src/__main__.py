""" LaScaVeSim command line interface
Created on 23.11.2012

:author: Gil Georges <gil.georges@lav.mavt.ethz.ch>
"""

import logging
logger = logging.getLogger(__name__)
import os
import sys
import argparse
import common
from simulation.graphfactory import equationgraph_factory
from simulation.equationgraph import EquationGraphError
from common import convert_to_networkx_graph
from simulation.conditioner import CantConditionGraphError
import simulation.model


def parseCmdArgs():

    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="a lascavesim-xml file to process")
    parser.add_argument('--just-parse', dest="check_syntax",
                        action='store_true', help='do nothing but parse the XML document')
    parser.add_argument('--dont-condition', dest="no_condition",
                        action='store_true', help='skip conditionning')
    return parser.parse_args()


def main():

    # identify
    common.print_version()

    args = parseCmdArgs()
    doc = common.parseFile(args.input)

    if args.check_syntax:
        return

    graph = equationgraph_factory(doc.system[0])

    if args.no_condition:
        convert_to_networkx_graph(graph)
        return

    try:
        graph.condition()
    except CantConditionGraphError:
        problems = []
        for component in doc.system[0].component:
            for aspect in component.aspect:
                    graph = equationgraph_factory(aspect)
                    try:
                        graph.condition()
                    except CantConditionGraphError:
                        problems.append((doc.system[0], component, aspect))

        problems = ((p[0].name, p[1].name, p[2].name, p[2]._xml_lineNumber,
                    p[2]._xml_fileId) for p in problems)
        problems = ("  * {}.{}.{} as defined on line {} of {}".format(*p)
                    for p in problems)
        problems = "\n".join(problems)
        logger.error("could not determine all causalities; the problem was "
                     "traced to (any of) the following component-aspect(s):\n" +
                     problems)
        exit(1)

    # remove loops
    from simulation.equationgraph.algebraicloop import remove_loops
    remove_loops(graph)
    assert graph.is_conditioned()

    model = simulation.model.Simulation(graph)
    simulation.model.Export2Python(model)

if __name__ == '__main__':
    if sys.version_info < (3, 0):
        reload(sys)
        sys.setdefaultencoding('utf8')
    logging.basicConfig(level=logging.INFO)
    main()
    exit(0)
