'''
Created on 22.11.2012

@author: ggeorges
'''

import re
import inspect
import core.common
import os


class XMLBaseClass(core.common.LascavesimBase):
    pass


class TextPropertyDescription(XMLBaseClass):

    """ Describes text-only nodes """

    object_property = None
    allowed_tags = None

    def __init__(self, object_property=None, allowed_tags=None):
        """ Initialize
            :param str object_property: name of the object's property to which
                the text contained in the tag shall be attributed
            :param allowed_tags: list of allowed tags, an empty list for no
                tags (tags will be ignored) or None to indicate that tags shall
                be as text
            :type allowed_tags: list or None
            """
        self.object_property = object_property
        self.allowed_tags = allowed_tags


def xmlize(tagname=None, children=None, textProperty=None, namespace=None,
           attribute_handlers=None):

    # check arguments
    if not textProperty is None and not children is None and children:
        raise RuntimeError("A class with a non-None text-property can't have "
                           "any children and vis-versa")
    if (not textProperty is None and
        not isinstance(textProperty, str) and
            not hasattr(textProperty, 'object_property')):
        raise TypeError("textProperty argument must either be 'None', a string"
                        " or a TextPropertyDescription object instance")

    # argument-free usage
    argLess = None
    if not isinstance(tagname, str):
        argLess = tagname
        tagname = None

    def actual_decorator(cls):

        # defaults that require the class
        tagName = cls.__name__ if tagname is None else tagname

        # ensure valid tag-names according to http://www.w3schools.com/xml/
        # xml_elements.asp supplemented with own practices (see docs)
        if not re.match('(?![xX][mM][lL])[a-zA-Z_][a-zA-Z0-9.\-_:]*', tagName):
            raise XMLConfigError(cls, 'invalid tag-name "{}"'.format(tagName))

        # some locally resuable code
        def setxmlattr(cls, attr, value):

            # do nothing if the attribute is already set (inheritance)
            if (hasattr(cls, attr) and not getattr(cls, attr) is None
                    and value is None):
                return

            setattr(cls, attr, value)

        # add and set class properties accordingly
        setxmlattr(cls, '_xml_tagName', tagName)
        setxmlattr(cls, '_xml_namespace', namespace)

        # set children
        child_classes = [] if children else None
        for child in (children if children else []):

            # different variable name / compound collection?
            if isinstance(child, tuple):

                # first is the variable name
                detailed = list(child)
                setxmlattr(cls, str(detailed.pop(0)),
                           XMLpersistentListCreator(detailed))

                # subsequent elements are allowed types in the list
                child_classes += detailed

            else:

                # don't create a list for lose elements, but keep them in the
                # allowed child list (otherwise the parser won't recognize
                # them)
                if not hasattr(child, 'setLoseElementContext'):

                    # variable name = class name
                    setxmlattr(cls, child._xml_tagName,
                               XMLpersistentListCreator([child]))

                child_classes.append(child)
        setxmlattr(cls, '_xml_children', child_classes)

        # set the text property variables
        setxmlattr(cls, '_xml_textProperty', textProperty)

        # are there foreign attributes?
        setxmlattr(cls, '_xml_attributeHandlers', attribute_handlers)

        return cls

    # enable argument-free access
    if argLess is None:
        return actual_decorator
    return actual_decorator(argLess)


class XMLpersistentListCreator(XMLBaseClass):

    classes = None

    def __init__(self, classes):
        self.classes = tuple(classes)

    def create(self, parent):
        return XMLpersistentList(parent, self.classes)


class XMLpersistentObject(XMLBaseClass):

    _xml_tagName = None
    _xml_namespace = None
    _xml_children = None
    _xml_textProperty = None
    _xml_lineNumber = None
    _xml_fileId = None
    _linkBack = None
    _list_index = None
    name = None

    def __init__(self):

        # make sure the list is initialized
        def isolate(el):
            return isinstance(el, XMLpersistentListCreator)

        self._list_index = {}

        # find corresponding list
        for prop, creator in inspect.getmembers(self.__class__, isolate):
            lst = creator.create(self)
            setattr(self, prop, lst)
            for cls in creator.classes:
                self._list_index[cls] = lst

    def isVirtual(self):
        return False

    def _log_message(self, msg, *args):
        """ Include file identifier and line number to log messages """
        return "{} line {}: {}".format(str(self._xml_fileId),
                                       str(self._xml_lineNumber),
                                       str(msg).format(*args))

    def adopt(self, obj):

        # recognize lose elements; they are now treated by the parser, so we
        # raise an exception to quickly recognize any problem that could
        # occur in future releases when this would change again

        # find a list
        try:
            lst = self._list_index[obj.__class__]
        except KeyError:
            raise RuntimeError("{}-object does not accept {} type objects",
                               self.__class__.__name__,
                               obj.__class__.__name__)
        if hasattr(self, 'preAdoptionHook'):
            self.preAdoptionHook(obj)
        lst.append(obj)

    def getXMLproperties(self):
        """ Returns any property with the same name as an __init__ argument """

        func_arg_names = inspect.getargspec(self.__init__).args
        func_arg_names.remove('self')

        ret = {}

        for arg_name in func_arg_names:

            if hasattr(self, 'arg_name'):
                ret[arg_name] = getattr(self, arg_name)

        return ret

    def getParent(self):
        if self._linkBack is None:
            return None
        if not hasattr(self._linkBack, 'getParent'):
            return None
        return self._linkBack.getParent()

    def getParents(self, limit=None):
        parents = []
        me = self.getParent()
        while not me is None:
            parents.insert(0, me)
            # allow limit to filter
            if me == limit or (hasattr(me, '_xml_tagName') and
                               me._xml_tagName == limit):
                return parents
            me = me.getParent()
        return parents

    def getMyXPathIndex(self):
        if self._linkBack is None or not hasattr(self._linkBack, 'index'):
            return None

        return self._linkBack.xpath_index(self)

    def getMyIndex(self):
        if self._linkBack is None or not hasattr(self._linkBack, 'index'):
            return None

        return self._linkBack.index(self)

    def getXPath(self, parent=None):

        # first get the objects
        path = self.getParents(parent)
        path.append(self)

        # get name or index if there is none
        def _process(branch):

            for obj in branch:

                # local copy
                name = obj.name

                if name is None:
                    # if there is no name, try to get an index
                    idx = obj.getMyXPathIndex()
                    name = "[{}]".format(idx) if not idx is None else ""
                else:
                    # name-query
                    name = "[@name='{}']".format(obj.name)

                # add tag-name
                yield obj.__class__._xml_tagName + name

        # convert them to names
        return "/" + ("" if parent is None else "/") + "/".join(_process(path))

    def getName(self, limit=None):
        return ".".join(str(s.name) for s in (self.getParents(limit) + [self]))

    def resolveXPath(self, xpath):
        raise NotImplementedError()
        # TODO: may come in handy


class XMLpersistentList(list):

    _parent = None
    _memberClass = None

    def __init__(self, parent=None, enforceMemberClass=None):
        """"Initializes a NamedList instance

        In addition to thnormal behavior of list.__init__, this methods sets
        this object's 'parent' attribute so that bonds over NamedList-s are
        navigable in both directions.

        :param object parent: any object, serving as host to the list
        :param object enforceMemberClass: use this class as default
        """
        list.__init__(self)

        self._parent = parent

        # make sure enforceMbmerClass is iterable or None
        if not enforceMemberClass is None:
            if not isinstance(enforceMemberClass, (list, tuple)):
                enforceMemberClass = (enforceMemberClass,)
            self._memberClass = enforceMemberClass

    def getParent(self):
        return self._parent

    def add(self, obj):
        self.append(obj)

    def append(self, obj):
        """Adds 'obj' to the list

        In addition to the usual behavior of list.append, this method sets a
        reference in obj's _linkBack field so that the link is navigable in
        both directions.

        :param NamedEntity obj: object to be linked
        """
        if not hasattr(obj, '_linkBack') or not hasattr(obj, 'name'):
            raise TypeError('obj must be a subclass of XMLpersistentObject')

        # make sure key does not exist
        try:
            self.getPositionByName(obj.name)
        except KeyError:
            list.append(self, obj)
            obj._linkBack = self
        else:
            raise XMLConfigError(obj.__class__, "duplicate name error")

    def __del__(self):
        """Removes all associated elements

        This ensures that when the list is deleted, so are all its members. In
        other words, list members die when the list dies, even if they are
        still referenced somewhere else."""
        for el in self:
            del el

    def getByName(self, key):
        """Finds member named 'key'"""
        key = str(key)
        for el in self:
            if el.name == key:
                return el
        raise KeyError()

    def getPositionByName(self, key):
        """Finds position of member named 'key'"""
        key = str(key)
        for idx, el in enumerate(self):
            if el.name == key:
                return idx
        raise KeyError()

    def __setitem__(self, key, value):
        """Sets value by name or position"""
        try:
            # is it an integer? then normal lookup
            list.__set__(int(key), value)
        except (ValueError, TypeError):
            # else, let's see if there is a matching name
            self[self.getPositionByName(key)] = value

    def __getitem__(self, key):
        """Gets value by name or posiiton"""
        try:
            # is it an integer? then normal lookup
            return list.__getitem__(self, int(key))
        except (ValueError, TypeError):

            # else, let's see if there is a matching name
            return self.getByName(key)

    def xpath_index(self, obj):
        """ Returns the position within one tag-name group

        If a list acceps only one _memberClass, this is the same as index. If
        there is more than one type though, this gives the position within a
        particular tag, e.g. the index() of "tag2" below is 2, but its
        xpath_index is 1:

            <tag1 /> -> index:0, xpath_index: 1
            <tag1 /> -> index:1, xpath_index: 2
            <tag2 /> -> index:2, xpath_index: 1
            <tag1 /> -> index:3, xpath_index: 3

        Note that XPath starts counting at 1.

        :param obj: the object we are looking for"""

        if self._memberClass is None or len(self._memberClass) == 1:
            return self.index(obj) + 1

        counter = int(0)
        for el in self:
            counter += int(el._xml_tagName == obj._xml_tagName)
            if el == obj:
                return counter
        raise KeyError()


class XMLConfigError(core.common.LascavesimException):

    _context = None

    def __init__(self, context, message, *args):
        self.message = message.format(*args)
        self._context = context

    def log(self):
        if hasattr(self._context, 'log_error'):
            self._context.log_error(self.message)
        else:
            print "missing context; got message -> {}".format(self.message)


class XMLAttributeHandler(XMLBaseClass):

    """ Allows to add actions to attributes

    :py:class:`XMLAttributeHandler` allows to trigger events using attributes,
    similarly to what delayed actions do for tags. This is primarily meant to
    deal with arguments from foreign namespaces, but can be applied to any
    argument. To trigger events, an array of :py:class:`XMLAttributeHandler`
    instances (one or more) has to be passed to :py:func:`xmlize`'s
    'attribute_handlers' argument. Note that contrary to
    :py:class:`DelayedAction`, attribute handlers are fired immediately after
    object initialization (the document may not be fully loaded).

    This class is meant as a base-class for specific handlers. It can be
    instanciated though, and missing an implementation of any of the three
    "trigger"-functions, it will simply do nothing. It could therefore be used
    as "ignore"-action to suppress error-messages that would otherwise occur
    on things such as schema definitions.

    The three supported trigger-methods are:
        * execute(self, object, value): is executed immediately after object
            creation, right before the object is added to the hierarchy (i.e.
            there is no parent yet). 'value' holds the value of the original
            attribute, while 'object' is a reference to the created object.
        * execute_aposteriori(self, object, value): same as execute, except
            that it is executed immediately after the object was added to the
            hierarchy, i.e. there should be a parent (no guarantees)
        * parserModifier(self, parser, value): same as execute, but instead of
            the new created object, a reference to the parser is passed along.
            This allows to modify the parser's behavior (at the current level)
            and in particular trigger the 'sub-document' mode
            (see :py:mod:`core.xml.parser2`).
    """

    my_attribute = None,
    my_namespace = None,
    my_value = None

    def __init__(self, namespace, attribute_name):
        self.my_attribute = attribute_name
        self.my_namespace = namespace

    def resolvedXMLname(self):
        return (self.my_namespace, self.my_attribute)


class XMLImportAttributeHandler(XMLAttributeHandler):

    """ A specialized attribute handler to insert foreign content """

    def __init__(self, namespace, attribute="import"):
        super(XMLImportAttributeHandler, self).__init__(namespace, attribute)

    def parserModifier(self, parser, value):

        fpath, xpath = self._process_xpointer(value)
        if not os.path.isabs(fpath):
            pwd = os.path.dirname(parser.getFileId())
            fpath = os.path.join(pwd, fpath)

        resource = None
        try:
            resource = open(fpath, "r")
        except IOError:
            self.log_error("can't open {}; tag definition may be incomplete",
                           value)
        else:
            self.log_info("importing {}", value)
            parser.enterSubDocumentMode(resource, xpath)
        finally:
            if hasattr(resource, 'close'):
                resource.close()

    def _process_xpointer(self, pointer):

        # evaluate assuming xpointer syntax
        r = re.compile('([\w\-. {}:/]*)#xpointer\((.*)\)')
        matches = r.match(str(pointer))
        if matches is None:
            raise XPointerSyntaxError(pointer)
        fpath, xpath = matches.groups()
        fpath = os.path.normpath(fpath)
        if not os.path.exists(fpath):
            raise RuntimeError("can't locate {}".format(fpath))
        return (fpath, xpath)


class XPointerSyntaxError(core.common.LascavesimException):
    pass


class XMLVirtualEntity(XMLpersistentObject):

    def isVirtual(self):
        return True
