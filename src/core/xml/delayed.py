"""
Implements delayed actions

With complex data structures it may become possible to perform actions on parts
of the document that have not been read in yet. A concrete example is linking,
i.e. referencing objects defined elsewhere. The pointer to the actual object
is only available once the object was read in. Delayed actions allow to stack
a method call away for later, and thereby provide a flexible framework to
create complex  data trees.

Technically, delayed actions are persitent objects like any other, but they
are not stored permanently in the parent object, i.e. they are stacked away
centrally and then processed once the document has been completely read. After
this, they are removed from memory. If there will ever be an XML export
function, it has to generate the appropriate XML without the support of the
actual object.

:date: 10.01.2013
:author: ggeorges
"""

from persistence import XMLpersistentObject, XMLConfigError
from core.xml.persistence import XMLVirtualEntity


class ActionStack(list):

    def execute(self):
        for action in self:
            try:
                action.executeDelayedAction()
            except XMLConfigError as e:
                e.log()

    def append(self, object, parent):
        if not hasattr(object, 'executeDelayedAction'):
            raise TypeError("{} this is not a delayed action".format(object))
        list.append(self, object)


class DelayedActionInterface(XMLVirtualEntity):
    """
    classdocs
    """

    context = None

    def __init__(self):

        # store for later
        super(DelayedActionInterface, self).__init__()

    def preInjectionHook(self, context, parser=None):
        self.context = context

    def executeDelayedAction(self):
        raise NotImplementedError("virtual base class")

    def executeAction(self):
        raise NotImplemented("use 'executeDelayedAction' instead")


class DelayedAction(DelayedActionInterface):
    """
    classdocs
    """

    # tell @xmlize to treat us specially

    method = None
    args = None
    kwargs = None

    def __init__(self, method_name, *args, **kwargs):

        # store for later
        self.method_name = method_name
        self.args = args
        self.kwargs = kwargs
        DelayedActionInterface.__init__(self)

    def executeDelayedAction(self):

        # make sure the attribute exists
        try:
            action = getattr(self.context, self.method_name)
        except AttributeError:
            raise RuntimeError("no attribute '{}'".format(self.method_name))

        # is it callable
        if not hasattr(action, '__call__'):
            raise RuntimeError("attribute {} not callable".
                               format(self.method_name))

        # and perform
        action(*self.args, **self.kwargs)


class ObjectReference(DelayedActionInterface):

    reference = None

    def __init__(self, reference, parent_tag_name, child_names):

        self.parent_tag_name = parent_tag_name
        self.child_names = child_names

        # set the reference straight
        reference = str(reference).split('.')
        if len(reference) != len(child_names):
            raise RuntimeError('invalid reference: {}'
                               .format(".".join(reference)))

        DelayedActionInterface.__init__(self)

        self.reference = reference

    def executeDelayedAction(self):

        # local copies
        relativeTo = self.context.getParents(self.parent_tag_name).pop(0)

        # loop to destination
        for prop, key in zip(self.child_names, self.reference):
            if not hasattr(relativeTo, prop):
                self.log_error("{} has no {}-property",
                               relativeTo._xml_tagName, prop)
                raise RuntimeError("structural problem")

            # throws a KeyError if the reference is wrong
            try:
                relativeTo = getattr(relativeTo, prop)[key]
            except KeyError:
                raise XMLConfigError(self, "invalid signal reference '{}': "
                                           "there is no {} named '{}' in the "
                                           "{} named '{}' (defined on line {})",
                                     ".".join(self.reference), prop, key,
                                     relativeTo._xml_tagName,
                                     relativeTo.name,
                                     relativeTo._xml_lineNumber)

        # run the link-function
        self.link(self.context, relativeTo)

    def link(self, calling, referenced):
        raise NotImplementedError()


class TwoPartObjectReference(ObjectReference):

    def __init__(self, reference, root, parent_tag_name, child_names):

        # set the reference straight
        reference = str(reference)
        if not root is None:
            if reference.count('.') > 1:
                raise RuntimeError('invalid reference')
            if reference.count('.') == 1 and not root in reference:
                raise RuntimeError('parent name mismatch')
            if not '.' in reference:
                reference = "{}.{}".format(root, reference)

        # move on
        ObjectReference.__init__(self, reference, parent_tag_name, child_names)

