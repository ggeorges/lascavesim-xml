""" Recreates a class (actually object)-tree from an XML file

The actual processing, or more to the point, interpreting of XML tags occurs
within the :py:class:`modeBase`-children. At all times, one of these is
linked to the eXpat parser. Whenever the latter encounters an XML entity
it fires one of its call-back hooks, linked to the current
py:class:`modeBase`-subclass instance. This usually causes a mode-change,
meaning that another py:class:`modeBase`-subclass instance links to the
parser and processes the transmitted data. Given the hierarchical nature of
XML, the result is a dynamic cascade of py:class:`modeBase`-subclass
instances that mirror the document structure.

Processing is controlled by "xmlized"-classes. These are either subclasses
of :py:class:`XMLized` or ordinary classes with the :py:func:`xmlize`-
decorator applied to them. Those classes have an interface specifying at
least a tag-name, representing an instance of the class within an XML
document. By specifying legal child-classes, "xmlized"-class instances can
be chained in the hierarchy described by the XML document. And vice-vers,
this gives the parser the information, which tags to look for.

Created on 20.11.2012
:author: ggeorges
"""

import inspect
import re

import logging
logger = logging.getLogger(__name__)


class ggException(Exception):

    def __str__(self):
        return self.message


class TextPropertyDescription(object):
    """ Describes text-only nodes """

    object_property = None
    allowed_tags = None

    def __init__(self, object_property=None, allowed_tags=None):
        """ Initialize
            :param str object_property: name of the object's property to which
                the text contained in the tag shall be attributed
            :param allowed_tags: list of allowed tags, an empty list for no
                tags (tags will be ignored) or None to indicate that tags shall
                be as text
            :type allowed_tags: list or None
            """
        self.object_property = object_property
        self.allowed_tags = allowed_tags


class modeBase(object):
    """ Common basis for all xml-interpreter nodes

    The :py:class:`modeBase`-class is a XML interpreter meant to process events
    fired by an eXpat parser, parsing an XML document. Actually it is a kind of
    intermediate layer between XML-parsing interpretation as it also tracks the
    position within the document hierarchy. But strictly technically speaking,
    :py:class:`modeBase` converts the feedback from an actual parser (eXpat)
    into something meaningful; in this case a set of interrelated class
    instances.

    Indeed eXpat works by firing call-back functions. :py:class:`modeBase` and
    each of its subclasses provide implementations for such functions. By
    linking different :py:class:`modeBase`-subclasses to the parser, the
    behavior of the parser can be changed. This is done on a contextual base:
    each :py:class:`modeBase`-subclass is monitoring the eXpat feedback for
    specific events, such as e.g. the opening or closing of a certain tag. Once
    that happens, action such as calling a call-back function of their own can
    be taken. Also control is deferred to another
    :py:class:`modeBase`-subclass.

    Hence :py:class:`modeBase`-subclasses are not only a set of subroutines but
    they represent the current state (mode) of the XML-interpreter/parser.
    Be aware though that as objects, they have states of their own and may be
    state-machines themselves.

    By hierarchically arranging :py:class:`modeBase`-subclasses through their
    :py:attr:`parent_mode` attribute, the position on a branch within the
    hierarchical XML format can be tracked. For each child-
    node, a new mode-object takes over, while keeping a link back to its parent
    It will then process sub-elements of its own in the same way, until
    eventually, usually upon encountering a closing tag, it will defer control
    back to its parent.

    Currently there are 4 different modes, explaind in detail in the respective
    class documentation. Basically, they reflect three base types of nodes (
    those without any content, those bordering only text and those with
    children of their own) and a "skip"-mode, designed to skip all content
    within unknown resp. misplaced nodes.

    Context-switching between nodes is controlled by probably the single most
    important method of this class: :py:meth:`getNextMode`.

    The interpreter is configured by external classes, that need to implement
    the "xmlize" interface. This basically means that for each tag in the
    document, there should be a class having an _xml_tagName attribute set to
    the corresponding tag-name, along with a list of admissible sub-elements/
    classes in its _xml_children attribute. This allows the interpreter to find
    the correct class, from which the next mode can be derived. Once data-
    gathering has finished, it will call the class-constructor by passing along
    the detected XML-attributes as arguments."""

    _parser = None
    parent_mode = None
    my_tagName = None
    namespaces = None
    allowed_children = None

    # data management
    my_tagName = None
    my_namespace = None
    my_class = None
    my_object = None
    my_attributes = None

    def __init__(self, parent_mode, my_class, attributes, localNamespaces):
        """ Initializes modeElementWithChildElements

        Common initializer, creating the:
            * link back to the calling parent mode object
            * storing the detected class-object
            * storing the attributes passed along by eXpat for later processing

        :param modeBase parent_mode: calling instance
        :param classobj my_class: class of the external object linked to the
            current tag, as detected by the calling :py:class:`modeBase`-child
        :param dict attributes: XML-attributes stored in a dictionary, i.e.
            keys correspond to attributes names and values to attributes values
        """

        self.parent_mode = parent_mode

        # check arguments
        if (not hasattr(my_class, '_xml_tagName')
            or not hasattr(my_class, '_xml_namespace')):
            raise TypeError('myself must point to an xmlized class')

        # copy arguments
        self.my_tagName = my_class._xml_tagName
        self.my_namespace = str(my_class._xml_namespace)
        self.namespaces = localNamespaces
        self.my_class = my_class

        # extract handled attributes and tackle namespace-attributes
        self.setAttributes(attributes)
        if hasattr(my_class, '_xml_children') and my_class._xml_children:
            self.allowed_children = my_class._xml_children

    # working with delayed actions

    def setAttributes(self, attributes):

        # get handled attributes
        handled_attributes = set()
        if (hasattr(self.my_class, '_xml_attributeHandlers') and
            hasattr(self.my_class._xml_attributeHandlers, '__iter__')):
            handled_attributes = set((s.resolvedXMLname(), s) for s in
                                     self.my_class._xml_attributeHandlers)

        self.my_attributeHandlers = []
        self.my_attributes = []
        for key, attr in attributes.iteritems():

            # get attribute as namespace / attribute-name pair
            attribute = (self.my_namespace, key)
            if ":" in key:
                try:
                    attribute = self.resolveXMLname(key)
                except KeyError:
                    logger.info("{} will be treated as attribute name within "
                                "the parent tag's name-space".format(key))

            # is there a handler?
            for xml_name, handler in handled_attributes:
                if xml_name == attribute:
                    self.my_attributeHandlers.append((handler, attr))
                    break
            else:
                if attribute[0] != self.my_namespace:
                    logger.info("Line {}: no matching attribute-handler for {}"
                                "; will be treated as {} of namespace {}".
                                format(self.getLineNumber(), key, key,
                                       self.my_namespace))
                    self.my_attributes.append((key, attr))
                else:
                    self.my_attributes.append((attribute[1], attr))
        self.my_attributes = dict(self.my_attributes)

        # local copy
        self.allowed_children = []

    def addToActionStack(self, action):
        self.getDocument().actionStack.add(action)

    #------handling the parser-------------------------------------------------

    def enterSubDocumentMode(self, document):
        mode = modeSubDocument(self, self.getDocument().allowed_children[0],
                               document)

    def getParser(self, parser=None):
        """ Returns the underlying eXpat parser instance

        This function provides singleton access to  the eXpat parser instance
        driving the interpreter. Note that his means that there may only be
        one parser instance at each time.

        If the parser attribute is specified, the passed parser instance (or
        any content for that matter) will be stored in the internal parser
        reference. this can be used to initialize the interpreter externally.

        :param xml.parser.expat.xmlparser parser: optional; when provided,
            overrides the internal parser reference. Warning, there is no type-
            checking here!
        :returns: xml.parser.expat.xmlparser
        """

        if modeBase._parser is None:
            modeBase._parser = parser
        return modeBase._parser

    def getLineNumber(self):
        """ Returns the line-number the parser is currently at

        For diagnostics and error-reporting, this function provides a
        convenient access to the parser's 'CurrentLineNumber' attribute.

        :returns: int
        """

        return self.getParser().CurrentLineNumber

    def takeOver(self):
        """ Attach the linked mode to the parser

        The function overrides the parser's call-back functions with the bound
        object's handler methods.The function must be explicitly called on new
        :py:class:`baseMode`-subclass instances for them to be able to process
        child-elements of any kind (otherwise eXpat events are directed to the
        parent).
        """

        logging.debug("mode {}".format(self.getModeStack()))

        parser = self.getParser()

        def fct(prefix, uri):
            print "NOW: {} -> {}".format(prefix, uri)
        def fct2(prefix):
            print "now leaving {}".format(prefix)
        def fct3(context, base, systemId, publicId):
            print context, base, systemId, publicId

        # adjust handlers to point to current class
        parser.SetBase("http://n.ethz.ch/~ggeorges/lascavesim")
        parser.StartElementHandler = self.startElement
        parser.EndElementHandler = self.endElement
        parser.CharacterDataHandler = self.characterData
        parser.StartCdataSectionHandler = self.cdataBegin
        parser.EndCdataSectionHandler = self.cdataEnd
        parser.CharacterDataHandler = self.characterData
        parser.StartNamespaceDeclHandler = fct
        parser.EndNamespaceDeclHandler = fct2
        parser.ExternalEntityRefHandler = fct3

    #------eXpat interface - default implementations---------------------------

    def startElement(self, name, attributes):
        """ Handles opening XML tags

        This function is called by eXpat whenever it encounters an opening
        XML-tag. The name of the tag and its attributes are passed along as
        arguments.

        In :py:class:`modeBase` :py:meth:`startElement` is an abstract method,
        meaning it will raise an :py:exc:`NotImplementedError` when called. In
        other words, every subclass of :py:class:`modeBase` does at least
        have to implement the :py:meth:`startElement`, as the very large share
        of actions need to be taken upong detecting a new opening tag, but
        these actions vary greatly by interpreter mode.

        :param str name: tag-name, e.g. <tag> yields name="tag"
        :param dict attributes: a dict where the keys correspond to attribute
            names and the values to attribute values, <tag test="value"> yields
            { "test": "values" }"""

        raise NotImplementedError('startElement is abstract')

    def endElement(self, name):
        """ Handles closing XML tags

        This function is called by eXpat whenever it encounters an opening
        XML-tag. The name of the tag is passed along as attribute. In most
        cases, the only closing tag of interest is the currently open tag being
        observed.

        The default behavior (the :py:class:`modeBase`-implementation) is to
        ignore any tags except the ones named :py:attr:`my_tagName`. If the
        latter is encountered, control is passed back to the parent mode.

        This should be sufficient for any nodes that accept either no content
        at all, or only other tags. For text-only and mixed nodes, the closing
        tag must be awaited, i.e. the:py:class:`modBase`'s implementation of
        :py:meth:`endElement` should be overwritten.

        :param str name: name of the closing tag, e.g. </tag> yields name="tag"
        """

        # get namespace
        xmlns, name = self.resolveXMLname(name)

        # check for matching tag AND namespace
        if name == self.my_tagName and xmlns == self.my_namespace:
            self.exit()

    def cdataBegin(self):
        """ Handles an opening CDATA block

        By default, an exception is risen as CDATA blocks are only allowed
        within specialized nodes, that require their own :py:class:`modeBase`-
        subclass anyways."""

        raise XMLParserError(self.getLineNumber(), "CDATA not allowed here")

    def cdataEnd(self):
        """ Handles closing of CDATA blocks

        The default behavior is to ignore closing CDATA blocks."""
        pass

    def characterData(self, data):
        """ Handles incoming character data

        As there is a lot of whitespace being piped in, the most efficient
        thing is to ignore characterData, unless needed by specialized subclass
        instances."""
        pass

    #------deal with namespaces -----------------------------------------------

    def resolveXMLname(self, name, localNamespace=None):
        prefix, name = splitIntoNamespacePrefixAndName(name)
        xmlns = self.getNamespace(prefix, localNamespace)
        return (xmlns, name)

    def getNamespace(self, prefix, localNamespace=None):
        """ Returns the namespace of a tag or attribute

        Given a tag-name such as "ns:tag" or "tag" resp. an argument name such
        as "ns:arg" or "arg", this function checks for the corresponding
        namespace. If no prefix explicitly defines a namespace, the function
        goes back recursively through the mode stack until it finds one with
        a 'xmlns'-namespace defined; i.e. if no namespace is given, the
        default namespace of the parent is inherited.

        :param str prefix: prefix name to check for
        :param XMLnamespaceDefinitions localNamespace: locally defined
            namespaces resp. prefixes, that override the current scpe
        :returns: str, the name-space qualifier
        :raises: KeyError
        """

        # namespaces not defined? then the answer is obvious...
        if not localNamespace is None:
            try:
                return localNamespace[prefix]
            except KeyError:
                pass

        # no local match? check the branch; if there is no namespace definition
        # in the branchm then the answer is trivial
        if self.namespaces is None:
            raise KeyError()

        try:
            return self.namespaces[prefix]
        except KeyError:

            # we reached the document node: either the prefix does not resolve
            # to anything or it is xmlns no default namespace was defined
            # -> the caller is supposed to detect this and complain accordingly
            if not hasattr(self.parent_mode, 'getNamespace'):
                raise KeyError()

            # inheritance: check is defined earlier (as long as xmlns is not
            # defined it can be inherited;  this also allows individual
            # branches to have a different default namespace)s
            return self.parent_mode.getNamespace(prefix)

    def handleNamespaces(self, name, attributes):
        """ Performs the standard routine for handling namespaces

        The function first splits 'name' (supposed to be the 'raw'-tag-name
        obtained from the parser) into its namespace-prefix and an actual tag
        name; then processes the attribute-dict using
        :py:class:`XMLnamespaceDefinitions`; then calls :py:meth:`getNamespace`
        with the previously obtained namespace-definitions against the current
        mode to resolve the namespace to its URI. It returns the name, xmlns
        and namespace-dict for further processing. """

        # get name and prefix
        prefix, name = splitIntoNamespacePrefixAndName(name)

        # process local namspace definitions
        namespaces = XMLnamespaceDefinitions(attributes)

        # resolve the namespace
        xmlns = self.getNamespace(prefix, namespaces)

        return (xmlns, name, namespaces)

    #------interpreter---------------------------------------------------------

    def getNextMode(self, name, attributes, namespace, localNamespaces):
        """ Returns the sub-mode corresponding to a given child-element.

        This method controls the basic control flow of the interpreter. Based
        on its :py:attr:`allowed_children` child-class collection, it detects
        permissible child-elements, by finding a matching class to the tag-name
        passed in via the :attr:`name` argument. If there is none, it returns
        None.

        Otherwise, it will use the _xml_* properties of the xmlize interface
        to determine which mode applies, i.e. what kind of node we are dealing
        with: basically it may either accept children or not. In the latter
        case the node may either accept no content at all, or it may be
        "text-only", meaning that the tags border some text that is supposed to
        be stored in one of the final POPO's properties (note that additionally
        the text-gatherer can be configured to also include tags in the final
        text). Note that there is a fourth possibility, not included here: when
        no matches have been round, i.e. this method returns None, the external
        logic may switch to "modeSkipTag", which just skips until it finds the
        closing tag to the unknown entity. This is done externally to allow the
        individual mode-classes to decide how to handle unknown entities.

        The function returns a fully configured :py:class:`modeBase`-subclass
        instance. In order for it to process the content it encompasses, the
        :py:meth:`takeOver` method must be called on it explicitly. For most
        cases this will have to be done in the :py:meth:`startElement` method.

        Note that structurally, this function, although - in the current
        implementation exclusively used within :py:meth:`startElement` - is not
        included as it is the same for all modes. This allows overwriting
        :py:meth:`startElement` without having to rewrite the main context
        switching logic every time.

        :param str name: name of the discovered XML-entity (from eXpat)
        :param dict attributes: XML-attributes dictionary (from eXpat)
        :param str namespace: resolved namespace of the current element
        :param dict localNamespaces: dictionary with local namespace
            definitions to be passed along to the next mode
        :returns: :py:class:`modeBase`-subclass or None
         """

        # get acceptable tags
        for cls in self.allowed_children:

            # skip non-matching tags and name-spaces
            if not hasattr(cls, '_xml_tagName'):
                continue
            if cls._xml_tagName != name:
                continue
            if (hasattr(cls, '_xml_namespace')
                and cls._xml_namespace != namespace):
                continue

            # text-property comes first as textMode=2 types can have child-
            # nodes of their own
            if (hasattr(cls, '_xml_textProperty') and (
                  isinstance(cls._xml_textProperty, TextPropertyDescription)
                  or isinstance(cls._xml_textProperty, str))):

                return modeElementWithOnlyText(self, cls, attributes,
                                               localNamespaces,
                                               cls._xml_textProperty)

            # TODO: textNodes with child-nodes -> HTML-like documentation
            # mixing text with tags for format directives; for now this
            # will be done by piping the "includeTags"-imported XML-text
            # to an XSLT-engine

            # no text-property, but accepts children:
            if hasattr(cls, '_xml_children') and cls._xml_children:

                # continue normally
                return modeElementWithChildren(self, cls, attributes,
                                               localNamespaces)

            # no text-property, no children
            return modeContentFreeElement(self, cls, attributes,
                                          localNamespaces)

        return None

    def exit(self):
        """ Switch control back to the parent mode

        If :py:meth:`takeOver` gives control to a foreign mode, this function
        does the opposite: when called on the current mode, it returns control
        to the mode referenced in :py:attr:`parent_mode`.

        It does so by calling :py:meth:`takeOver` on the parent mode.

        .. note: exit must never be called on anything but the final mode in
            the mode stack, i.e. the mode that no other mode's
            :py:attr:`parent_mode` field points to
        """

        # forward along the created object
        self.parent_mode.addChildObject(self.my_object)

        # remove reference to parent and autodestruct
        parent = self.parent_mode
        self.parent_mode = None
        del self

        # give control to the parent
        if hasattr(parent, 'takeOver'):
            parent.takeOver()

    def addChildObject(self, obj):
        """ Receives child-mode handler's object

        Upon exiting, (i.e. the default behavior of :py:meth:`exit`), all
        :py:class:`modBase`-subclasses with an unmodified :py:meth:`exit`
        method call this method to pass along the created object instance. This
        reference is then passed to the current object, so that it may append
        this child."""

        if obj is None:
            return

        # do this here (could be done in the XMLpersistentObject's adopt method
        # but delayed actions are only created by xml, so having the parser
        # handle them has the advantage of having a convenient location to
        # store and later automatically execute them
        if hasattr(obj, 'setLoseElementContext'):
            obj.setLoseElementContext(self.my_object)
            self.addToActionStack(obj)
            return

        # process normally
        if not hasattr(self.my_object, 'adopt'):
            logger.error("{}-object has no adopt method; object lost".
                         format(self.my_object.__class__.__name__))
            return

        self.my_object.adopt(obj)

    #------reporting-----------------------------------------------------------

    def __str__(self):
        return self.__class__.__name__ + "({})".format(self.my_tagName)

    def getModeStack(self):
        """ Gets a textual representation of the current interpreter state

        The function recursively traverses the mode-stack. That's the virtual
        entity of the chain of :py:class:`modeBase`-subclass instances, linked
        to one another via their :py:attr:`parent_mode` attribute. It uses the
        compiles all the names into a string of the format: "str(node) >
        str(node) ..."
        """

        me = self
        text = [str(me)]
        while not me.parent_mode is None:
            me = me.parent_mode
            text.insert(0, str(me))
        return " > ".join(text)

    def getDocument(self):
        if self.parent_mode is None:
            return self
        return self.parent_mode.getDocument()

    def initMyClass(self):
        """ Initializes a new instance of the associated class

        As :py:class:`modebase`-subclasses are always tied to a defining class
        object, this function is the common way of initializing a new object
        instance.

        :returns: a new instance of the type :py:attr:`my_class`
        """

        # get arguments
        args = self._getInitArguments()

        # inform the user
        logger.debug("calling {}.{}.__init__(*args) with args={}".
                    format(self.my_class.__module__, self.my_class.__name__,
                           str(args)))

        # create new object instance
        self.my_object = self.my_class(*args)

        # for reporting
        if hasattr(self.my_object, '_xml_lineNumber'):
            self.my_object._xml_lineNumber = self.getLineNumber()

        # execute attribute handlers
        for handler, value in self.my_attributeHandlers:
            if hasattr(handler, 'execute'):
                handler.execute(self.my_object, value)
            if hasattr(handler, 'parserModifier'):
                handler.parserModifier(self, value)

    def _getInitArguments(self):
        """ Converts XML arguments to an argument-list for __init___

        This function uses reflection (mod. inspect) to detect constructor
        arguments matching the detected xml-attributes of a tag. It then
        converts them to a list that will be unpacked -> *args.

        The function is directly invoked by :py:meth:`initMyClass` and is as
        such only a refactored code fragment of that function. The reason for
        this is allowing :py:meth:`initMyClass` to be easily overridden, while
        the :py:meth:`_getInitArguments` is likely to remain the same.

        :returns: list
        """

        # local copies
        cls = self.my_class
        attributes = self.my_attributes

        # check class
        assert hasattr(cls, '__init__')

        # get function arguments
        arg_specs = inspect.getargspec(cls.__init__)
        assert isinstance(arg_specs, inspect.ArgSpec)

        # get argument names
        func_arg_names = arg_specs.args
        func_arg_names.remove('self')

        # get default values
        arg_defaults = arg_specs.defaults

        # compute once
        n_mandatory = len(func_arg_names) - (len(arg_defaults) if arg_defaults
                                             else 0)
        # dim
        args = []

        # find acceptable args
        for idx, arg_name in enumerate(func_arg_names):

            # if the arg exists, use it; _ may represent ':' or '-'
            for _name in (arg_name, arg_name.replace('_', '-'),
                          arg_name.replace('_', ':')):

                try:
                    args.append(attributes[_name])
                    del attributes[_name]
                    break
                except KeyError:
                    continue

            else:
                # otherwise, brake if it is mandatory (don't just simply skip
                # because then subsequent variables couldn't be set)
                # note that this implementation will not raise an exception if
                # a mandatory parameter is set to ""; the __init__ of the
                # respective class should do that then
                if idx + 1 > n_mandatory:
                    args.append(arg_defaults[idx - n_mandatory])
                else:
                    raise XMLParserError(None, '{}-attribute on {}-tags is '
                                         'mandatory'.format(arg_name,
                                                            cls._xml_tagName))

        # warn of unused attributes
        if attributes:
            XMLParsingProblem(self.getLineNumber(),
                             "The following unknown "
                             "attribute(s) of the {}-tag was/were ignored: "
                             "{}".format(cls._xml_tagName,
                                          ", ".join(attributes.iterkeys())))

        # TODO: reintroduce the 'id' field with uniqids
        return args


class modeDocument(modeBase):
    """ Finding the document root element

    In this mode, the interpreter scans for the document root element passed
    along as first argument to the constructor. If the specified node is
    nowhere to be found, it fails with an :py:exc:`XMLParserError` .

    Besides fatal errors from external functions or the parser itself (XML
    syntax or structural errors), this is the only case in which the parser
    fatally break. Indeed, any valid XML document may have but one document
    root element with only comments, processing instructions and possibly
    doc-type specifications as sibling-elements alongside. So contrary to
    "ordinary" nodes, skipping non-matching nodes does not help.

    The implementation is very similar to the
    :py:class`modeElementWithChildren`class in that it scans for child nodes,
    but it allows only one (the root note), it permits for there being no
    parent-node (or mode) and it provides a more convenient interface, taking
    merely the root-element (and the parser as it is the main entry point) as
    constructor arguments. Also, contrary to any other :py:class:`modeBase`-
    subclass, it is the only that takes over the parser without an explicit
    call to :py:meth:`takeOver`.
    """

    _parser = None
    parent_mode = None
    my_tagName = None
    result = None
    namespaces = None
    default_namespace = None
    actionStack = None

    allowed_children = None

    def __init__(self, rootElement, parser, XPathFilter=None):
        """ Initializes and automatically enables the document mode

        Contrary to other modes, the document mode automatically takes over on
        initialization. This is because it is supposed to be the entry point
        to the interpreter logic. This is also why it takes the eXpat parser
        object as constructor argument.

        Note that contrary to any other modes, the rootElement may either be a
        class OR an instance; this is to facilitate loading several individual
        XML files within the same session.

        :param xmlize rootElement: classboj or instance belonging to the
            document root element
        :param xml.parser.expat.xmlparser parser: eXpat parser instance; note
            that the 'Parser' or 'ParseFile' method must only be called on the
            parser AFTER it has been associated to the document mode.
        """

        self.parent_mode = None
        self.result = None
        self.getParser(parser)
        self.actionStack = set()

        # only one node allowed
        self.allowed_children = [rootElement]
        self.my_tagName = "#Document"

        # take over
        self.takeOver()

    def processActionStack(self):
        for action in self.actionStack:
            action.executeAction()

    def startElement(self, name, attributes):
        """ Handles opening tags

        In the document mode, there may only be one opening tag and it has to
        be the document root element. Otherwise this is either invalid XML, or
        some foreign document that has nothing to do with us.

        The function thus tries to process the found tag, and if it does not
        match any :py:attr:`allowed_children`, i.e. the root element, it
        immediately throws an exception.
        """
        default_namespace = str(self.allowed_children[0]._xml_namespace)

        # process name-space attributes
        namespaces = XMLnamespaceDefinitions(attributes)

        # assume default-namespace
        if not 'xmlns' in namespaces.keys():
            namespaces['xmlns'] = default_namespace
            logger.warning('No default name-space specified. Add xmlns="{}" as'
                           ' an attribute of the document root element to get '
                           ' rid of this warning'.
                           format(namespaces['xmlns']))

        # get name/namespace pair and resolve namespace; check its correct
        xmlns, name = splitIntoNamespacePrefixAndName(name)
        try:
            xmlns = self.getNamespace(xmlns, namespaces)
        except KeyError:
            raise XMLParserError('Unknown namespace {}'.format(xmlns))
        if xmlns != default_namespace:
            raise XMLParserError(self.getLineNumber(),
                                 "Root element must be in namespace '{}'".
                                 format(default_namespace))

        # on the document-level, there's only one possibility, so skipping
        # stuff won't help
        mode = self.getNextMode(name, attributes, xmlns, namespaces)
        if mode is None:
            raise XMLParserError(self.getLineNumber(), 'invalid XML; the '
                                 'document root element must be a {}-tag'.
                                 format(self.allowed_children[0]._xml_tagName))
        mode.takeOver()

    def takeOver(self):
        """ Handles document closing

        As there is no _endElement or similar event that will be called anymore
        once the document root element has closed, calling takeOver on the
        document mode handler for a second time, in the document root element's
        :py:meth:`exit`method`, has no effect. Hence the takeOver function
        detects this second call and instead of re-directing the parser hooks,
        explicityl calls :py:meth:`exit` on the document mode.
        """

        if self.getParser().StartElementHandler is None:
            modeBase.takeOver(self)
        else:
            self.exit()

    def exit(self):
        """ Reached end of document """
        logger.debug("reached end of document")

    def addChildObject(self, obj):
        self.result = obj

    @classmethod
    def parse(cls, resource, root):
        """ Deserializes an object-tree from the XML source 'resource'

        Calling this function launches the actual parser. Note that if
        :py:meth:`__init__` was called without its resource argument, then
        parsing is deferred until the present function is called explicitly.

        :param resource: an XML data-source; if omitted or set to 'None',
            parsing is deferred until :py:meth:`parse` is called
        :type resource: open or str
        """

        # init
        result = None

        # a local context-class (With block) seems excessive and would actually
        # require some black magic to still allow access to the CurrentLine
        # property of the parser object. As the expat-parser is not supposed to
        # be accessed outside the class, a try-final block should to the trick
        try:

            filename = resource.name if hasattr(resource, 'name') else "XML"
            logger.info("parsing {}".format(filename))

            # no need to have this on the module's scope -> keep expat local
            import xml.parsers.expat

            # create new parser
            parser = xml.parsers.expat.ParserCreate()

            XMLnamespaceDefinitions.setAllowedNameSpaces(root)

            # initialize finite state machine
            doc = modeDocument(root, parser)

            # start parser
            if hasattr(resource, 'read'):
                parser.ParseFile(resource)
            else:
                parser.Parse(resource)

            # run the delayed actions
            doc.processActionStack()

            logger.info("finished parsing {}".format(filename))

            # retrieve results
            result = doc.result

        finally:

            # clear parser what ever happens
            del parser

        # if successfull, return the result (don't circumvent 'finally')
        return result


class modeSubDocument(modeDocument):

    parent_parser = None
    XMLfilter = None

    def __init__(self, parent_mode, rootElement, subdocument, XMLfilter=None):

        self.parent_mode = parent_mode
        self.result = None

        import xml.parsers.expat
        parser = xml.parsers.expat.ParserCreate()
        self.parent_parser = modeBase._parser
        modeBase._parser = parser

        # only one node allowed
        self.allowed_children = [rootElement]
        self.my_tagName = "#Document"

        # take over
        self.takeOver()
        self.getParser().ParseFile(subdocument)
        self.XMLfilter = XMLfilter

    def exit(self):
        modeBase._parser = self.parent_parser
        modeBase.exit(self)


class modeElementWithChildren(modeBase):
    """ Process a node that only contains other tags

    When in this mode, the interpreter scans the current element for other
    node elements, as listed in :py:attr:`allowed_children`. If a particular
    element does not match any of these, it is skipped with a warning being
    uttered through a logger.  Thus unexpected nodes won't completely break
    processing; yet all of their contents will be skipped, which may be of
    particular consequence when there's a typo in some tag-name,
    """

    def __init__(self, parent_mode, my_class, attributes, localNamespaces):
        """ Initializes the element-with-children mode

        In addition to the :py:class:`modeBase` constructor actions, this
        method immediately calls :py:meth:`initMyClass` to create a new object
        instance. This is because nodes that accept only other child-nodes
        have all the data needed for their creation in the opening-tag, and
        child-elements can be added to the newly created object afterwards.
        """

        # init the object
        modeBase.__init__(self, parent_mode, my_class, attributes,
                          localNamespaces)

        # and immediately create a new instance
        self.initMyClass()

    def startElement(self, name, attributes):
        """ Handles opening tags

        When a known tag opens within the current, known tag, a new object
        instance has to be created. This function does so by scanning the
        current tag for any known node and passes control from the current to
        a newly initialized, subordinate element-with-children mode handler.

        If the detected node is unknown (hint: it cannot detect typos...), it
        will issue a warning through the module's logger and skip the node by
        turning control over to a subordinate skip-tag mode.
        """

        # xmlns-declarations apply to the node that defines them and all its
        # children; hence at this point we need to look ahead if the node (for
        # which we are about to trigger a sub-mode) defines its own namespace
        # resp, introduces a new prefix (that must be valid only for the node
        # itself and its children)
        namespaces = XMLnamespaceDefinitions(attributes)
        try:
            xmlns, name = self.resolveXMLname(name, namespaces)
        except KeyError:

            # make a record of this
            hint = "Maybe a typo?"
            if ":" in name:
                xmlns = name.split(":")[0]

                hint = ("If '{0}' is meant as a namespace prefix, you probably"
                        " forgot to define it. Add the appropritate "
                        'xmlns:{0}="[namespace]" attribute to any tag higher up'
                        "in the document hierarchy".
                        format(xmlns))

            XMLParsingProblem(self.getLineNumber(), "{}-tag is not allowed "
                              "within {}-tags, and will be ignored. {}".
                              format(name, self.my_tagName, hint))

            # switch to skip-tag mode
            mode = modeSkipTag(self, name)

        else:
            # on the document-level, there's only one possibility, so skipping
            # stuff won't help
            mode = self.getNextMode(name, attributes, xmlns, namespaces)

            if mode is None:

                XMLParsingProblem(self.getLineNumber(), "{}-tag is not allowed "
                              "within {}-tags, and will be ignored.".
                              format(name, self.my_tagName))

                # switch to skip-tag mode
                mode = modeSkipTag(self, name)

        # go on with the selected mode
        mode.takeOver()


class modeElementWithOnlyText(modeBase):
    """ Gets all the text (including tags) within a node

    This class handles elements who encompass text, meaning:
        <tag>some Text</tag>

    It gathers all the text contained within the tag (this includes CDATA tags
    and also works if the text is split among multiple lines) and later passes
    it to the constructor as the value for the constructor argument listed in
    the corresponding classobj's :py:attr:`_xml_textPrpoerty` field.

    Note that by default, any tags inside such an element will be ignored:

        <tag>some text <subtag>with a tag</subtag></tab>

    yields: "some text with a tag"

    In order to easily handle e.g. documentation, requiring nodes mixing
    text-elements and node-elements, the additional
    py:attr:`_xml_textProperty_includeTags` can be used to instruct this mode
    handler to include any (or selected) tags textually, meaning:

        <tag>some text <subtag>with a tag</subtag></tab>

    yields: "some text <subtag>with a tag</subtag>"
    This can then be passed to another parser or an XSLT processor.
    """

    text = None
    withinCDATA = None
    warnOnce = None
    include_tags = False

    def __init__(self, parent_mode, my_class, attributes, localNamespaces,
                 texProp):
        """ Initializes the element with only text mode

        In addition to the usual behavior and interface, this method provides
        the :py:attr:`include_tags` parameter (and object attribute). If set to
        None or an empty list, any tags within the current tag will be ignored
        and a generic warning will be displayed (once at the first occurance of
        a tag). If it is set to the str '*', any tags will be allowed. If it
        is set to a list of str, each identifies one allowed tag. Note that the
        hierachy in which they appear cannot be controlled.
        """

        # allow strings or text propertie descriptpors
        texProp = (TextPropertyDescription(texProp) if isinstance(texProp, str)
                   else texProp)
        if (not texProp is None and
            not isinstance(texProp, TextPropertyDescription)):
            raise TypeError("texProp must be a text property description")

        modeBase.__init__(self, parent_mode, my_class, attributes,
                          localNamespaces)
        self.text = ""
        self.withinCDATA = False
        self.warnOnce = False
        self.textProperty = texProp

    def _warn(self):
        if not self.warnOnce:
            logger.warning("XML-tags within {}-tag ignored"
                           .format(self.my_tagName))
            self.warnOnce = True

    def startElement(self, name, attributes):

        # TODO: somehow get true namespace support here
        namespaces = XMLnamespaceDefinitions(attributes)
        xmlns, name = self.resolveXMLname(name, namespaces)

        # warn and exit
        if self.textProperty.allowed_tags is None:
            self._warn()
            return

        # otherwise, go back to XML and append
        if self.textProperty.allowed_tags and not name in self.include_tags:
            XMLParsingProblem(self.getLineNumber(), "{}-tag are not allowed "
                              "within {}-tags".format(name, self.my_tagName))
            return

        self.text += "<" + name
        if attributes:
            text = ('{}="{}"'.format(attr) for attr in attributes)
            self.text += " " + " ".join(text)
        self.text += ">"

    def endElement(self, name):

        # process name-space prefixes
        xmlns, name = self.resolveXMLname(name)

        # can initialize only after having the complete text
        if self.my_tagName == name and self.my_namespace == xmlns:
            self.my_attributes[self.textProperty.object_property] = self.text
            self.initMyClass()
            self.exit()
            return

        # otherwise, go back to XML and append
        if self.include_tags:
            self.text += "</{}>".format(name)

    def characterData(self, data):
        if not self.withinCDATA:
            data = data.replace('\n', '').replace('\t', '').strip()
        self.text += data

    def cdataBegin(self):
        if self.withinCDATA:
            raise XMLParserError("structural XML error")
        self.withinCDATA = True

        # go back to XML and append
        if self.include_tags:
            self.text += "<![CDATA["

        # TODO: maybe add a comment-handler

    def cdataEnd(self):
        if not self.withinCDATA:
            raise XMLParserError("structural XML error")
        self.withinCDATA = False

        # go back to XML and append
        if not self.include_tags:
            self.text += "]]>"


class modeContentFreeElement(modeBase):
    """ Handles elements that do not accept any content

    In XML, these should correspond to tags written as:
        <tag property="" />

    The element will issue a warning to the logger if any contents are detected
    and then simply ignore it.
    """

    warnedOnce = None

    def __init__(self, parent_mode, my_class, attributes, localNamespaces):
        modeBase.__init__(self, parent_mode, my_class, attributes,
                          localNamespaces)
        self.warnedOnce = False
        self.initMyClass()

    def warn(self):
        if self.warnedOnce:
            return
        XMLParsingProblem(self.getLineNumber(), "The {}-tag is supposed to be "
                          "empty; ignoring its contents".
                          format(self.my_tagName))
        self.warnedOnce = True

    def startElement(self, name, attributes):
        self.warn()

    def cdataBegin(self):
        self.warn()

    def characterData(self, data):
        self.warn()


class modeSkipTag(modeBase):
    """ Skips the specified tag

    Contrary to all other modes, this one takes a string as argument. The
    specified node will be skipped, i.e. all inputs will be ignored until the
    closing tag corresponding to the given tag-name is reached. It then simply
    returns control to the calling mode, so the mode handler doesn't really
    do much.

    In particular no direct (print) output or log entries are generated
    as it is assumed that the caller (or in this case :py:meth:`_getNextMode`
    has already taken care of that.
    """

    start_line = None

    def __init__(self, parent_mode, name):
        self.parent_mode = parent_mode
        self.my_tagName = name
        self.start_line = self.getLineNumber()

    def startElement(self, name, attributes):
        pass

    def endElement(self, name):
        """ Same as :py:class:`modeBase`, but disregards namespaces """

        if name == self.my_tagName:
            logger.debug("found closing </{}> at line {}".
                         format(name, self.getLineNumber()))
            self.exit()


class XMLParsingProblem(object):
    """ Describes a problem encountered while parsing an XML file

    This object is used to describe any problems that were detected while
    processing an XML source. It stores the name of the source, the line number
    and a description of the problem.

    It can be provided into a warning, an exception and its initialization will
    be marked as error or wrning in the module logger."""

    message = None
    lineNumber = None
    resource = None

    def __init__(self, lineNumber, message, resource=None, warn=True):
        """ Initializes a problem report instance """

        self.message = message
        self.lineNumber = lineNumber
        self.resource = resource
        if warn:
            self.warn()

    def __str__(self):
        return "Line {}: {}".format(self.lineNumber, self.message)

    def getException(self):
        """ Turns this into a XMLParserError """
        return XMLParserError(self.lineNumber, self.message)

    def warn(self):
        logger.warning(str(self))


class XMLParserError(ggException):
    """ Exception with a line-indication describing a problem with the XML file

    This error-class provides the conventional interface, but it also accepts
    a line-number as input, which it will put in front of the error message.
    This is for the user to easily identify problems with the XML source."""

    lineNumber = None

    def __init__(self, line, error):
        self.lineNumber = line
        self.message = error

    def __str__(self):
        return "Line {}: {}".format(self.lineNumber, self.message)

    def getReport(self):
        """ Gets the corresponding :py:class:`XMLParsingProblem` """
        return XMLParsingProblem(self.lineNumber, self.message, None)


def splitIntoNamespacePrefixAndName(name):

        r_xmlns = re.compile('((?!xml)[A-Z_][A-Z0-9.\-_:]*):'
                             '([A-Z_][A-Z0-9.\-_:]+)', re.IGNORECASE)
        matches = r_xmlns.match(name)

        if matches is None:
            prefix = 'xmlns'
        else:
            prefix = matches.group(1)
            name = matches.group(2)

        return (prefix, name)


class XMLnamespaceDefinitions(dict):
    # TODO: use eXpat's StartNamespaceDeclHandler

    _allowedNameSpaces = None

    def __init__(self, attributes=None):
        if not attributes is None:
            self.parseAttributes(attributes)

    def parseAttributes(self, attributes):

        # get namespaces
        r_xmlns = re.compile('^[xX][mM][lL][nN][sS](?::([a-zA-Z0-9\-_]+))?$')

        # get name-space attributes, define, then delete the attribute
        for attr in attributes.keys():

            # check for xmlns attribute
            matches = r_xmlns.match(attr)
            if not matches:
                continue

            # get prefix-name; note that the standard-namespace may be
            # overwritten at any level
            nspfx = matches.group(1)
            nspfx = 'xmlns' if nspfx is None else nspfx
            nsid = attributes[attr]
            del attributes[attr]

            if nsid in self.__class__._allowedNameSpaces:
                logger.info("encountered namespace: {}:{}".format(nspfx, nsid))
                self[nspfx] = nsid
            else:
                logger.warn("unknown namespace {}; tags prefixed by "
                            "'{}:' will be ignored".format(nsid, nspfx))

    @classmethod
    def setAllowedNameSpaces(cls, root):
        """ Retrieves all known namespaces within the application """

        def recursor(el):
            allowed = []
            if (hasattr(el, '_xml_children')
                and hasattr(el._xml_children, '__iter__')):
                for child in el._xml_children:
                    allowed += recursor(child)
            allowed.append(str(el._xml_namespace))
            if (hasattr(el, '_xml_attributeHandlers')
                and hasattr(el._xml_attributeHandlers, '__iter__')):
                for handler in el._xml_attributeHandlers:
                    allowed.append(str(handler.my_namespace))
            return allowed

        # allow schemas...
        cls._allowedNameSpaces = set(recursor(root))
        cls._allowedNameSpaces.add("http://www.w3.org/2001/XMLSchema-instance")

