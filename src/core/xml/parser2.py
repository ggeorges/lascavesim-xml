'''
Created on 04.03.2013

@author: ggeorges
'''

import re
import inspect
import xml.parsers.expat
import core.common


class HierarchyMember(core.common.LascavesimBase):

    _parent = None

    def __init__(self, parent):
        self._parent = parent

    def getParent(self):
        return self._parent


class XMLNamespaceManager(HierarchyMember):
    """ Hierarchical object structure for managing nested namespaces """

    local_namespaces = None

    _r_xmlname = None
    _r_xmlns = None
    _allowedNamespaces = None

    def __init__(self, parent, attributes):

        # dim
        self.local_namespaces = {}

        # convenientely add the parent namespace object
        if hasattr(parent, 'namespaces'):
            parent = parent.namespaces
        if not parent is None and not hasattr(parent, 'resolveXMLNamespace'):
            raise TypeError()
        self._parent = parent

        # process attributes
        for attr_name in set(attributes.iterkeys()):
            if self.processNamespaceDeclaration(attr_name,
                                                attributes[attr_name]):
                del attributes[attr_name]

    @staticmethod
    def _splitPrefix(name):
        if XMLNamespaceManager._r_xmlname is None:
            pattern = '((?!xml)[A-Z_][A-Z0-9.\-_:]*):([A-Z_][A-Z0-9.\-_:]+)'
            XMLNamespaceManager._r_xmlname = re.compile(pattern, re.IGNORECASE)
        matches = XMLNamespaceManager._r_xmlname.match(name)
        if not matches:
            return None
        return (matches.group(1), matches.group(2))

    @staticmethod
    def _splitNamespaceDeclaration(name):
        if XMLNamespaceManager._r_xmlns is None:
            pattern = '^xmlns(?::([A-Z0-9\-_]+))?$'
            XMLNamespaceManager._r_xmlns = re.compile(pattern, re.IGNORECASE)
        matches = XMLNamespaceManager._r_xmlns.match(name)
        if not matches:
            return None
        return "xmlns" if matches.group(1) is None else matches.group(1)

    def resolveXMLNamespace(self, prefix):
        """ Gets the uri corresponding to 'prefix' """

        # try to find the prefix locally; if there is none, delegate to parent
        try:
            return self.local_namespaces[prefix]
        except KeyError:
            if self.getParent() is None:
                raise KeyError("unknown namespace prefix '{}'".format(prefix))
            return self.getParent().resolveXMLNamespace(prefix)

    def resolveXMLname(self, name):
        """ Converts prefix:name to (uri, name)

        This function converts any xml-name to a namespace-xmlname pair. Names
        can refer both to attributes and tags. Unprefixed names are by
        definition always part of the local default namespace. Prefixes may not
        necessarily point to namespaces though; in that case, the xml-name will
        be made to include the apparent prefix, and the namespace iwll be the
        local namespace.

        Note that this function does not process namespace declarations. As
        xml-names generally are not allowed to start with "xml",
        xmlns-declarations will cause an error. """

        # no prefix: we are in the default namespace for sure
        if not ":" in name:
            uri = self.resolveXMLNamespace('xmlns')
            return(uri, name)

        # split into prefix and name
        parts = XMLNamespaceManager._splitPrefix(name)
        if parts is None:
            raise TypeError("'{}' is not a valid xml-name".format(name))

        # if the prefix is unresolvable, it's not a namsepace identifier
        try:
            uri = self.resolveXMLNamespace(parts[0])
            name = parts[1]
        except KeyError:
            uri = self.resolveXMLNamespace('xmlns')
        return (uri, name)

    def processNamespaceDeclaration(self, attr_name, attr_value):
        prefix = XMLNamespaceManager._splitNamespaceDeclaration(attr_name)
        if prefix is None:
            return False
        if not attr_value in XMLNamespaceManager._allowedNamespaces:
            self.log_warn("unknown namespace {} was ignored", attr_value)
            return True
        self.local_namespaces[prefix] = attr_value
        return True

    @staticmethod
    def setAllowedNamespaces(root):
        """ Retrieves all known namespaces within the application """

        def recursor(el):
            allowed = []
            if (hasattr(el, '_xml_children')
                and hasattr(el._xml_children, '__iter__')):
                for child in el._xml_children:
                    allowed += recursor(child)
            allowed.append(str(el._xml_namespace))
            if (hasattr(el, '_xml_attributeHandlers')
                and hasattr(el._xml_attributeHandlers, '__iter__')):
                for handler in el._xml_attributeHandlers:
                    allowed.append(str(handler.my_namespace))
            return allowed

        # allow schemas...
        XMLNamespaceManager._allowedNamespaces = set(recursor(root))


class EXpatParserControl(HierarchyMember):

    def __init__(self, parser=None, parent=None):
        super(EXpatParserControl, self).__init__(parent)
        self._parser = parser

    def getParser(self):
        """ Returns the underlying eXpat parser instance

        :returns: xml.parser.expat.xmlparser """

        # get local parser or the next highest in the hierarchy
        if self._parser is None:
            return self.getParent().getParser()
        return self._parser

    def getFileId(self):
        return self.getSystemId()

    def getSystemId(self):
        return self.getDocument().getSystemId()

    def getPublicId(self):
        return self.getDocument().getPublicId()

    def getLineNumber(self):
        """ Returns the line-number the parser is currently at

        For diagnostics and error-reporting, this function provides a
        convenient access to the parser's 'CurrentLineNumber' attribute.

        :returns: int """
        return self.getParser().CurrentLineNumber

    def takeOver(self):
        """ Attach the linked mode to the parser

        The function overrides the parser's call-back functions with the bound
        object's handler methods.The function must be explicitly called on new
        :py:class:`baseMode`-subclass instances for them to be able to process
        child-elements of any kind (otherwise eXpat events are directed to the
        parent).
        """

        parser = self.getParser()

        # adjust handlers to point to current class
        parser.StartElementHandler = self._startElement
        parser.EndElementHandler = self._endElement
        parser.CharacterDataHandler = self.characterData
        parser.StartCdataSectionHandler = self.cdataBegin
        parser.EndCdataSectionHandler = self.cdataEnd
        parser.CharacterDataHandler = self.characterData

    def _log_message(self, msg, *fmt):
        return "{} line {}: {}".format(self.getFileId(),
                                       self.getLineNumber(),
                                       str(msg).format(*fmt))

    def _getLogger(self):
        return super(EXpatParserControl, self)._getLogger("xml-parser")

    def _startElement(self, name, attributes):
        mode = self.startElement(name, attributes)
        if hasattr(mode, 'takeOver'):
            mode.takeOver()

    def _endElement(self, name):
        mode = self.endElement(name)
        if hasattr(self, 'exit'):
            self.exit()
        if hasattr(mode, 'takeOver'):
            mode.takeOver()

    def startElement(self, name, attributes):
        raise NotImplementedError('startElement is abstract')

    def endElement(self, name):
        raise NotImplementedError('endElement is abstract')

    def cdataBegin(self):
        # default behavior: ignore
        pass

    def cdataEnd(self):
        # default behavior: ignore
        pass

    def characterData(self, data):
        # default behavior: ignore
        pass


class XMLparser(EXpatParserControl):

    cls = None
    object = None
    namespaces = None
    _document = None

    def __init__(self, parent, namespaces, cls, parser=None):
        super(XMLparser, self).__init__(parser, parent)
        self.cls = cls
        self.namespaces = namespaces
        self.object = None

    def getInitDict(self, cls):

        """ Initializes a new list

        Given any new-style python class-object, this method extracts the
        constructor's argument list using inspect and retrieves the defaults.
        """

        # get function arguments
        try:
            arg_specs = inspect.getargspec(cls.__init__)
        except TypeError:
            raise RuntimeError("cls is not a new-style python class (probably"
                               " __init__ was not explicitly declared)")
        # gather data and compile to list
        func_arg_names = arg_specs.args
        func_arg_names.remove('self')
        arg_defaults = (list(arg_specs.defaults)
                        if hasattr(arg_specs.defaults, '__iter__') else [])

        #go collecting
        arguments = []
        mandatory = []
        for arg_name in reversed(func_arg_names):

            # get default arg, if thereis one
            try:
                default = arg_defaults.pop()
            except IndexError:
                default = None
                mandatory.append(arg_name)

            arguments.append((arg_name, default))

        # need to restore order
        return (dict(reversed(arguments)), mandatory)

    def createObject(self, attributes):

        # process arguments -> detect mandatory/optional constructor arguments
        # and if there are none, get handled attributes; everything remaining
        # should raise a logger message; mandatory constructor arguments that
        # have not been defined should yield an exception

        # get handled arguments
        handled_attributes = set()
        if (hasattr(self.cls, '_xml_attributeHandlers') and
            hasattr(self.cls._xml_attributeHandlers, '__iter__')):
            handled_attributes = set((s.resolvedXMLname(), s) for s in
                                     self.cls._xml_attributeHandlers)

        my_arguments, mandatory_args = self.getInitDict(self.cls)
        my_argHandlers = []

        for attr_name, attr_value in attributes.iteritems():

            # resolve
            if not ":" in attr_name:
                uri = self.cls._xml_namespace
                name = attr_name
            else:
                uri, name = self.namespaces.resolveXMLname(str(attr_name))

            # a normal argument?
            if str(uri) == str(self.cls._xml_namespace):

                # allow for ":" and "-" in attribute names
                _name = name.replace(":", "_").replace("-", "_")
                if _name in my_arguments.iterkeys():
                    my_arguments[_name] = str(attr_value)
                    if _name in mandatory_args:
                        mandatory_args.remove(_name)
                    continue

            # is this a handled attribute?
            for xml_name, handler in handled_attributes:
                if xml_name == (uri, name):
                    my_argHandlers.append((handler, str(attr_value)))
                    break
            else:
                self.log_warn("{}-attribute not allowed in {}-tags;", name,
                              self.cls._xml_tagName)
            continue

        if mandatory_args:
            self.log_error("mandatory {} attribute(s) on {}-tag missing",
                           " and ".join(mandatory_args),
                           self.cls._xml_tagName)
            raise TypeError("missing arguments: {}".
                            format(", ".join(mandatory_args)))

        # create and remember
        try:
            self.object = self.cls(**my_arguments)
        except core.common.LascavesimException as e:
            self.log_error(e)
            return
        setattr(self.object, '_xml_lineNumber', self.getLineNumber())
        setattr(self.object, '_xml_fileId', self.getFileId())

        # execute handled attributes
        for handler, value in my_argHandlers:
            if hasattr(handler, 'execute'):
                try:
                    handler.execute(self.object, value)
                except core.common.LascavesimException as e:
                    e.log()

            if hasattr(handler, 'parserModifier'):
                handler.parserModifier(self, value)

        # make sure there is a parent
        self.addToHierarchy()

        # execute handled attributes when object is in hieararchy = has parent
        for handler, value in my_argHandlers:
            if hasattr(handler, 'execute_aposteriori'):
                handler.execute_aposteriori(self.object, value)

        # run post-creation hook
        if hasattr(self.object, 'postCreationHook'):
            self.object.postCreationHook()

    def addToHierarchy(self):

        # get parent object and try to get adopted; assumptions:
        #    -> None-object is the XMLparser_excrept modes; just skip ahead
        #       and there will be suitable parent
        #    -> if there is an object, but it doesn't have an isVirtual method,
        #       assume default behavior, i.e. isVirtual == false (so that 
        #       @xmlized objects belong too the DOM without modification)
        #    -> an object with a isVirtual method is easy
        #    -> also stop if we arrived at the root document
        #    -> all parents must have an 'object' attribute, or they are not
        #       subclasses of the ^'XMLparser' base class
        parent = self
        while True:
            parent = parent.getParent()
            if parent == self.getDocument():
                break
            if not hasattr(parent, 'object'):
                raise RuntimeError("non-parser object in parser stack")
            if parent.object is None:
                continue
            if not hasattr(parent.object, 'isVirtual'):
                break
            if (hasattr(parent.object, 'isVirtual')
                and not parent.object.isVirtual()):
                break

        if not hasattr(parent, 'object'):
            raise RuntimeError("top level node undefined")

        # run the pre-injection hook
        if hasattr(self.object, 'preInjectionHook'):
            try:
                self.object.preInjectionHook(parent.object, self)
            except core.common.LascavesimException as e:
                self.log_error(e)

        # completely outsource the decision to the action stack
        if hasattr(self.getDocument().actionStack, "append"):
            try:
                stack = self.getDocument().actionStack
                stack.append(self.object, parent.object)
            except TypeError as e:
                pass

        # virtual entities must not be added, but they may need a parent, so
        # just go until here
        if hasattr(self.object, 'isVirtual') and self.object.isVirtual():
            return

        if parent.object is None:
            parent.object = self.object
        elif hasattr(parent.object, 'adopt'):
            try:
                parent.object.adopt(self.object)
            except core.common.LascavesimException as e:
                self.log_error(e)
        else:
            raise RuntimeError("corrupt tree")

        # run post-injection hook
        if hasattr(self.object, 'postInjectionHook'):
            try:
                self.object.postInjectionHook(self)
            except core.common.LascavesimException as e:
                self.log_error(e)

    def isDocument(self):
        return False

    def isSubDocument(self):
        return False

    def getDocument(self):

        # return stored reference to save time
        if not self._document is None:
            return self._document

        # compute reference -> recursively
        if self.isDocument() and not self.isSubDocument():
            self._document = self
            return self
        if not hasattr(self, 'getParent'):
            raise RuntimeError("serious problem")
        parent = self.getParent()
        if not hasattr(parent, 'getDocument'):
            raise RuntimeError("serious problem")
        self._document = parent.getDocument()
        return self._document

    def resolveXMLtag(self, uri, name):

        for child in self.getAllowedChildren():
            if child._xml_tagName == name and child._xml_namespace == uri:
                return child

        # this seems invalid (but other actions could still be taken)
        raise KeyError("can't resolve {} in namespace {}".format(name, uri))

    def resolveXMLname(self, name):
        return self.namespaces.resolveXMLname(name)

    def getAllowedChildren(self):
        # is there a suitable child-class?
        if (not hasattr(self.cls, '_xml_children')
            or not hasattr(self.cls._xml_children, "__iter__")):
            return []
        return self.cls._xml_children


class XMLparser_normal(XMLparser):

    def __init__(self, parent, namespaces, cls, attributes):
        super(XMLparser_normal, self).__init__(parent, namespaces, cls)
        self.createObject(attributes)

    def startElement(self, name, attributes):

        namespaces = XMLNamespaceManager(self.namespaces, attributes)
        xml_name = namespaces.resolveXMLname(name)

        # look for a potential child-class:
        try:
            child = self.resolveXMLtag(*xml_name)
        except KeyError:
            return XMLparser_skipTag(self, namespaces, name)

        # text-property has its own mode
        if (hasattr(child, '_xml_textProperty')
            and not child._xml_textProperty is None):
            return XMLparser_textOnly(self, namespaces, child, attributes)
        return XMLparser_normal(self, namespaces, child, attributes)

    def endElement(self, tag_name):

        # default behavior: on associated class's closing tag, go back to parent
        uri, name = self.resolveXMLname(tag_name)
        if self.cls._xml_tagName == name and self.cls._xml_namespace == uri:
            return self.getParent()

    def enterSubDocumentMode(self, resource, xpath_filter=None):
        XMLparser_excreptDocument(self, resource, xpath_filter)


class XMLparser_document(XMLparser):

    actionStack = None
    _systemId = None
    _publicId = None

    def __init__(self, stream, rootElement, parent=None, actionStack=None,
                 systemId=None, publicId=None):

        # check inputs
        if not hasattr(stream, 'read') or not hasattr(stream, 'name'):
            raise TypeError("stream must be readable")
        if (not hasattr(rootElement, '_xml_tagName') or not
            hasattr(rootElement, '_xml_namespace')):
            raise TypeError("root-element must be @xmlized")

        # get info about the parser
        parser = xml.parsers.expat.ParserCreate()
        self._systemId = stream.name if systemId is None else systemId
        self._publicId = None if publicId is None else publicId
        XMLNamespaceManager.setAllowedNamespaces(rootElement)
        super(XMLparser_document, self).__init__(parent, None, rootElement,
                                                 parser)

        # prepare the action stack
        self.actionStack = actionStack
        # go parsing
        self.takeOver()
        self._parser.ParseFile(stream)

    def isDocument(self):
        return True

    def getSystemId(self):
        return self._systemId

    def getPublicId(self):
        return self._publicId

    def startElement(self, name, attributes):

        namespaces = XMLNamespaceManager(self.namespaces, attributes)
        xml_name = namespaces.resolveXMLname(name)

        # look for a potential child-class:
        if xml_name != (self.cls._xml_namespace, self.cls._xml_tagName):
            raise RuntimeError("wrong root element")

        # text-property has its own mode
        if (hasattr(self.cls, '_xml_textProperty')
            and not self.cls._xml_textProperty is None):
            return XMLparser_textOnly(self, namespaces, self.cls, attributes)
        return XMLparser_normal(self, namespaces, self.cls, attributes)


class XMLparser_xpathFilter(object):

    xpath_filter = None
    _node_filter = None
    _attr_filter = None
    _wildcard_mode = None
    _match_counter = None

    _r_xpath = None

    def __init__(self, xpath_filter):

        # wildcard-mode: search for tag anywhere
        self._wildcard_mode = False
        self._match_counter = 0

        # adjust -> remove first "/" (and complain if there is none)
        self.xpath_filter = xpath_filter
        if isinstance(xpath_filter, str):
            if xpath_filter[-1] == "/":
                raise RuntimeError("unsupported xpath syntax (filter may not "
                                   "end in '/'): {}".format(self.xpath_filter))
            if not xpath_filter[0] == "/":
                raise RuntimeError("unsupported xpath syntax (must begin "
                                   "with a '/'): {}".format(xpath_filter))
            self.xpath_filter = xpath_filter[1:].split("/")

        # must be an array with at least one element
        if not self.xpath_filter:
            raise RuntimeError("xpath ran out, but target wasn't acquired")

        # set active filter
        self._setActiveFilter(self.xpath_filter[0])

        # detect wild-card mode (needed if we are not at the target yet)
        # note that a '*[...]' type filter does not require the wildcard mode
        # '//'-type filters only make sense with a following argument
        if (self._node_filter == "*" and not self._attr_filter):
            self._wildcard_mode = True
        if self._node_filter == "":
            self._wildcard_mode = True
            if len(self.xpath_filter) < 2:
                raise RuntimeError("'//' not allowed as final or only filter")
            self._setActiveFilter(self.xpath_filter[1])

    def isAtTarget(self):
        return len(self.xpath_filter) < 2

    def _setActiveFilter(self, filt):
        xpath = self._process_xpath_subexpr(filt)
        self._node_filter, self._attr_filter = xpath

    def isInWildcardMode(self):
        return self._wildcard_mode

    def _process_xpath_subexpr(self, expr):
        """ Breaks a sup-expression down into processable parts """

        if expr is None:
            raise TypeError("expecting string")
        expr = str(expr)
        if expr == '':
            return ('', [])

        # get node-name and filter (if none, we're done)
        r = "^/?([\w\-:]+|\*)(\[.*\])?$"
        matches = re.match(r, expr)
        if not matches:
            raise RuntimeError("xpath syntax error: {}".format(expr))
        node, filt_expr = matches.groups()
        if filt_expr is None:
            return (node, [])

        # split by []-group
        parts = re.findall("\[([^\[\]]+)\]", filt_expr)
        if parts is None or not parts:
            return (node, [])

        # get all filters ('and' match)
        r = re.compile("((?:\@[\w:\-]+)|\@\*|(?:[1-9][0-9]*))" # @attr / @* / 1
                       "(?: *((?:<|>|!)?=) *" # comparison operators
                       "(?:(?:(?P<qt>['\"])([^(?P=qt)]*)(?P=qt))" # quoted text
                       "|([0-9])+))?") # or an unquoted numeral
        filt = []
        for part in parts:
            for lhs, op, quote, rhs, rhs_num in r.findall(part):
                if rhs == '' and rhs_num != '':
                    try:
                        rhs = int(rhs_num)
                    except ValueError:
                        rhs = float(rhs_num)
                filt.append((lhs, op, rhs))

        return (node, filt)

    def getChildFilter(self):
        if self.xpath_filter[0] == "":
            return self.xpath_filter[2:]
        return self.xpath_filter[1:]

    def matchesActiveFilter(self, child, attributes, namespaces):

        if self._node_filter == "" or self._node_filter is None:
            raise RuntimeError("this can't be")

        # unless the filter is '*', the tag_name must match the filter
        try:
            xml_name = namespaces.resolveXMLname(self._node_filter)
        except KeyError:
            return False
        if ((xml_name[1] != child._xml_tagName
             or xml_name[0] != child._xml_namespace
            ) and self._node_filter != '*'):
            return False

        def not_comp(val1, val2, operator):
            if isinstance(val1, int) or isinstance(val2, int):
                val1 = int(val1)
                val2 = int(val2)
            if isinstance(val1, float) or isinstance(val2, float):
                val1 = float(val1)
                val2 = float(val2)

            if operator == "=":
                return val1 != val2
            if operator == ">":
                return val1 <= val2
            if operator == ">=":
                return val1 < val2
            if operator == "<":
                return val1 >= val2
            if operator == "<=":
                return val1 > val2
            if operator == "!=":
                return val1 == val2

        # now go check the attribute filter
        _counter_found = None
        for attribute, operator, value in self._attr_filter:

            # check the attribute value (may also be '*' to match an element
            # that has any attribute, no matter the value)
            if attribute[0] == "@":
                attribute = attribute[1:]
                if attribute == "*" and not attributes:
                    return False
                try:
                    if not_comp(attributes[attribute], value, operator):
                        return False
                except KeyError:
                    return False

            # always the last counter
            elif attribute == "pos()":
                _counter_found = int(value)

            elif isinstance(attribute, int):
                _counter_found = attribute

        # now check the counter
        if _counter_found is not None:
            self._match_counter += 1
            if self._match_counter != _counter_found:
                return False

        return True

    def levelIsBeingIgnored(self):
        return self._node_filter == "*" and not self._attr_filter


class XMLparser_excrept(XMLparser, XMLparser_xpathFilter):

    got_result = None

    def __init__(self, parent, namespaces, cls, attributes, xpath_filter=None):

        XMLparser_xpathFilter.__init__(self, xpath_filter)
        super(XMLparser_excrept, self).__init__(parent, namespaces, cls)
        self.got_result = False

    def startElement(self, name, attributes):

        namespaces = XMLNamespaceManager(self.namespaces, attributes)
        xml_name = namespaces.resolveXMLname(name)

        # look for a potential child-class:
        try:
            child = self.resolveXMLtag(*xml_name)
        except KeyError:
            self.log_warn("{}-tag not allowed in this context", name)
            return XMLparser_skipTag(self, namespaces, name)

        # a match? continue or already at destination?
        if self.levelIsBeingIgnored() and not self.isAtTarget():
            return XMLparser_excrept(self, namespaces, child, attributes,
                                     self.getChildFilter())

        if self.matchesActiveFilter(child, attributes, namespaces):
            if self.isAtTarget():
                self.getSubDocument().got_result = True
                #TODO: text-only nodes
                return XMLparser_normal(self, namespaces, child, attributes)

            return XMLparser_excrept(self, namespaces, child, attributes,
                                     self.getChildFilter())

        # in wildcard mode, we sitll jhave to check for children
        if self.isInWildcardMode():
            return XMLparser_excrept(self, namespaces, child, attributes,
                                     self.xpath_filter)


        # no matches? then either skip or keep looking
        return XMLparser_skipTag(self, child._xml_namespace, child._xml_tagName)

    def endElement(self, tag_name):

        # default behavior: on associated class's closing tag, go back to parent
        uri, name = self.resolveXMLname(tag_name)
        if self.cls._xml_tagName == name and self.cls._xml_namespace == uri:
            return self.getParent()

    def exit(self):
        if self.getParent().isSubDocument():
            if not self.got_result:
                self.log_warn("xpath filter didn't match anything")

    def getSubDocument(self):
        parent = self.getParent()
        while not parent.isDocument() or not parent.isSubDocument():
            parent = parent.getParent()
        return parent


class XMLparser_excreptDocument(XMLparser_document, XMLparser_xpathFilter):

    xpath_filter = None

    def __init__(self, parent, stream, xpath_filter=None, systemId=None,
                 publicId=None):

        XMLparser_xpathFilter.__init__(self, xpath_filter)
        cls = parent.getDocument().cls

        # this launches the document parser -> last statement
        XMLparser_document.__init__(self, stream, cls, parent,
                                    parent.getDocument().actionStack,
                                    systemId, publicId)

    def startElement(self, name, attributes):

        namespaces = XMLNamespaceManager(self.namespaces, attributes)
        xml_name = namespaces.resolveXMLname(name)

        # look for a potential child-class:
        if xml_name != (self.cls._xml_namespace, self.cls._xml_tagName):
            raise RuntimeError("wrong root element")

        # a match? continue or already at destination?
        if self.levelIsBeingIgnored():
            return XMLparser_excrept(self, namespaces, self.cls, attributes,
                                     self.getChildFilter())

        if self.matchesActiveFilter(self.cls, attributes, namespaces):
            if self.isAtTarget():
                raise RuntimeError("can't import root node")
            return XMLparser_excrept(self, namespaces, self.cls, attributes,
                                     self.getChildFilter())

        # if this occurs, the document is corrupt
        if not self.isInWildcardMode():
            raise RuntimeError("invalid xml")
        return XMLparser_excrept(self, namespaces, self.cls, attributes,
                                 self.xpath_filter)

    def isSubDocument(self):
        return True


class XMLparser_textOnly(XMLparser):

    _text = None
    _textPropertyName = None
    _allowedTags = None
    _attributes = None

    def __init__(self, parent, namespaces, cls, attributes):
        if not hasattr(cls, '_xml_textProperty'):
            raise TypeError("cls must use textProperty")
        self._text = ""
        self._textPropertyName = (str(cls._xml_textProperty.object_property) if
                                  hasattr(cls._xml_textProperty,
                                          'object_property')
                                  else cls._xml_textProperty)
        self._allowedTags = (list(cls._xml_textProperty.allowed_tags) if
                             hasattr(cls._xml_textProperty, 'allowed_tags')
                             else [])
        super(XMLparser_textOnly, self).__init__(parent, namespaces, cls)
        self._attributes = attributes

    def startElement(self, tag_name, attributes):
        uri, name = self.resolveXMLname(tag_name)
        if (self.cls._xml_namespace == uri and (self._allowedTags is None or
                                                name in self._allowedTags)):
            txt = (' {} = "{}"'.format(*attr)for attr in attributes.iteritems())
            self._text += "<{}{}>".format(name, "".join(txt))
        return None

    def endElement(self, tag_name):
        uri, name = self.resolveXMLname(tag_name)
        if (self.cls._xml_namespace == uri and (self._allowedTags is None or
                                                name in self._allowedTags)):
            self._txt = "</{}>".format(name)
            return None

        # default behavior: on associated class's closing tag, go back to parent
        uri, name = self.resolveXMLname(tag_name)
        if self.cls._xml_tagName == name and self.cls._xml_namespace == uri:
            self._attributes[self._textPropertyName] = self._text
            self.createObject(self._attributes)
            return self.getParent()

    def characterData(self, data):
        self._text += data


class XMLparser_skipTag(XMLparser):

    def __init__(self, parent, namespaces, name):

        # misuse cls to store the name we want to skip (by name)
        super(XMLparser_skipTag, self).__init__(parent, namespaces, name)
        self.cls = name

    def startElement(self, name, attributes):
        return None

    def endElement(self, tag_name):
        if self.cls == tag_name:
            return self.getParent()

