"""
-class definition

:date: 15.03.2013
:author: ggeorges
"""
import logging
import os


class LascavesimBase(object):
    """
    classdocs
    """

    def _getLogger(self, name="lascavesim"):
        return logging.getLogger(name)

    def log_error(self, msg, *args):
        msg = str(msg.message) if hasattr(msg, 'message') else str(msg)
        self._getLogger().error(self._log_message(msg, *args))

    def log_warn(self, msg, *args):
        msg = str(msg.message) if hasattr(msg, 'message') else str(msg)
        self._getLogger().warning(self._log_message(msg, *args))

    def log_info(self, msg, *args):
        msg = str(msg.message) if hasattr(msg, 'message') else str(msg)
        self._getLogger().info(self._log_message(msg, *args))

    def log_debug(self, msg, *args):
        msg = str(msg.message) if hasattr(msg, 'message') else str(msg)
        self._getLogger().info(self._log_message(msg, *args))

    def _log_message(self, msg, *args):
        return str(msg).format(*args)


class LascavesimException(Exception):
    pass
