"""
-class definition

:date: 06.02.2013
:author: ggeorges
"""
import core.common


class CASenabledObject(core.common.LascavesimBase):

    _cas = None

    def _CAS(self):
        return CASenabledObject._cas


class MathEquationSystem(CASenabledObject):
    """ Handles and represents collections of equations

    This class is a virtual representation of a mathematical equation system.
    All equations that are member of a system  apply at the same time (logical
    AND). Symbols of the same name used within different expressions are
    considered unique, i.e. the system is also a kind of mathematical
    namespace.

    Therefore the class does not only provide functionality to construct and
    modify equation systems, but also manage the employed symbols.
    """

    symbol = None
    """ symbol registry """

    equation = None
    """ equation registry """

    def __init__(self):
        self.symbol = set()
        self.equation = set()

    def getSymbolNamed(self, symbol, createIfUnknown=False):
        """ Retrieves a symbol by name (str) """

        # make sure symbol is a string
        if not isinstance(symbol, str):
            raise TypeError('symbol must by of type str')

        # look up by name
        for s in self.symbol:
            if s.isNamed(symbol):
                return s

        if createIfUnknown:
            return self.createSymbol(symbol)

        raise KeyError("No symbol named '{}'".format(symbol))

    def createSymbol(self, symbol):
        """ Creates a new symbol and adds it to this namespace """

        # create object by deferring to the symbol class -> DEPENDENCY
        obj = MathSymbol(symbol, self)

        # add and return the results
        self.symbol.add(obj)
        return obj


class MathExpression(CASenabledObject):
    """ Manages mathematical expressions

    The Expression-class is the base-class for equations and inequlities. It
    provides a basic interface to the CAS, to convert textual representations
    into CAS objects. Most importantly, the :py:math:`getInvolvedSymbols`
    extracts all symbols (by name) for insertion into a
    :py:class:`MathematicalNamespace` instace. """

    expression = None

    def __init__(self, expression):
        self.expression = expression

    def __str__(self):
        return str(self.expression)

    def getParent(self):
        raise NotImplementedError()

    def getInvolvedSymbols(self, omit_constants=False, omit_variables=False,
                           only_derivatives=False, createUnknownSymbols=False,
                           exclude_derivatives=False):
        """ Returns an array of Symbol - objects used within this expression

        When associated to a :py:class:`MathematicalNamespace` or one of its
        descendants, this function returns an array of
        :py:class:`modeling.quantity.Symbol` instances corresponding to the
        symbols being used in this expression. Symbols are matched by name,
        hence the required association to a mathematical - namespace.

        For ease of use, constants, variables and non - derivative variables
        can be filtered out using the corresponding omit_ * arguments.

        :param bool omit_constants: omit constants such as "a" in "a*x(t)" ?
        :param bool omit_variables: omit variables such as "x" in "a*x(t)" ?
        :param bool exclude_derivatives: if :py:arg:`omit_variables` is active,
            setting this argument to True causes only symbols that are never
            used within a derivative to be selected (i.e. those symbols that
            figure both in a derivative and normally are not within this list)
        :param bool only_derivatives: overrides all other parameters; this
            returns a list of symbols that are being used in a time-derivative.
        :param bool createUnknownSymbols: the default behavior (with this set
            to its default value of False) when discovering an unknown symbol,
            i.e. a symbol used within the associated expression but not
            registered at the parent's symbol array, is to throw an exception.
            This can be used to force the creation of a new symbol using
            :py:meth:`MathematicalNamespace.createSymbol`
        :raises: :py:exc:`RuntimeError` if the instance is not associated to a
            :py:class:`MathematicalNamespace` instance
        """

        # let's get some names from the expression -> keep the arg of
        # time-variant symbols so that we can identify them later on
        symbols = []
        if not omit_variables:
            symbols += self._CAS().getTimevariantTerms(self.getExpression(),
                    include_arg=True, exclude_derivatives=exclude_derivatives)
        if not omit_constants:
            symbols += self._CAS().getConstantTerms(self.getExpression())
        if only_derivatives:
            symbols = self._CAS().getDerivativeTerms(self.getExpression(),
                                                     include_arg=True)

        # defer to getSymbol; str(symbol) required as the CAS might return
        # its own Symbol objects instead of strings
        collection = set()
        for symbol in symbols:

            # let exceptions from getSymbol leave this context (no try-block)
            collection.add(self.getParent().getSymbolNamed(str(symbol),
                                                        createUnknownSymbols))

        return collection

    def getExpression(self):
        return self.expression


class MathSymbol(CASenabledObject):

    def __str__(self):
        return self.getSymbolName()

    def isNamed(self, name):
        return name == self.getSymbolName()

    def getSymbolName(self):
        raise NotImplementedError()

    def isVariable(self):
        return False

    def isConstant(self):
        return False
