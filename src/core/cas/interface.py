'''
Created on 23.11.2012

@author: ggeorges
'''
import core.common


class CAS_backend(core.common.LascavesimBase):

    def parse_toEquation(self, expr):
        raise NotImplementedError()

    def parse_toExpression(self, expr):
        raise NotImplementedError()

    def getTimevariantTerms(self, expr, terms=None, include_arg=False):
        raise NotImplementedError()

    def getDerivativeTerms(self, expr, terms=None):
        raise NotImplementedError()

    def getConstantTerms(self, expr, terms=None):
        raise NotImplementedError()


class CAS_parser_error(Exception):

    expr = None,

    def __init__(self, msg, expr):
        super(CAS_parser_error, self).__init__()
        self.message = str(msg)
        self.expr = str(expr)

    def __str__(self):
        return "Unable to parse {}: {}".format(self.expr, self.message)

