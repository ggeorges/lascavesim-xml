'''
Created on 23.11.2012

@author: ggeorges
'''
import sympy
from interface import CAS_backend, CAS_parser_error


class interface(CAS_backend):

    def parse_toEquation(self, expr):
        # convert to sympy equation
        if "=" in expr:
            if "==" in expr:
                parts = expr.split("==")
            else:
                parts = expr.split("=")
            if len(parts) != 2:
                raise CAS_parser_error("something's wrong with an '='", expr)
            parts = [self.parse_toExpression(p.strip()) for p in parts]
            return sympy.Eq(*parts)

        else:
            return sympy.Eq(self.parse_toExpression(expr), 0)

    def parse_toExpression(self, expr):

        # uses the local module variable _clashing_symbols to force common
        # symbols such as Q, N, gamma, etc. to be represented as symbol and not
        # the mathematical/sympy functions they designate
        try:
            return sympy.sympify(expr, locals=_clashing_symbols)
        except Exception as e:
            raise CAS_parser_error(e.message, expr)

    def convertToGT0Inequality(self, expr, strict=False):
        if strict:
            return sympy.Gt(expr, 0)
        return sympy.Ge(expr, 0)

    def getTimevariantTerms(self, expr, terms=None, include_arg=False,
                            exclude_derivatives=False):
        """Retrieves function terms of expr

        This function performs the actual work for getTimedependentSymbols.
        It has been refactored into an own method to prevent users from
        accessing the recursive interface directly.

        Given a pre-processed (parse) expression, it returns the names of all
        symbols (in LaScaVeSim's definition) as a list of strings.

        :param expr: a parsed mathematical expression
        :param terms: the result array passed to a deeper level of recursion.
        :param bool include_arg: if false, the '(t)' argument is stripped
        :param bool exclude_derivatives: only include symbols if they are not
            used within a derivative
        :returns: list of CAS symbol objects
        """

        terms = [] if terms == None else terms
        t = sympy.symbols('t')

        # expression may be a lonely function
        if expr.is_Function:
            if len(expr.args) == 1:
                if expr.args[0] == t:
                    func = expr if include_arg else expr.func
                    terms.append(func)
                    return terms

        for term in expr.args:

            # non-resolved stuff needs further processing
            if (term.is_Mul or term.is_Pow or term.is_Add or
                    (term.is_Derivative and not exclude_derivatives)):
                terms = self.getTimevariantTerms(term, terms, include_arg,
                                                 exclude_derivatives)
                continue

            # so we found a function; is it's sole argument 't'?
            if term.is_Function:

                # jump this obscure case
                if not term.args:
                    continue
                # found one?
                if len(term.args) == 1 and term.args[0] == t:
                    if not (term in terms):
                        terms.append(term if include_arg else term.func)
                else:

                    # sub-arguments may still be symbols
                    terms = self.getTimevariantTerms(term, terms, include_arg,
                                                     exclude_derivatives)

        return terms

    def getDerivativeTerms(self, expr, terms=None, include_arg=False,
                           allow_other_symbols=False):
        """Retrieves function terms of expr that are differentiated by time

        :param expr: a parsed mathematical expression
        :param terms: the result array passed to a deeper level of recursion.
        :param bool include_arg: if false, the '(t)' argument is stripped
        :param bool allow_other_symbols: if true, the function does not only
            select solely time-dependent functions derivated by time, but any
            time-derivated function
        :returns: list of str
        """

        terms = [] if terms == None else terms
        t = sympy.symbols('t')

        # expr may already be a derivative
        if expr.is_Derivative:

            if not expr.args:
                self.low_warn("Argument-less derivative: {}".format(expr))
                return terms

            # get the function term, and make sure it is a function of t
            subterm = expr.args[0]
            if not subterm.is_Function:
                self.log_warn("Derivative of a non-function: {}".format(expr))
                return terms
            if not t in subterm.args:
                return terms
            if not allow_other_symbols and len(subterm.args) > 1:
                return terms

            # add the function name, with or without its arguments
            if not include_arg:
                subterm = subterm.func
            if not subterm in terms:
                terms.append(subterm)
            return terms

        if expr.args:

            for term in expr.args:

                # non-resolved stuff needs further processing
                if term.is_Mul or term.is_Pow or term.is_Add or term.is_Function:
                    terms = self.getDerivativeTerms(term, terms)
                    continue

                # make sure its a derivative
                if term.is_Derivative:
                    terms = self.getDerivativeTerms(term, terms)
                    continue

        return terms

    def getConstantTerms(self, expr, terms=None):
        """Retrieves constant terms of expr

        This function performs the actual work for getConstantSymbols.
        It has been refactored into an own method to prevent users from
        accessing the recursive interface directly.

        Given a pre-processed (parse) expression, it returns the names of all
        parameters (in the lascavesim definition). Numeric values and 't' are
        not included.

        :param expr: a parsed mathematical expression
        :param terms: the result array passed to a deeper level of recursion.
        :returns: list of str"""

        t = sympy.symbols('t')

        # stuff we can skip
        if (expr.args == [t] or expr.is_Number or expr == t):
            return []

        # non-resolved stuff needs further processing
        if (expr.is_Mul or expr.is_Pow or expr.is_Add or expr.is_Function
                or expr.is_Derivative or expr.is_Equality):
            terms = []
            for term in expr.args:
                terms += self.getConstantTerms(term, terms)
            return terms

        # what's left are constant terms
        return [str(expr)]

    def detectSymbols(self, expr, symbol_dict, omit_variables=False,
                      omit_constants=False, omit_derivatives=False):

        if not hasattr(symbol_dict, '__iter__'):
            raise TypeError()
        if not isinstance(symbol_dict, dict):
            symbol_dict = dict((str(s), s) for s in symbol_dict)

        symbols = []
        if not omit_variables:
            symbols += self.getTimevariantTerms(expr, include_arg=True,
                                                exclude_derivatives=True)
        if not omit_constants:
            symbols += self.getConstantTerms(expr)
        if not omit_derivatives:
            symbols += self.getDerivativeTerms(expr, include_arg=True)

        for symbol in symbols:
            symbol = str(symbol)
            ans = symbol_dict.get(symbol, None)
            if ans is None:
                ans = symbol_dict.get(symbol.replace("(t)", ""), None)
            if ans is None:
                ans = symbol_dict.get("{}(t)".format(symbol), None)
            if ans is None:
                continue
            yield ans

    def substitute(self, eq, replaceThis, byThat=None):
        """ Substitutes an expression for another """

        # extract equation
        expr = eq.expression if hasattr(eq, 'expression') else eq
        if not hasattr(expr, 'subs'):
            raise TypeError("must be a sympy expression")

        # allow sympy-style lists of tuples
        if byThat is None and hasattr(replaceThis, '__iter__'):
            for this, that in replaceThis:
                eq = self.substitute(eq, this, that)
            return eq

        # if not an expression, convert to symbol
        replaceThis = self._convertArg(replaceThis)
        byThat = self._convertArg(byThat)
        expr = expr.subs([(replaceThis, byThat)]).doit()

        # what to return?
        if not hasattr(eq, 'expression'):
            return expr

        # overwrite the expression
        eq.expression = expr
        return eq

    def _convertArg(self, arg):
        if isinstance(arg, str):
            return sympy.sympify(arg)
        if hasattr(arg, 'getSymbolName'):
            return sympy.sympify(arg.getSymbolName())
        if hasattr(arg, 'expression'):
            return arg.expression
        return arg

    def solve_equations(self, equations, symbols):
        symbol_registry = dict((str(s), s) for s in symbols)
        sol = self.solve(equations, symbols)
        if not hasattr(sol, 'iteritems'):
            sol = sol.pop()
        eqs = ((symbol_registry[str(k)], sympy.Eq(k, v))
               for k, v in sol.iteritems())
        return dict(eqs)

    def solve(self, eq, symbol, onlyOneSolution=False):

        if hasattr(symbol, '__iter__'):
            symbol = [self._convertArg(s) for s in symbol]
        else:
            symbol = self._convertArg(symbol)

        # extract equation
        expr = eq.expression if hasattr(eq, 'expression') else eq

        try:
            sol = sympy.solve(expr, symbol)
            if sol is None or not sol:
                raise RuntimeError()
        except RuntimeError:
            raise RuntimeError("unable to solve {} for {}".
                               format(expr, symbol))
        if onlyOneSolution:
            if len(sol) > 1:
                raise RuntimeError("more than one solution when solving {} "
                                   "for {}".format(expr, symbol))
            return sol[0]

        return sol

    def isTrivialEquation(self, expr):
        if expr.is_Equality and len(expr.args) > 1:
            return expr.args[0] == expr.args[1]
        else:
            return expr == 0

    def simplify(self, expr):
        if not hasattr(expr, 'simplify'):
            raise TypeError("not a sympy exoression")
        return expr.simplify()

    def eliminate(self, expressions, symbols_to_eliminate,
                  breakOnMutuallyDependentSymbols=False):

        # dim
        free = []
        subs = {}

        # symbols should be string lists
        symbols_to_eliminate = [str(s) for s in symbols_to_eliminate]

        # equalities must become expressions for sympy.groebner to work
        exprs = (expr.expression if hasattr(expr, 'expression') else expr
                 for expr in expressions)
        exprs = (expr.args[0] - expr.args[1] if expr.is_Equality else expr
                 for expr in exprs)
        # exprs = sympy.groebner(list(exprs))

        # loop so that ill-ordered exprs array may still be solved
        last_len = 3 * [0]
        while True:
            exprs = self._eliminate(exprs, symbols_to_eliminate, subs, free)
            if not exprs:
                break
            if breakOnMutuallyDependentSymbols:
                raise RuntimeError("Mutually dependent symbols")

            # failsafe: prevent endless loops
            last_len.insert(0, len(exprs))
            if last_len.pop() == last_len[0]:
                print "\n".join("{}: {}".format(", ".join(str(s) for s in
                                                          symbols_to_eliminate
                                                          if expr.has(s)),
                                                str(expr)) for expr in exprs)
                raise RuntimeError("eliminate got stuck")

            # go over the array again if there are still known symbols left
            if any(any(ex.has(s) for s in subs.iterkeys()) for ex in exprs):
                continue

            # remove the left-most symbol in the *_eliminate array
            symbols = (s for s in symbols_to_eliminate
                       if any(expr.has(s) for expr in exprs))
            try:
                symbol = iter(symbols).next()
            except StopIteration:
                raise RuntimeError("unsolvable")
            symbols_to_eliminate.remove(symbol)
            subs[symbol] = self.parse_toExpression(symbol)

        return (subs, free)

    def _eliminate(self, exprs, symbols_to_eliminate, substitutions, free):

        remaining_exprs = []
        for expr in exprs:

            # apply knwown substitutions
            known = (s for s in symbols_to_eliminate if expr.has(s) and
                     s in substitutions.iterkeys())
            for s in known:
                expr = self.substitute(expr, s, substitutions[s])

            # get symbols that shall be eradicated
            symbols = set(s for s in symbols_to_eliminate if expr.has(s))

            # no symbols is greate -> we got an expression without syms_to_el.
            if not symbols:
                free.append(expr)
                continue

            # one symbols is great: we got a new substitution:
            if len(symbols) == 1:
                eliminate = symbols.pop()
                substitutions[eliminate] = self.solve(expr, eliminate,
                                                      onlyOneSolution=True)
                continue

            # with the Groebner basis, at least to the author's understanding,
            # the len(symbols) > 1 should not occur, unless some of the
            # symbols_to_eliminate are mutually dependent. This assumption may
            # be wrong! But until proof of the contrary, the algorithm will
            # assume variables to be mutually dependent when two occur in the
            # same expression. The algorithm then does nothing, so that if there
            # are equations remaining in expr after this, we know there is a
            # dependence in there somewhere
            remaining_exprs.append(expr)

        return remaining_exprs


# apparently, they removed support for clashing symbols (used to be in
# sympy.abc.clashing); so I adapted their old code for my needs
def _clash():
    _latin = list('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    _greek = ('alpha beta gamma delta epsilon zeta eta theta iota kappa '
              'mu nu xi omicron pi rho sigma tau upsilon phi chi psi '
              'omega').split(' ')

    ns = {}
    clash = {}
    exec 'from sympy import *' in ns
    for name, func in ns.iteritems():
        if name in _greek or name in _latin:
            clash[name] = sympy.Symbol(name)
    return clash

_clashing_symbols = _clash()
