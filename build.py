#!/usr/local/bin/python2.7
# encoding: utf-8
"""
build -- CI-script for LaScaVeSim

This script collects simple funcitonality to test and deploy LaScaVesim using
a single command.

:author:     Gil Geoges <gil.georges@lav.mavt.ethz.ch>

:copyright:  2012 LAV-ETHZ. All rights reserved.

:license:    license

:contact:    gil.georges@lav.mavt.ethz.ch
"""

import unittest
import sys
import os
import sphinx

# build-log
import logging


def runUnitTests():

    # discover tests in test-directory
    loader = unittest.TestLoader()
    suite = loader.discover('./tests', '*.py')

    # run them
    result = unittest.TestResult()
    suite.run(result)

    # process errors
    if result.wasSuccessful():
        print
        return True

    # log problems
    for failure in result.errors:
        print failure[1]
    raise RuntimeError("unit-tests failed")



def compileDocumentation():

    # trun sphinx
    sphinx.main(["sphinx-build", "./docs/source", "./docs/build/html"])


def deployAsZip():
    pass

class Logger(object):

    _instance = None

    @classmethod
    def singleton(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    @classmethod
    def getLogger(cls, name="BUILD"):
        l = cls.singleton()
        return l.createLogger(name)

    def __init__(self):
        # create standard log-handler
        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(logging.INFO)
        handler.setFormatter(logging.Formatter("[%(levelname)s] %(name)s: "
                                               "%(filename)s: %(message)s"))
        self.handler = handler

    def createLogger(self, name="BUILD"):
        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)
        logger.addHandler(self.handler)
        return logger


# target definitions
targets = {'test': runUnitTests,
           'docs': compileDocumentation,
           'deploy': deployAsZip }


def main(argv):

    # add the source and test directories
    sys.path.append(os.path.abspath('./src'))
    sys.path.append(os.path.abspath('./tests'))

    # execute all?
    if not argv or "all" in argv:
        argv = targets.iterkeys()

    # execute targets    
    for target in argv:
        print "-----------------------------------------------------------"
        print "TARGET: {}".format(target)
        print "-----------------------------------------------------------"
        try:
            targets[target]()

            # success, tell the user
            print "{} completed successfully".format(target)
            print ""
            print ""

        except RuntimeError as e:
            # damn, something went wrong
            print "{} failed: {}".format(target, e.message)
            sys.exit(1)


# main entry point
if __name__ == '__main__':
    main(sys.argv[1:])

