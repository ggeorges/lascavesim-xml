.. _lascavesim:

Introduction
############

About LaScaVeSim
================

LaScaVeSim stands for Large Scale Vehicle Simulation. It is a software
suite designed to simulate many different configurations of a physical system
in very short time. While its design makes it quite versatile, it has been 
designed to investigate possible future passenger car powertrain designs using
backward facing simulation (applying the quasi-steady state assumption). Hence
the standard library that comes with the tool only contains models for vehicle
simulation, but it is easily extended.

The main reason for the existence of this tool is that it combines key aspects
of different tools I've been using previously in order to accomplish the same:

   * Written in Python, it can rely on much the same funcitonality as the previous 
     MATLAB implementation of the tool. 

   * Inspired by Modelica, it is based on a 
     :abbr:`CAS (Computer Algebra System)` (`SymPy <http://sympy.org>`_), so that 
     it can autonomously convert equations into the form needed by the solver. 
     This accelerates modeling enormously, as modeling is reduced to what it 
     should be: finding the right equation for the task.
   
   * Inspired by ADVISOR (2003) it can switch from backward to forward 
     simulation if certain constraints prohibit the system from following the
     imposed state trajectory. Even more so, thanks to the CAS, it can actually
     compute bounding inputs ahead of time (conventional backward systems can
     only do this by using iterative methods). Yet it is much more flexible 
     than the rigid Simulink models of ADVISOR (although it doesn't even begin
     to compete with their component library).
   
   * Inspired by IDSC's dpm function, there is a deterministic dynamic 
     programming solver that allows to find the optimum state trajectory for 
     hybrid electric vehicles. Computation speed has been improved through
     massive parallelization.