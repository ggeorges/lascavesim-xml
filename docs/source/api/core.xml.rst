xml Package
===========

:mod:`parser` Module
--------------------

.. automodule:: core.xml.parser
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`persistence` Module
-------------------------

.. automodule:: core.xml.persistence
    :members:
    :undoc-members:
    :show-inheritance:

