core Package
============

The core-package provides basic funcitonality to all components of `lascavesim`.
More precisely, it consists of rather generic base classes that can be used to 
e.g. make a more specialized class XML-serializable. The actual business logic 
is implemented in other packages. However, they make heavy use of the common 
features introduced below.

The package is subdivided into the following modules:

    * :py:mod:`core.xmlBackend`: 
        It provides a very convenient way of making a 
        :abbr:`POPO (plain old python object)` serializable to XML and back. All 
        input-data and configuration tasks in LaScaVeSim are handled that
        way (see :ref:`lascavesim`).
    * :py:mod:`core.namedCollection`: 
        This provides primitives to link 
        :abbr:`POPOs (plain old python objects)` into
        a hierarchical structure. This mechanism is the basis of the modeling
        approach of `lascavesim`.

Subpackages
-----------

.. toctree::

    core.cas
    core.xml

