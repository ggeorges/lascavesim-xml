src
===

.. toctree::
   :maxdepth: 4

   __main__
   core
   modeling
