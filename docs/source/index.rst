.. LaScaVeSim documentation master file, created by
   sphinx-quickstart on Fri Nov 16 18:42:54 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

LaScaVeSim's documentation
##########################

This documentation covers the basic principles behind LaScaVeSim, discusses the
models of the standard library and also contains the API documentation.

There will be a:
  * User's manual
  * Developper's manual (technical documentation)
  * Scientific manual

Just like LaScaVeSim itself, this document is still very much a work in progress.

Contents:
=========

.. toctree::
   :maxdepth: 2
   
   intro
   api_doc