
Technical documentation
#######################

The API-documentation is intended for those who want/need to modify/extend
LaScaVeSim.

About this software
===================

LaScaVeSim is written in Python (version 2.7).

External dependencies
---------------------

LaScaVeSim uses quite a few external libraries, most of which ship with the
Python standard library though.

Within the standard library:
   * Pyhon's `minidom <http://docs.python.org/2/library/xml.dom.minidom.html>`_
     (xml.dom.minidom) 


:abbr:`API (application programming interface)` documentation
=============================================================

As mentioned earlier, LaScaVeSim is a suite of five integrated tools:
   * the modeling application
   * the design optimizer
   * the pre-processor / "compiler"
   * the backward/forward solver
   * the post-processor

The source-tree of LaScaVeSim therefore consists of 5 "business-logic" 
packages (meaning directories) and a shared functionality package.

.. toctree::
    api/modules
   
 