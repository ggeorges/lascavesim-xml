import sys
sys.path.append('../src')
import unittest
import mock
from modeling.logicalentity import System, Component, Aspect
from modeling.expression import Equation
from modeling.quantity import Variable, Constant, Effort, Flow
from modeling.ports import PhysicalPort, PortConnection
from simulation.graphfactory import equationgraph_factory, _get_symbol_registry
import simulation.equationgraph
import loopy_equationgraph
import simulation.model
from simulation.equationgraph.algebraicloop import remove_loops


class FunctionalTest(unittest.TestCase):

    def setUp(self):
        self.graph = None
        self.symbols = None
        self.equations = None

        loopy_equationgraph.Example1().copy_vars(self)

    def test_graph(self):
        self.graph.condition()
        remove_loops(self.graph)
        model = simulation.model.Simulation(self.graph)

        # make sure the model is computable, and that all symbols are resolved
        known = set(s for s in self.graph.super_symbols if s.is_source())
        for step in model.steps:
            for symbol in step.needs():
                self.assertIn(symbol, known)
            known.update(step.yields())
        self.assertItemsEqual(known, self.graph.super_symbols)

        simulation.model.Export2Python(model)
