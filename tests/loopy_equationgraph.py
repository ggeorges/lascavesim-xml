from simulation.equationgraph import EquationGraph


class GraphExample(object):

    def __init__(self):
        self.draw()

    def copy_vars(self, testcase):
        fields = ('equations', 'symbols', 'graph', 'integrals', 'differntials',
                  'loops')
        for field in fields:
            if not hasattr(testcase, field) or not hasattr(self, field):
                continue
            setattr(testcase, field, getattr(self, field))


class Example1(GraphExample):

    def draw(self):

        graph = EquationGraph()
        s = list(graph.add_supersymbol() for i in xrange(8))
        s.append(graph.add_supersymbol(differential_of=s[0]))
        s += list(graph.add_supersymbol() for i in xrange(4))
        s.append(graph.add_supersymbol(differential_of=s[12]))
        s.append(graph.add_supersymbol(modulated=True))
        S = graph.add_localsymbol
        self.symbols = []
        self.equations = []
        e = graph.add_equation("s1(t)+s2(t)-s3(t)=0",
                               S("s3", super_symbol=s[2], dependent=True),
                               S("s1", super_symbol=s[0]),
                               S("s2", super_symbol=s[1]))
        self.equations.append(e)

        e = graph.add_equation("s2(t)=1.2*s1(t)",
                               S("s2", super_symbol=s[1], dependent=True),
                               S("s1", super_symbol=s[0]))
        self.equations.append(e)

        e = graph.add_equation("s11(t)=s2(t)+s3(t)-s12(t)-s4(t)",
                               S("s11", super_symbol=s[10], dependent=True),
                               S("s2", super_symbol=s[1]),
                               S("s3", super_symbol=s[2]),
                               S("s4", super_symbol=s[3]),
                               S("s12", super_symbol=s[11]))
        self.equations.append(e)

        e = graph.add_equation("s5(t)=s4(t)",
                               S("s5", super_symbol=s[4], dependent=True),
                               S("s4", super_symbol=s[3]))
        self.equations.append(e)

        e = graph.add_equation("s6(t)=-s4(t)",
                               S("s6", super_symbol=s[5], dependent=True),
                               S("s4", super_symbol=s[3]))
        self.equations.append(e)

        e = graph.add_equation("s7(t)=s5(t)*s6(t)",
                               S("s7", super_symbol=s[6], dependent=True),
                               S("s5", super_symbol=s[4]),
                               S("s6", super_symbol=s[5]))
        self.equations.append(e)

        e = graph.add_equation("s8(t)=s9(t).diff(t)+s7(t)",
                               S("s8", super_symbol=s[7], dependent=True),
                               S("s9", super_symbol=s[8], differential_order=1),
                               S("s7", super_symbol=s[6]))
        self.equations.append(e)

        e = graph.add_equation("s9(t).diff(t)=s8(t)*1.1",
                               S("s9", super_symbol=s[8], dependent=True,
                                 differential_order=1),
                               S("s8", super_symbol=s[7]))
        self.equations.append(e)

        e = graph.add_equation("s10(t)=s3(t)**2",
                               S("s10", super_symbol=s[9], dependent=True),
                               S("s3", super_symbol=s[2]))
        self.equations.append(e)

        e = graph.add_equation("s4(t)=s11(t)*2",
                               S("s4", super_symbol=s[3], dependent=True),
                               S("s11", super_symbol=s[10]))
        self.equations.append(e)

        e = graph.add_equation("s12(t)=s4(t)+s15(t)",
                               S("s12", super_symbol=s[11], dependent=True),
                               S("s4", super_symbol=s[3]),
                               S("s15", super_symbol=s[14]))
        self.equations.append(e)

        # add a "tank", i.e. sth. that is integrated, but is no state
        e = graph.add_equation("s14(t).diff(t)=-s10(t)",
                               S("s14", super_symbol=s[13], dependent=True,
                                 differential_order=1),
                               S("s10", super_symbol=s[9]))
        self.equations.append(e)

        self.graph = graph
        self.symbols = s
        self.loops = [([self.symbols[i] for i in (3, 10, 11)] +
                       [self.equations[i] for i in (2, 10, 9)]),
                      ([self.symbols[i] for i in (7, 8)] +
                       [self.equations[i] for i in (6, 7)])]
        self.integrals = []
        self.differentials = []
