'''
Created on Dec 16, 2012

@author: gil
'''
import unittest
import mock
import core.xml.parser
import core.xml.persistence
import core.xml.delayed


class TestParserEssentials(unittest.TestCase):

    def setUp(self):

        # create mockup serializable class
        cls = mock.Mock()
        setattr(cls, '_xml_tagName', 'tag')
        setattr(cls, '_xml_namespace', 'something')
        setattr(cls, '_xml_children', [cls])
        self.xmlClass = cls

        # local class
        self.modeClass = core.xml.parser.modeBase

    def tearDown(self):
        self.xmlClass = None
        self.modeClass = None
        self.modeObject = None
        core.xml.parser.modeBase._parser = None

    def testConstructor(self):
        """ Checking whether the modBase constructor assigns properties """

        # must have the xml_tagName attribute
        with self.assertRaises(TypeError):
            self.modeClass(None, object())

        # check proper attribute handling
        self.modeObject = self.modeClass(None, self.xmlClass, None, "sth")
        self.assertEqual(self.modeObject.my_tagName,
                         self.xmlClass._xml_tagName)
        self.assertEqual(self.modeObject.my_namespace,
                         self.xmlClass._xml_namespace)
        self.assertEqual(self.modeObject.namespaces, "sth")
        self.assertEqual(self.modeObject.my_class, self.xmlClass)
        self.assertEqual(self.modeObject.allowed_children, [self.xmlClass])

    def testGetParser(self):
        """ Checks the get-parser method """

        # get new instance (testConstructor must run before)
        cls = mock.Mock()
        mode = self.modeClass(None, self.xmlClass, None, None)
        mode.getParser(cls)

        # get it back
        self.assertEqual(mode.getParser(), cls)

        # try overwriting
        self.assertEqual(mode.getParser(None), cls)

    def testTakeOver(self):
        """ Test whether take-over method assigns method handles properly """

        # pass on mock-parser
        cls = mock.Mock()
        mocks = [mock.Mock() for i in xrange(5)]
        cls.StartElementHandler = mocks[0]
        cls.EndElementHandler = mocks[1]
        cls.CharacterDataHandler = mocks[2]
        cls.StartCdataSectionHandler = mocks[3]
        cls.EndCdataSectionHandler = mocks[4]
        mode = self.modeClass(None, self.xmlClass, None, None)
        mode.getParser(cls)
        mode.takeOver()

        # check if it got taken over
        self.assertEqual(cls.StartElementHandler, mode.startElement)
        self.assertEqual(cls.EndElementHandler, mode.endElement)
        self.assertEqual(cls.CharacterDataHandler, mode.characterData)
        self.assertEqual(cls.StartCdataSectionHandler, mode.cdataBegin)
        self.assertEqual(cls.EndCdataSectionHandler, mode.cdataEnd)


class TestParserModes(unittest.TestCase):

    xmlClass = None
    modeObject = None
    modeClass = None
    fakeParser = None
    fakeParent = None
    constructor_attributes = None
    constructor_localNamespaces = None

    def setUp(self, modeClass=core.xml.parser.modeBase):

        # create mockup serializable class
        cls = mock.Mock()
        setattr(cls, '_xml_tagName', 'tag')
        setattr(cls, '_xml_namespace', 'something')
        setattr(cls, '_xml_children', [cls])
        self.xmlClass = cls

        # local class
        self.modeClass = modeClass
        self.fakeParser = self._new_mockParser()
        self.modeObject = self._new_instance()
        self.modeObject.getParser(self.fakeParser)
        self.modeObject.takeOver()

    def tearDown(self):
        self.xmlClass = None
        self.modeClass = None
        self.modeObject = None
        core.xml.parser.modeBase._parser = None

    def _new_instance(self, attributes=None, localNamespaces=None):
        self.fakeParent = mock.Mock()
        self.fakeParent.exit = mock.Mock()
        self.fakeParent.parent_mode = None

        def raiseKeyError(arg):
            raise KeyError()

        # constructor arguments
        attributes = (self.constructor_attributes if attributes is None
                      else attributes)
        localNamespaces = (self.constructor_localNamespaces
                           if localNamespaces is None else localNamespaces)

        self.fakeParent.getNamespace = raiseKeyError
        return self.modeClass(self.fakeParent, self.xmlClass, attributes,
                              localNamespaces)

    def _new_mockParser(self):

        # create mock-parser
        cls = mock.Mock()
        mocks = [mock.Mock() for i in xrange(6)]
        cls.StartElementHandler = mocks[0]
        cls.EndElementHandler = mocks[1]
        cls.CharacterDataHandler = mocks[2]
        cls.StartCdataSectionHandler = mocks[3]
        cls.EndCdataSectionHandler = mocks[4]
        cls.CharacterDataHandler = mocks[5]
        cls.CurrentLineNumber = 0
        return cls


class TestParserNamespaceHandling(unittest.TestCase):

    def testSplitIntoNamespacePrefixAndName(self):
        """ Split "namespace:tag"-tags into namespace and tag """

        # local copy
        f = core.xml.parser.splitIntoNamespacePrefixAndName

        # run testcases
        self.assertEqual(f("eq:test"), ("eq", "test"))
        self.assertEqual(f("xmlns:test"), ("xmlns", "xmlns:test"))
        self.assertEqual(f("test"), ("xmlns", "test"))

    def testNamespaceDefinitions(self):
        """ Checks XMLnamespaceDefinition-class behavior """

        # local test elements
        c = core.xml.parser.XMLnamespaceDefinitions
        attr = {"testit": "value", "xmlns:test": "some thing"}

        # test parsing attributes
        obj = c(attr)
        self.assertEqual(obj['test'], "some thing")
        self.assertNotIn('testit', obj)
        self.assertNotIn('xmlns:test', attr)
        self.assertEqual(attr['testit'], 'value')
        self.assertNotIn('xmlns', obj)

        # add a default namespace
        attr = {"xmlns": "now he was a weenie"}
        obj.parseAttributes(attr)
        self.assertNotIn('xmlns', attr)
        self.assertEqual(obj['xmlns'], "now he was a weenie")


class TestParserModeBase(TestParserModes):

    def testGetLineNumber(self):
        """ Checks line-number is picked up from parser"""

        self.fakeParser.CurrentLineNumber = 3.1415
        self.assertEqual(self.modeObject.getLineNumber(), 3.1415)

    def testNamespaceHandling(self):
        """ Checks namespace arguments are processed and gathered correctly"""

        attr = {"xmlns:test": "something", "xmlns": "something else",
                "and": "something different"}
        self.assertEqual(self.modeObject.handleNamespaces("myself", attr),
                         ("something else", "myself",
                         {"test": "something", "xmlns": "something else"}))
        self.modeObject.namespaces = {"test": "sth"}
        self.assertEqual(self.modeObject.getNamespace("test"), "sth")
        with self.assertRaises(KeyError):
            self.modeObject.getNamespace("mister")
        self.assertEqual(self.modeObject.resolveXMLname("test:moin"),
                         ("sth", "moin"))

    def testExit(self):
        """ Tests exit-routine """

        # doing the mocking
        obj = mock.Mock()
        func = mock.Mock()
        func2 = mock.Mock()
        self.modeObject.my_object = obj
        self.modeObject.parent_mode.addChildObject = func
        self.modeObject.parent_mode.takeOver = func2

        # method call
        self.modeObject.exit()

        # does stuff work?
        func.assert_called_once_with(obj)
        func2.asser_called_conce_with()
        self.assertEqual(self.modeObject.parent_mode, None)

    def testOpeningElement(self):
        """ The startElement handler is virtual; exception must be risen """

        with self.assertRaises(NotImplementedError):
            self.fakeParser.StartElementHandler('sth', {})

    @mock.patch("inspect.getargspec")
    def testGetInitArgsNormal(self, argspec):
        """ Check arguments are parsed correctly """

        # test normally
        import inspect
        argspec.return_value = inspect.ArgSpec(["self", "wuff", "test"], None,
                                               None, [None, None])
        self.modeObject.my_attributes = {"test": "it fell off"}
        args = self.modeObject._getInitArguments()
        self.assertEqual(args, [None, "it fell off"])

    @mock.patch("inspect.getargspec")
    @mock.patch("core.xml.parser.XMLParsingProblem")
    def testGetInitArgsUnkownArg(self, problem, argspec):
        """ Check a warning is issued on unknown args """

        import inspect
        self.modeObject.my_attributes = {"test": "1", "wrongArg": "2"}
        argspec.return_value = inspect.ArgSpec(["self", "test"], None,
                                               None, None)
        args = self.modeObject._getInitArguments()
        self.assertEqual(args, ['1'])

    @mock.patch("inspect.getargspec")
    def testGetInitArgsMissingArg(self, argspec):
        """ Check missing arguments cause an exception """

        import inspect
        self.modeObject.my_attributes = {"test": "it fell off"}
        argspec.return_value = inspect.ArgSpec(["self", "wuff", "test"], None,
                                               None, None)
        with self.assertRaises(core.xml.parser.XMLParserError):
            self.modeObject._getInitArguments()

    @mock.patch("inspect.getargspec")
    def testInitMyClass(self, argspec):

        self.modeObject.my_attributes = {"test": "1234", "wuff": "miau"}
        import inspect
        argspec.return_value = inspect.ArgSpec(["self", "wuff", "test"], None,
                                               None, None)
        self.xmlClass.__name__ = "something"
        self.modeObject.initMyClass()
        self.xmlClass.assert_called_once_with("miau", "1234")

    def testDelayedActionsRecognition(self):

        # create mock objects
        action = mock.Mock(core.xml.delayed.DelayedActionInterface)
        mockParent = mock.Mock()
        mockParent.adopt = mock.Mock()
        self.modeObject.my_object = mockParent

        # this should result in the delayed-context being called instead of ad.
        self.modeObject.addChildObject(action)
        assert not self.modeObject.my_object.adopt.called
        action.setLoseElementContext.assert_called_with(mockParent)


class TestParserModeDocument(TestParserModes):

    def setUp(self):

        self.modeClass = core.xml.parser.modeDocument
        self.xmlClass = mock.Mock()
        self.xmlClass._xml_tagName = "root"
        self.xmlClass._xml_namespace = "something"
        self.xmlClass.__name__ = "root"
        self.modeObject = self._new_instance()

    def _new_instance(self):

        # fake parser -> passed as argument here
        self.fakeParser = self._new_mockParser()

        # add fake root-element class
        return self.modeClass(self.xmlClass, self.fakeParser)

    def testWrongRootElement(self):

        with self.assertRaises(core.xml.parser.XMLParserError):
            self.modeObject.startElement("root2", {"xmlns": "something"})

    def testWrongRootElementNamespace(self):

        with self.assertRaises(core.xml.parser.XMLParserError):
            self.modeObject.startElement("root", {"xmlns": "something else"})

    def testRootElementFound(self):

        # simulate parser callback
        func = mock.Mock()
        self.modeObject.getNextMode = func
        self.modeObject.startElement("root", {"xmlns": "something"})
        func.assert_called_once_with("root", {}, "something",
                                     {"xmlns": "something"})


class TestParserModeSwitch(TestParserModes):

    def setUp(self):
        TestParserModes.setUp(self)
        sub = mock.Mock()
        sub._xml_tagName = "sub"
        sub._xml_namespace = "something"
        sub.__name__ = "sub"
        self.subElement = sub
        self.modeObject.allowed_children = [self.subElement]

    def testChildLessChild(self):
        """ Checks wether empty child-array causes ContentFreeElement """

        self.modeObject.allowed_children[0]._xml_children = None
        mode = self.modeObject.getNextMode("sub", {}, "something",
                                          {"xmlns": "something"})
        self.assertIsInstance(mode, core.xml.parser.modeContentFreeElement)
        self.assertEqual(mode.my_tagName, "sub")
        self.assertEqual(mode.my_namespace, "something")
        self.assertEqual(mode.my_class, self.subElement)
        self.assertEqual(mode.parent_mode, self.modeObject)

    def testChildWithChildren(self):
        """ Checks whether child-list causes ElementWithChildren """

        self.modeObject.allowed_children[0]._xml_children = [self.subElement]
        mode = self.modeObject.getNextMode("sub", {}, "something",
                                          {"xmlns": "something"})
        self.assertIsInstance(mode, core.xml.parser.modeElementWithChildren)
        self.assertEqual(mode.my_tagName, "sub")
        self.assertEqual(mode.my_namespace, "something")
        self.assertEqual(mode.my_class, self.subElement)
        self.assertEqual(mode.parent_mode, self.modeObject)

    def testChildWithOnlyText(self):
        """ Checks if text-only property causes ChildWithOnlyText """

        self.modeObject.allowed_children[0]._xml_children = [self.subElement]
        self.modeObject.allowed_children[0]._xml_textProperty = "txt"
        mode = self.modeObject.getNextMode("sub", {}, "something",
                                          {"xmlns": "something"})
        self.assertIsInstance(mode, core.xml.parser.modeElementWithOnlyText)
        self.assertEqual(mode.my_tagName, "sub")
        self.assertEqual(mode.my_namespace, "something")
        self.assertEqual(mode.my_class, self.subElement)
        self.assertEqual(mode.parent_mode, self.modeObject)

    def testUnkownElement(self):
        """ Wrong or unknown tag must cause 'None' """

        self.modeObject.allowed_children = []
        mode = self.modeObject.getNextMode("sub", {}, "something",
                                          {"xmlns": "something"})
        self.assertIsNone(mode)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
