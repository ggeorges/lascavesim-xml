"""
 test-case definition

:date: 11.01.2013
:author: ggeorges
"""

import unittest
import mock
import modeling.ports
import modeling.quantity


class mockNameSearchabeConnection(list):

    def __getitem__(self, key):
        if isinstance(key, str):
            for el in self:
                if el.name == key:
                    return el
            raise KeyError()
        return list.__getitem__(self, key)


class TestPort(unittest.TestCase):

    def makeSignals(self, efforts=0, flows=0, inputs=0, outputs=0):

        def make_signal(i, effort=False, flow=False, input=False, output=False):
            if sum((effort, flow, input, output)) != 1:
                raise RuntimeError("invalid mock signal spec")
            signal = mock.Mock(modeling.quantity.Signal)
            signal.isPhysical = mock.Mock(return_value=(effort or flow))
            signal.isEffort = mock.Mock(return_value=effort)
            signal.isFlow = mock.Mock(return_value=flow)
            signal.isLogical = mock.Mock(return_value=(input or output))
            signal.isInput = mock.Mock(return_value=input)
            signal.isOutput = mock.Mock(return_value=output)
            signal.name = "signal{}".format(i)
            return signal

        signals = mockNameSearchabeConnection()
        offset = 0
        for number, kwarg in ((efforts, {'effort':True}),
                              (flows, {'flow':True}), (inputs, {'input':True}),
                              (outputs, {'output':True})):
            signals += [make_signal(i + offset, **kwarg)
                        for i in xrange(number)]
            offset += number
        return signals

    def testSignalInventory(self):
        port = modeling.ports.Port('someport')

        for t in ((1, 3, 2, 1), (0, 1, 12, 0), (0, 0, 0, 0), (0, 0, 1, 1)):
            port.signal = set(self.makeSignals(outputs=t[0], inputs=t[1],
                                               flows=t[2], efforts=t[3]))
            self.assertEqual(port.signalInventory(), t)

    def testIsPhysical(self):
        port = modeling.ports.Port('someport')

        # test a normal case
        port.signal = self.makeSignals(flows=3, efforts=2)
        self.assertTrue(port.isPhysical())

        # the config may be of, but it should still work
        port.signal = self.makeSignals(flows=3, efforts=2, inputs=1)
        self.assertTrue(port.isPhysical())

        # test a negative
        port.signal = self.makeSignals(inputs=1)
        self.assertFalse(port.isPhysical())

    def testIsLogical(self):
        port = modeling.ports.Port('someport')

        # test a normal case
        port.signal = self.makeSignals(inputs=3, outputs=2)
        self.assertTrue(port.isLogical())

        # the config may be of, but it should still work
        port.signal = self.makeSignals(flows=3, efforts=2, inputs=1)
        self.assertTrue(port.isLogical())

        # test a negative
        port.signal = self.makeSignals(efforts=3, flows=2)
        self.assertFalse(port.isLogical())

    def testIsConnected(self):
        port = modeling.ports.Port('someport')
        self.assertFalse(port.isConnected())

    @mock.patch('modeling.ports.Junction')
    def testConnectTo(self, connection):
        """ Use connectTo on a Junction and try to get it back """

        # test with conenction objhect
        port = modeling.ports.Port('someport')
        port.connectTo(connection)
        connection.connectPort.assert_called_with(port)

        # test with port, first a connected one -> should call connectPort
        # on the remote port's connection
        otherport = mock.Mock(modeling.ports.Port)
        otherport.name = "otherport"
        otherport.isConnected(return_value=True)
        otherport.connection = mock.Mock(modeling.ports.Junction)
        port.connectTo(otherport)
        otherport.connection.connectPort.assert_called_with(port)

        # then with unconnected port | no, this only works with a live port
        # connection (Because otherwise there si no feedback -> endless loop)

    def testCanBeConnectedTo(self):

        # one port
        port = modeling.ports.Port('someport')
        port.signal = self.makeSignals(flows=2, efforts=1)

        # another that fits perfectly
        compatible_port = modeling.ports.Port('anotherport')
        compatible_port.signal = self.makeSignals(flows=2, efforts=1)
        self.assertTrue(port.canBeConnectedTo(compatible_port))

        # another that fits perfectly
        incompatible_port = modeling.ports.Port('anotherport')
        incompatible_port.signal = self.makeSignals(2, 1)
        incompatible_port.signal[1].name = "haha"
        self.assertFalse(port.canBeConnectedTo(incompatible_port))

        # now with a physical/logical mismatch
        incompatible_port = modeling.ports.Port('anotherport')
        incompatible_port.signal = self.makeSignals(0, 0, 2, 1)
        self.assertFalse(port.canBeConnectedTo(incompatible_port))

        # then with two logical ports
        port = modeling.ports.Port('someport')
        port.signal = self.makeSignals(inputs=1, outputs=2)
        incompatible_port = modeling.ports.Port('anotherport')
        incompatible_port.signal = self.makeSignals(inputs=1, outputs=1)
        self.assertFalse(port.canBeConnectedTo(incompatible_port))

        incompatible_port.signal[0].name = "signal1"
        incompatible_port.signal[1].name = "signal0"
        self.assertTrue(port.canBeConnectedTo(incompatible_port))

    @mock.patch('modeling.logicalentity.System')
    def testIsSystemPort(self, sys):

        # simulate linked state to a system-object
        port = modeling.ports.Port("someport")
        port.getParent = mock.Mock(return_value=sys)

        sys.isSystem = mock.Mock(return_value=False)
        self.assertFalse(port.isSystemPort())

        sys.isSystem = mock.Mock(return_value=True)
        self.assertTrue(port.isSystemPort())


class TestPortConnection(unittest.TestCase):

    def testConnectPort(self):

        port = mock.Mock(modeling.ports.Port)

        # no pre-existing connections:
        connection = modeling.ports.Junction()
        connection.connectPort(port)

        # check that action to add it was taken
        port.setConnection.assert_called_with(connection)
        self.assertFalse(port.canBeConnectedTo.called)

        # with ports the compatibility check has to be made; first, let's see
        # what happens if it fails
        otherport = mock.Mock(modeling.ports.Port)
        otherport.canBeConnectedTo = mock.Mock(return_value=False)
        with self.assertRaises(RuntimeError):
            connection.connectPort(otherport)
        self.assertIn(port, connection.ports)
        self.assertEqual(len(connection.ports), 1)

        # now if it is successfull
        otherport.canBeConnectedTo = mock.Mock(return_value=True)
        connection.connectPort(otherport)
        self.assertIn(otherport, connection.ports)
        self.assertEqual(len(connection.ports), 2)



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
