'''
Created on 18.12.2012

@author: ggeorges
'''
import unittest
import core.cas.sympy_interface
import sympy


class TestCAS(unittest.TestCase):
    """ This only tests behavior constructed around the CAS; the CAS itself is
        assumed to be in perfect working order """

    def setUp(self):
        self.cas = core.cas.sympy_interface.interface()

    def testParseToEquation(self):
        """ Check whether equations are converted to the proper CAS object """

        x = self.cas.parse_toExpression('x')
        for expr in ["2*x=4", "3*x==6", "2*x =4", "3*x= 6", "x-2"]:
            eq = self.cas.parse_toEquation(expr)
            self.assertEqual(sympy.solve(eq, x), [2])

    def testGgetTimevariantTerms(self):
        """ Check if time-variant terms are identified correctly """

        for expr in ["3*x+y(t)", "3*x(y(t))", "3*y(t).diff(t)", "y(t)*x^-t",
                     "log(x)+3*x^(7-3*t+9^7^y(t))"]:
            expr = self.cas.parse_toExpression(expr)
            syms = [str(s) for s in self.cas.getTimevariantTerms(expr)]
            self.assertEqual(syms, ['y'])

    def testGetDerivativeTerms(self):
        """ Check if functions derivated by time (no matter if they also depend
            on other symbols) are correctly extracted """

        for expr in ["3*y(t).diff(t)^x", "3*x(z, t).diff(z)+y(t).diff(t)"]:
            expr = self.cas.parse_toExpression(expr)
            syms = [str(s) for s in self.cas.getDerivativeTerms(expr)]
            self.assertEqual(syms, ['y'])

    def testGetConstantTerms(self):
        """ Check if constants get extracted reliably """

        for expr in ["3*log(y)^x(t)*t-7", "f(t*y, d(t))", "3*x(t)*y+7-y^2"]:
            expr = self.cas.parse_toExpression(expr)
            syms = [str(s) for s in self.cas.getConstantTerms(expr)]
            self.assertEqual(syms, ['y'])


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
