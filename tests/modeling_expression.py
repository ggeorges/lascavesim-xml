"""
 test-case definition

:date: 19.12.2012
:author: ggeorges
"""

import unittest
import mock
import modeling.expression
import core.cas.sympy_interface


class TestConstructors(unittest.TestCase):

    def setUp(self):
        modeling.expression.Expression.cas = core.cas.sympy_interface.interface()

    def testExpression(self):

        # test safeguards
        with self.assertRaises(TypeError):
            modeling.expression.Expression()

        # set an expression and get it back via __str__ (tests the constructor,
        # setExpression - must convert x*x -> x**2 and the __str__ method)
        expr = modeling.expression.Expression("x*x")
        self.assertEqual(str(expr), 'x**2')

    def testEquation(self):

        # test safeguards:
        with self.assertRaises(TypeError):
            modeling.expression.Equation()

        # same as testExpression, but there must be an additional '==0'
        expr = modeling.expression.Equation("x*x")
        self.assertEqual(str(expr), 'x**2 == 0')


class TestSymbolHandling(unittest.TestCase):

    def testGetInvolvedSymbols(self):

        expr = modeling.expression.Expression("a*x(t)+b")

        # check the constructor -> no "orphan"-expressions
        with self.assertRaises(RuntimeError):

            # simulate no parent
            expr.getParent = mock.Mock(return_value=None)
            expr.getInvolvedSymbols()

            # simulate parent lacking the interface
            expr.getParent = mock.Mock(return_value=mock.Mock(object))
            expr.getInvolvedSymbols()

        # generate mock-names-pace that knows the symbols
        ns = mock.Mock(modeling.expression.MathematicalNamespace)

        def mock_func(arg2, createSyms=False):
            known = {'a': 1, 'x(t)': 2.2, 'b': 5}
            if not arg2 in known:
                raise KeyError('unknown {}'.format(arg2))
            return known[arg2]

        ns.getSymbol = mock_func
        ns.createSymbol = mock.Mock()
        expr.getParent = mock.Mock(return_value=ns)

        # execute and see what happened
        rep = expr.getInvolvedSymbols()
        self.assertEqual(len(rep), 3)
        self.assertIn(1, rep)
        self.assertIn(2.2, rep)
        self.assertIn(5, rep)

        # check filters
        self.assertEqual(expr.getInvolvedSymbols(omit_constants=True),
                         set((2.2,)))
        self.assertEqual(expr.getInvolvedSymbols(omit_variables=True),
                         set((1, 5)))
        self.assertEqual(expr.getInvolvedSymbols(omit_variables=True,
                                                 omit_constants=True), set())

        # try this again, but this time with a "missing" symbol
        expr = modeling.expression.Expression("a*x(t)+b+c")
        expr.getParent = mock.Mock(return_value=ns)
        with self.assertRaises(KeyError):
            rep = expr.getInvolvedSymbols()


class TestMathematicalNamespace(unittest.TestCase):

    def testGetSymbol(self):

        # create instance
        ns = modeling.expression.MathematicalNamespace()
        mock_var = mock.Mock(modeling.quantity.Variable)
        mock_var.isConstant = mock.Mock(return_value=False)
        ns.createSymbol = mock.Mock(return_value=mock_var)

        # add mock objects
        def mock_baker():
            for sym in ['a', 'x(t)', 'b', 'ze_0(t)', 'phi_x_max']:
                m = mock.Mock()
                m.name = sym
                m.isVariable = mock.Mock(return_value=('(t)' in sym))
                m.isConstant = mock.Mock(return_value=(not '(t)' in sym))
                yield m
        ns.symbol = [m for m in mock_baker()]

        # get all symbols, out of order
        self.assertEqual(ns.getSymbol('b'), ns.symbol[2])
        self.assertEqual(ns.getSymbol('phi_x_max'), ns.symbol[4])

        # get variable, with and without '(t)'
        self.assertEqual(ns.getSymbol('x'), ns.symbol[1])
        self.assertEqual(ns.getSymbol('x(t)'), ns.symbol[1])

        # and test error response
        with self.assertRaises(KeyError):
            ns.getSymbol('tere')
            ns.getSymbol('y(t)')
        with self.assertRaises(TypeError):
            ns.getSymbol([])
        with self.assertRaises(RuntimeError):
            ns.getSymbol('a(t)')

        # work with unknown symbols
        rep = ns.getSymbol('c(t)', createIfUnknown=True)
        ns.createSymbol.assert_called_with('c(t)')

    @mock.patch('modeling.quantity.Symbol')
    def testCreateSymbol(self, cls):
        """ Check the call to Symbol.createSymbol is made properly """

        cls.createSymbol = mock.Mock(return_value=1234)
        ns = modeling.expression.MathematicalNamespace()
        ns.symbol = set()
        self.assertEqual(ns.createSymbol("x(t)", "test", "test2", 1.44), 1234)
        cls.createSymbol.assert_called_with("x(t)", "test", "test2", 1.44)
        self.assertIn(1234, ns.symbol)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
