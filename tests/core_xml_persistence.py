'''
Created on 18.12.2012

@author: ggeorges
'''
import unittest
import mock
import core.xml.persistence
import core.xml.parser


class TestXmlizeDecorator(unittest.TestCase):

    def setUp(self):
        self.xmlize = core.xml.persistence.xmlize
        self.mockClass = mock.Mock(object())
        self.mockClass.__name__ = "SomeClass"

        def mockChildMaker():
            for i in xrange(4):
                m = mock.Mock(object())
                m.__name__ = "MockChild{}".format(i + 1)
                m._xml_tagName = "tag{}".format(i)
                yield m
        self.mockChildren = [m for m in mockChildMaker()]

    def tearDown(self):
        self.xmlize = None
        self.mockClass = None
        self.mockChildren = None

    def testEssentials(self, tagName="SomeClass",
                       namespace="http://n.ethz.ch/~ggeorges/lascavesim",
                       children=None, textProperty=None):

        # do the actual decorating (only simulates decorator with arguments)
        decorator = self.xmlize(tagName, children, textProperty, namespace)
        decorated = decorator(self.mockClass)

        # check name
        self.assertTrue(hasattr(decorated, '_xml_tagName'))
        self.assertEqual(decorated._xml_tagName, tagName)

        # check namespace
        self.assertTrue(hasattr(decorated, '_xml_namespace'))
        self.assertEqual(decorated._xml_namespace, namespace)

        # check children
        self.assertTrue(hasattr(decorated, '_xml_children'))
        if children is None:
            self.assertIsNone(decorated._xml_children)
        else:
            children = [child[1] if isinstance(child, tuple) else child
                        for child in children]
            self.assertEqual(decorated._xml_children, children)

        # check text property
        self.assertTrue(hasattr(decorated, '_xml_textProperty'))
        if textProperty is None:
            self.assertIsNone(decorated._xml_textProperty)
        else:
            self.assertEqual(decorated._xml_textProperty, textProperty)

        return decorated

    def testForOnlyAddNamespaceIfNoPreExistingProp(self):
        """ Tests for a bug in xmlize
        There was an if-clause in xmlize, preventing the addition of
        the :py:attr:`_xml_namepace` attribute, if the class already has such
        a variable. As all _xml* attributes were added to
        :py:class:`XMLpersistentObject` this would lead to namespace 'None'.
        However on the above test, object() has no pre-existing namespace
        assignment, so it would not be captured by the test. """

        setattr(self.mockClass, '_xml_namespace', None)
        decorator = self.xmlize('something', namespace="test")
        decorated = decorator(self.mockClass)
        self.assertTrue(hasattr(decorated, '_xml_namespace'))
        self.assertEqual(decorated._xml_namespace, "test")

    def testInheritance(self):
        d1 = self.xmlize('something', namespace="testit",
                                textProperty="test")
        d2 = self.xmlize('something-else')
        decorated = d2(d1(self.mockClass))
        self.assertEqual(decorated._xml_namespace, 'testit')
        self.assertEqual(decorated._xml_textProperty, 'test')
        self.assertEqual(decorated._xml_tagName, 'something-else')

    def testOverwriteTagName(self):
        """ Test whether the typical call to solely overwrite the tag-name """

        # the only thing allowed to be non-default is the tag-name
        self.testEssentials('tag-name')

    def testSimpleChildSpec(self):
        """ Check a typical node that has anything but a text-property """

        decorated = self.testEssentials('tag-name', 'hoi', self.mockChildren)

        # check for child-lists and appropriate list creators
        for c in self.mockChildren:
            self.assertTrue(hasattr(decorated, c._xml_tagName))
            self.assertIsInstance(getattr(decorated, c._xml_tagName),
                                core.xml.persistence.XMLpersistentListCreator)
            self.assertEqual(getattr(decorated, c._xml_tagName).classes, (c,))

    def testComplexChildSpec(self):

        # generate child-array
        children = [('renamed', self.mockChildren[0]),
                    ('complex', self.mockChildren[1:])]

        # do the usual
        decorated = self.testEssentials('tag-name', 'hoi', children)

        # check the list-creators
        for name, c in children:
            self.assertTrue(hasattr(decorated, name))
            self.assertIsInstance(getattr(decorated, name),
                                core.xml.persistence.XMLpersistentListCreator)
            self.assertEqual(getattr(decorated, name).classes, (c,))

    def testSimpleTextPropertySpec(self):

        # the usual...
        decorated = self.testEssentials('tag-name', 'hoi', None, 'test')

        # check for the descriptor
        self.assertEqual(decorated._xml_textProperty, 'test')

    def testComplexTextPropertySpec(self):

        tprop = core.xml.parser.TextPropertyDescription('test',
                                                        ['tag1', 'tag2'])

        # the usual...
        self.testEssentials('tag-name', 'hoi', None, tprop)

    def testChildrenTextpropertyConflict(self):
        """ Calling @xmlize with a text-property and a set of children raises
            a runtime exception"""

        with self.assertRaises(RuntimeError):
            self.xmlize('sth', self.mockChildren, 'test', 'sth')

    def testTextPropertyTypeConflict(self):
        """ The textProperty can be None, a str or a text-prop. descr. """
        with self.assertRaises(TypeError):
            self.xmlize('sth', None, 12, 'sth')
            self.xmlize('sth', None, [], 'sth')

    def testLoseElementHandling(self):
        """ Lose elements must not get a list """
        self.mockChildren[0].setLoseElementContext = mock.Mock()
        self.mockChildren[2].setLoseElementContext = mock.Mock()
        decorated = self.testEssentials('test', 'hoi', self.mockChildren)
        self.assertFalse(hasattr(decorated, 'tag0'))
        self.assertTrue(hasattr(decorated, 'tag1'))
        self.assertFalse(hasattr(decorated, 'tag2'))


class TestXMLpersistentObject(unittest.TestCase):

    def testAdoptWithLoseElement(self):
        parent = core.xml.persistence.XMLpersistentObject()
        child = core.xml.persistence.XMLpersistentObject()

        # should raise an exception
        with self.assertRaises(RuntimeError):
            parent.adopt(child)

        # should now not work anymore too
        # -> instead, test the parser!
        func = mock.Mock()
        setattr(child, 'setLoseElementContext', func)
        with self.assertRaises(RuntimeError):
            parent.adopt(child)




if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
