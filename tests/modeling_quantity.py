"""
Created on Dec 27, 2012

@author: gil
"""

import mock
import unittest

import modeling.quantity
import modeling.logicalentity
import modeling.expression
import core.cas.sympy_interface


class TestAspect(unittest.TestCase):

    def setUp(self):
        cas = core.cas.sympy_interface.interface
        modeling.expression.Expression.cas = cas()

    def tearDown(self):
        modeling.expression.Expression.cas = None

    def testSymbolHandling(self):

        # create new aspect and add equations
        aspect = modeling.logicalentity.Aspect("unknown")
        aspect.adopt(modeling.expression.Equation("3*x(t)==7*t*a"))

        # x should be unknown in this case
        with self.assertRaises(KeyError):
            aspect.getSymbol('x')

        # create the symbol
        s = aspect.equation[0].getInvolvedSymbols(createUnknownSymbols=True)
        self.assertIsInstance(aspect.getSymbol('x'),
                              modeling.quantity.Variable)
        self.assertIsInstance(aspect.getSymbol('a'),
                              modeling.quantity.Constant)
        self.assertIn(aspect.getSymbol('x'), s)
        self.assertIn(aspect.getSymbol('a'), s)
        self.assertEqual(len(s), 2)

    def testPortSignalSymbolLinking(self):

        # create an aspect
        aspect = modeling.logicalentity.Aspect("unknown")
        aspect.adopt(modeling.expression.Equation("x(t)"))
        aspect.equation[0].getInvolvedSymbols(createUnknownSymbols=True)

        # create a component
        component = modeling.logicalentity.Component("something")
        component.adopt(aspect)

        # create a port and a signal
        port = modeling.ports.Port("some_port")
        signal = modeling.quantity.Flow("x")
        port.adopt(signal)
        component.adopt(port)


class TestVariable(unittest.TestCase):

    def testConstructor(self):
        self.assertEqual(modeling.quantity.Variable('x(t)').name, "x")
        self.assertEqual(modeling.quantity.Variable('x').name, "x")
        self.assertEqual(modeling.quantity.Variable('x').linkedSignals, set())

    def testIsMethods(self):
        variable = modeling.quantity.Variable('x')
        self.assertTrue(variable.isVariable())
        self.assertFalse(variable.isConstant())

    def testIsLinkedAndIsLinkedTo(self):

        # create
        signal = mock.Mock(modeling.quantity.Signal)
        signal.linkToVariable = mock.Mock()
        signal.isLinkedTo = mock.Mock(return_value=True)
        variable = modeling.quantity.Variable('x')
        self.assertFalse(variable.isLinked())
        variable.linkedSignals = set((signal,))
        othersignal = mock.Mock(modeling.quantity.Signal)

        # check linkTo methods
        self.assertTrue(variable.isLinked())
        self.assertTrue(variable.isLinkedTo(signal))

        # check negative matches
        self.assertFalse(variable.isLinkedTo(object()))
        self.assertFalse(variable.isLinkedTo(othersignal))

        # add a mis-link and check if it is being removed
        signal2 = mock.Mock(modeling.quantity.Signal)
        signal2.isLinkedTo = mock.Mock(return_value=False)
        variable.linkedSignals = set((signal, signal2))
        #self.assertFalse(variable.isLinkedTo(signal2))
        # removed the above; the code should make sure the set is correct
        self.assertEqual(variable.linkedSignals, set((signal, signal2)))

    def testLinkToSignal(self):

        # make a mock signal
        signal = mock.Mock(modeling.quantity.Signal)
        signal.linkToVariable = mock.Mock()
        signal.isLinkedTo = mock.Mock(return_value=False)
        signal.isPhysical = mock.Mock(return_value=True)

        # create a variable
        variable = modeling.quantity.Variable('x')

        # call the linker
        variable.linkToSignal(signal)
        signal.linkToVariable.assert_called_once_with(variable)
        self.assertEqual(variable.linkedSignals, set((signal,)))

        # test add another physical signal
        signal.linkToVariable = mock.Mock()
        signal.isLinkedTo = mock.Mock(return_value=False)
        signal.isPhysical = mock.Mock(return_value=True)
        with self.assertRaises(TypeError):
            variable.linkToSignal(signal)

        # test type-check
        with self.assertRaises(TypeError):
            variable.linkToSignal(object())


class TestLiveLinking(unittest.TestCase):

    def testLinking(self):

        # constructors better work...
        signal = modeling.quantity.Effort("u")
        variable = modeling.quantity.Variable("x")

        # check we have proper linking
        self.assertFalse(signal.isLinked())
        self.assertFalse(variable.isLinked())
        self.assertFalse(signal.isLinkedTo(variable))
        self.assertFalse(variable.isLinkedTo(signal))

        # run linking; causes infinite recursion in original implementation
        signal.linkToVariable(variable)

        # check we have proper linking
        self.assertTrue(signal.isLinked())
        self.assertTrue(variable.isLinked())
        self.assertTrue(signal.isLinkedTo(variable))
        self.assertTrue(variable.isLinkedTo(signal))

        # it should be possible to connect any number of outputs
        for i in xrange(4):
            signal2 = modeling.quantity.Output("v{}".format(i))
            variable.linkToSignal(signal2)
            self.assertTrue(signal2.isLinkedTo(variable))

        # in this configuration, no input or flow must be added
        with self.assertRaises(TypeError):
            for signal2 in [modeling.quantity.Input("i"),
                            modeling.quantity.Flow("i")]:
                variable.linkToSignal(signal2)

        # finally, signal may not be relinked
        variable2 = modeling.quantity.Variable("y")
        with self.assertRaises(RuntimeError):
            signal.linkToVariable(variable2)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
