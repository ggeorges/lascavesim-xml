import sys
sys.path.append('../src')
import unittest
import mock
from modeling.logicalentity import System, Component, Aspect
from modeling.expression import Equation
from modeling.quantity import Variable, Constant, Effort, Flow
from modeling.ports import PhysicalPort, PortConnection
from simulation.graphfactory import equationgraph_factory, _get_symbol_registry
import simulation.equationgraph
import common
import sympy


class EquationNodeTest(unittest.TestCase):

    def setUp(self):

        # live-equationgraph objects work better than a Mock here
        self.graph = simulation.equationgraph.EquationGraph()
        self.myclass = simulation.equationgraph.EquationNode

        # need some super-symbols
        susy = simulation.equationgraph.SuperSymbol
        self.super_symbols = [self.graph.add_supersymbol() for i in xrange(3)]

    def _make_eq(self, expr, symbol_names):
        self.symbols = zip(symbol_names, self.super_symbols)
        node = self.myclass(self.graph, expr)
        for name, super_symbol in self.symbols:
            node.add_localsymbol(name, super_symbol=super_symbol)
        return node

    def _test_eq(self, node):

        # do the substitution
        expr = str(node.universal_expression)

        # check symbol-names got correctly replaced
        for name, symbol in self.symbols:
            self.assertNotIn(name, expr)
            self.assertIn(symbol.universal_name, expr)

    def test_universal_expression(self):

        # create an equation linked to super-symbols
        node = self._make_eq("x(t)=y(t)+3*z(t)", ("x", "y", "z"))
        self._test_eq(node)

    def test_universal_expression_with_derivatives(self):

        # create an equation linked to super-symbols
        node = self._make_eq("x(t).diff(t)=y(t)+3*z(t)-2*x(t)", ("x", "y", "z"))
        dx = self.graph.add_supersymbol(differential_of=self.symbols[0][1])
        symbol = node.add_localsymbol(
            "x", super_symbol=dx, differential_order=1)
        self._test_eq(node)
        expr = str(node.universal_expression)
        self.assertNotIn("x(t).diff(t)", expr)
        self.assertNotIn("Derivative(x(t), t)", expr)
        self.assertItemsEqual(symbol.get_universal_subs(),
                              ("x(t).diff(t)", "s3(t)"))
        self.assertIn(dx.universal_name + "(t)", expr)

    def test_unfortunate_variable_name_universal_expression(self):

        # create an equation linked to super-symbols
        node = self._make_eq("s1(t)=s2(t)+3*s3(t)", ("s1", "s2", "s3"))

        # here's the problem: the symbols to replace are called s1, s2 and s3;
        # when replaced, they'll be called s0, s1 and s2; so if we start by s3
        # and replace it by s2, the new expression will read s1 = 4*s2, which
        # is of course wrong; if we however start with s1 -> s3, everything
        # will be fine

        expr = str(node.universal_expression)
        for name in "s0,s1,s2".split(","):
            self.assertIn(name, expr)
        self.assertNotIn("s3", expr)


class FunctionalEquationgraphTestCase(unittest.TestCase):

    def setUp(self):

        common.setup_cas()
        self.system = experimental_system()

    def test_equivalent_symbols(self):
        """ Make sure symbol equivalences are detected properly """

        symbol_equiv = [("battery.electric.U", "connector.electric.U_in"),
                        ("battery.electric.I", "connector.electric.I",
                         "consumer.electric.I"),
                        ("connector.electric.U_out", "consumer.electric.U")]

        # get corresponding symbols
        symbol_equiv = ((r.split(".") for r in s) for s in symbol_equiv)
        get_sym = lambda r: (self.system.component[r[0]].aspect[r[1]]
                             .symbol[r[2]])
        symbol_equiv = set(tuple(get_sym(r) for r in s) for s in symbol_equiv)

        # loop over all symbols
        aspects = (a for c in self.system.component for a in c.aspect)
        symbols = (s for a in aspects for s in a.symbol if s.isVariable())
        for symbol in symbols:

            # find if there is a known equivalence
            for equiv in symbol_equiv:
                if symbol in equiv:
                    break
            else:
                equiv = (symbol, )

            # run getEquivalentSymbols and compare results
            ans = symbol.getEquivalentSymbols()
            self.assertItemsEqual(equiv, ans)

    def test_symbol_registry(self):
        """ Make sure all symbols are registered """

        graph = simulation.equationgraph.EquationGraph()
        isolation_mode = False
        aspects = set(a for c in self.system.component for a in c.aspect)
        symbols = set(s for a in aspects for s in a.symbol if s.isVariable())
        registry = _get_symbol_registry(symbols, graph, isolation_mode)
        super_syms = set(s[0] for s in registry.itervalues())
        self.assertItemsEqual(registry.iterkeys(), symbols)
        self.assertEqual(len(super_syms), 7)

    def test_equation_extraction(self):
        """ Make sure all equations are taken into account """

        graph = equationgraph_factory(self.system).nodes
        equations = (eq.expression for c in self.system.component
                     for a in c.aspect
                     for eq in a.equation)
        equations = dict((s, 0) for s in equations)
        self.assertEqual(len(equations), len(graph))
        for eq in graph:
            try:
                equations[eq.expression] += 1
            except KeyError:
                self.fail()
        self.assertTrue(all(s == 1 for s in equations.itervalues()))

    def test_symbol_definition(self):
        """ Check the derived symbols and their casualties """

        graph = equationgraph_factory(self.system).nodes
        symbols = set(s for n in graph for s in n.symbols)
        self.assertEqual(len(symbols), 18)
        correct = {1: {"U": True, "I": False, "U_0": None},
                   2: {"U_0": None, "Q": None},
                   3: {"Q": None, "I": False},
                   4: {"U": False, "I": True, "U_1": None},
                   5: {"U_1": None, "I": True},
                   6: {"U_in": False, "U_out": True, "U_0": None, "I": True},
                   7: {"U_0": None, "I": True}}
        for node in graph:
            comp = eq_id_lookup[node.expression]
            for symbol in node.symbols:
                symb = symbol.name
                if not comp in correct:
                    raise NotImplementedError(comp)
                msg = ("symbol {} of {} should have dependence '{}', "
                       "yet is_dependent returned '{}'")
                msg = msg.format(symb,
                                 str(node.expression),
                                 correct[comp][symb],
                                 symbol.dependent)
                self.assertEqual(symbol.dependent,
                                 correct[comp][symb], msg)

    def test_graph_direction(self):
        graph = equationgraph_factory(self.system)
        graph.condition()
        syms = (s for n in graph.nodes for s in n.symbols)
        syms = (s for s in syms if s.dependent is None)
        self.assertEqual(len(set(syms)), 0)
        self.assertFalse(graph.has_undetermined_causalities(),
                         "at least one causality could not be determined")


eq_id_lookup = {}


def experimental_system():
    # build a 2-component test system
    system = System('test')

    # the battery
    battery = add_component(system, 'battery')
    add_aspect(battery, 'electric', {1: 'U(t)=U_0(t)-R_i*I(t)',
                                     2: 'U_0(t)=C*Q(t)',
                                     3: 'Q(t).diff(t)=I(t)'},
               ('U', 'I', 'U_0', 'Q'), ('R_i', 'C'))
    p1 = add_electrical_port(battery, 'electric', 'U', 'I', soe=True)

    # some intermediate
    connector = add_component(system, 'connector')
    add_aspect(connector, 'electric', {6: 'U_out(t)=U_in(t)-U_0(t)-R_i*I(t)',
                                       7: 'U_0(t)=R_ii*I(t)'},
               ('U_in', 'U_out', 'I', 'U_0'), ('R_i', 'R_ii'))
    p3 = add_electrical_port(
        connector, 'electric', 'U_in', 'I', soe=False, name="in")
    p4 = add_electrical_port(
        connector, 'electric', 'U_out', 'I', soe=True, name="out")

    # the consumer
    consumer = add_component(system, 'consumer')
    add_aspect(consumer, 'electric', {4: 'U(t)=R_i*I(t)+U_1(t)',
                                      5: 'U_1(t)=R_ii*I(t)'},
               ('U', 'I', 'U_1'), ('R_i', 'R_ii'))
    p2 = add_electrical_port(consumer, 'electric', 'U', 'I', soe=False)

    # connect them together
    connect_ports(p1, p3)
    connect_ports(p4, p2)

    return system


def add_component(system, name):
    component = Component(name)
    system.adopt(component)
    return component


def add_aspect(component, name, equations, variables, constants):

    aspect = Aspect(name)
    component.adopt(aspect)

    for _id, expr in equations.iteritems():
        eq = Equation(expr)
        eq_id_lookup[eq.expression] = _id
        aspect.adopt(eq)

    for s in variables:
        aspect.adopt(Variable(s))
    for s in constants:
        aspect.adopt(Constant(s))
    return aspect


def add_electrical_port(component, aspect, symbol_U, symbol_I, soe=False,
                        name="electric"):
    port = PhysicalPort(name)
    port.adopt(Effort('U', causality="source" if soe else "sink"))
    port.adopt(Flow('I', causality="sink" if soe else "source"))
    aspect = component.aspect[aspect]
    symbol_U, symbol_I = tuple(aspect.symbol[s] for s in (symbol_U, symbol_I))
    port.signal['U'].linkToVariable(symbol_U)
    port.signal['I'].linkToVariable(symbol_I)
    component.adopt(port)
    return port


def connect_ports(*ports):
    connection = PortConnection()
    for port in ports:
        connection.connectPort(port)
