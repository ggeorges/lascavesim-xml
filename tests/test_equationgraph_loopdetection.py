import sys
sys.path.append('../src')
import unittest
import mock
import logging
logger = logging.getLogger(__name__)
from simulation.equationgraph import EquationGraph
from simulation.equationgraph.localsymbol import LocalSymbol
from simulation.equationgraph.supersymbol import SuperSymbol
from simulation.equationgraph.tools import convert_to_networkx_graph
from simulation.equationgraph.common import EquationGraphError

import core.cas.sympy_interface
_cas = core.cas.sympy_interface.interface()
import random
import loopy_equationgraph

from simulation.equationgraph.algebraicloop import (
    remove_loops, LoopBuster, GraphLoop)


class GraphLoopCausalLoopDetectionTestCase(unittest.TestCase):

    def setUp(self):

        # no source, no external equations
        eq = mock.Mock()
        eq.is_source = mock.Mock(return_value=False)
        eq.is_super_symbol = mock.Mock(return_value=False)
        mocks = [eq]
        for i in xrange(5):
            m = mock.Mock()
            mocks.append(m)
            m.is_source = mock.Mock(return_value=False)
            m.is_super_symbol = mock.Mock(return_value=True)
            sub_sym = mock.Mock()
            sub_sym.equation = mocks[0]
            m.get_dependent_symbol = mock.Mock(return_value=sub_sym)
        self.mocks = mocks
        self.graph = GraphLoop(self.mocks)

    def test_setUp(self):
        self.assertTrue(all(m.is_source() is False for m in self.mocks))
        self.assertTrue(all(m.is_super_symbol() for m in self.mocks[1:]))

    def test_negative(self):
        self.assertFalse(self.graph.has_sources())
        self.assertFalse(self.graph.has_external_dependent_equations())

    def test_positive_via_source(self):

        # now we make one random mock a source
        random.choice(self.mocks[1:]).is_source = mock.Mock(return_value=True)
        self.assertTrue(self.graph.has_sources())
        self.assertFalse(self.graph.has_external_dependent_equations())

    def test_positive_via_external_node(self):

        # make one dependent on an exterior node
        ext = mock.Mock()
        m = random.choice(self.mocks[1:])
        m.get_dependent_symbol = mock.Mock(return_value=ext)
        self.assertFalse(self.graph.has_sources())
        self.assertTrue(self.graph.has_external_dependent_equations())


class GraphLoopHasIntersectionWithTestCase(unittest.TestCase):

    def setUp(self):
        self.symbols = [mock.Mock() for i in xrange(10)]

    def test_positive(self):

        # there is an intersection
        g1 = GraphLoop(self.symbols[:4])
        g2 = GraphLoop(self.symbols[2:])
        self.assertTrue(g1.has_intersection_with(g2))

    def test_negative(self):

        # there is no intersection
        g1 = GraphLoop(self.symbols[:2])
        g2 = GraphLoop(self.symbols[1:])
        self.assertFalse(g1.has_intersection_with(g2))


func = "simulation.equationgraph.algebraicloop.LoopBuster._iter_cycles"


@mock.patch.object(LoopBuster, '_iter_cycles', spec=mock.Mock())
class LoopBusterInit(unittest.TestCase):

    def setUp(self):

        self.cycles = []
        for i in xrange(8):
            cycle = mock.Mock()
            cycle.has_sources = mock.Mock(return_value=False)
            fct = mock.Mock(return_value=False)
            cycle.has_external_dependent_equations = fct
            cycle.has_intersection_with = mock.Mock(return_value=False)
            self.cycles.append(cycle)
        self.iterator = (s for s in self.cycles)
        self.buster = LoopBuster

    def test_no_intersections_all_causal(self, mockiter):
        mockiter.return_value = self.iterator

        buster = self.buster(mock.Mock)
        self.assertItemsEqual(buster.loops, self.cycles)

    def test_one_causal_by_source(self, mockiter):
        mockiter.return_value = self.iterator

        for i in xrange(random.randint(1, 3)):
            m = random.choice(self.cycles)
            m.has_sources = mock.Mock(return_value=True)
            self.cycles.remove(m)
        buster = self.buster(mock.Mock)
        self.assertItemsEqual(buster.loops, self.cycles)

    def test_some_intersections(self, mockiter):
        mockiter.return_value = self.iterator

        for i in xrange(random.randint(1, 3)):
            m = random.choice(self.cycles)
            m.has_intersection_with = mock.Mock(return_value=True)
            self.cycles.remove(m)
        buster = self.buster(mock.Mock)
        self.assertItemsEqual(buster.loops, self.cycles)


class FunctionalTestOnExample1(unittest.TestCase):

    def setUp(self):

        self.graph = None
        self.symbols = None
        self.equations = None
        self.loops = None
        loopy_equationgraph.Example1().copy_vars(self)

        self.buster = LoopBuster(self.graph)

    def test_loop_detection(self):
        for loop in self.buster.loops:
            for known_loop in self.loops:
                if loop.has_intersection_with(known_loop):
                    self.assertItemsEqual(known_loop, loop)
                    break
            else:
                raise RuntimeError()

    def test_loop_removal(self):
        remove_loops(self.graph)
        self.assertTrue(self.graph.is_conditioned())
        buster = LoopBuster(self.graph)
        self.assertFalse(buster.loops)
