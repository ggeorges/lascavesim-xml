import sys
sys.path.append('../src')
import unittest
import logging
logger = logging.getLogger(__name__)
from simulation.equationgraph import EquationGraph
from simulation.equationgraph.localsymbol import LocalSymbol
from simulation.equationgraph.tools import convert_to_networkx_graph
from simulation.equationgraph.common import EquationGraphError
from simulation.equationgraph.conditioner import clean_graph
import common


class FunctionalEquationgraphTestCase(unittest.TestCase):

    def setUp(self):

        graph = EquationGraph()
        U_0, Q, U, I = tuple(graph.add_supersymbol() for i in xrange(4))
        dQ = graph.add_supersymbol(differential_of=Q)
        S = lambda *args, **kwargs: LocalSymbol(graph, *args, **kwargs)

        e1 = graph.add_equation("U_0(t) = f(Q(t))",
                                S("U_0", dependent=True, super_symbol=U_0),
                                S("Q", super_symbol=Q))

        e2 = graph.add_equation("U(t) = U_0(t)-R_i*I(t)",
                                S("U", dependent=True, super_symbol=U),
                                S("U_0", super_symbol=U_0),
                                S("I", super_symbol=I))

        e3 = graph.add_equation("Q(t).diff(t) = f(Q(t))*I(t)",
                                S("Q", dependent=True, super_symbol=dQ),
                                S("Q", super_symbol=Q),
                                S("I", super_symbol=I, negative=True))

        e4 = graph.add_equation("U(t)=R*I(t)")
        e4.add_localsymbol("I", dependent=True, super_symbol=I)
        e4.add_localsymbol("U", super_symbol=U)

        self.graph = graph
        self.symbols = (U_0, Q, U, I, dQ)
        self.derivatives = (dQ, )
        self.integrals = (Q, )
        self.equations = (e1, e2, e3, e4)

    def test_is_source(self):

        syms = [s for s in self.graph.super_symbols if s.is_source()]
        self.assertItemsEqual(self.integrals, syms)

    def test_differential_symbols(self):

        # check integrals are found
        self.assertTrue(all(s.is_differential()) for s in self.derivatives)

        # get the integral terms and check them
        syms = [s.integral for s in self.derivatives]
        self.assertItemsEqual(syms, self.integrals)

        # check integrals are found
        self.assertTrue(all(s.has_differential()) for s in self.integrals)

        # go back to the derivatives, over the differential property
        syms = [s.differential for s in self.integrals]
        self.assertItemsEqual(syms, self.derivatives)

    def test_reference_counts(self):

        # there is one ref within getrefcount, 1 is from the for loopp and then
        # there can be always exactly one more for the symbols/equations array
        # the der. and int. arrays are deleted to make this simpler
        del self.derivatives
        del self.integrals
        for symbol in self.symbols:
            self.assertEqual(sys.getrefcount(symbol),  3 + 1)
        for equation in self.equations:
            self.assertEqual(sys.getrefcount(equation),  3 + 1)
            for symbol in equation.symbols:
                self.assertEqual(sys.getrefcount(symbol),  2 + 1)

        # the graph must be linked to "weakly"
        self.assertEqual(sys.getrefcount(self.graph), 2)
