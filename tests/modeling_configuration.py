'''
Created on 19.12.2012

@author: ggeorges
'''
import unittest
import modeling.configuration
import modeling.expression
import modeling.logicalentity
import modeling.ports
import modeling.quantity


class TestLascavesim(unittest.TestCase):

    def testConstructor(self):

        # run empty
        app = modeling.configuration.Lascavesim()

        # check child-lists were created
        self.assertTrue(hasattr(app, 'library'))
        self.assertTrue(hasattr(app.library, 'append'))
        self.assertTrue(hasattr(app, 'system'))
        self.assertTrue(hasattr(app.system, 'append'))


class TestLibrary(unittest.TestCase):

    def testConstructor(self):

        # argument must be string
        with self.assertRaises(RuntimeError):
            modeling.configuration.Library("test", 123)

        # directory must exist
        with self.assertRaises(RuntimeError):
            modeling.configuration.Library("test", "/does-not-exist")

        lib = modeling.configuration.Library("test", './')

        # check properties got set allright
        self.assertEqual(lib.name, 'test')
        self.assertEqual(lib.path, './')


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testCreateDocument']
    unittest.main()
