import sys
sys.path.append('../src')
import unittest
import logging
logger = logging.getLogger(__name__)
import loopy_equationgraph
from simulation.equationgraph import (EquationGraph, LocalSymbol,
                                      EquationGraphError)
from simulation.equationgraph.conditioner import clean_graph
import common


class NonCausalLoopTestCase(unittest.TestCase):

    def setUp(self):
        self.graph = None
        self.symbols = None
        self.equations = None

        loopy_equationgraph.Example1().copy_vars(self)

    def test_graph(self):

        # sink and sources
        self.assertTrue(self.symbols[0].is_source())
        self.assertTrue(self.symbols[-1].is_source())

        # everything must be determined
        symbols = (s for n in self.graph.nodes for s in n.symbols)
        self.assertFalse(any(s.dependent is None for s in symbols))

    def test_full_conditioning(self):

        # remove any causality information
        symbols = set(s for n in self.graph.nodes for s in n.symbols)
        for s in symbols:
            s._dependent = None
            self.assertIsNone(s.dependent)

        self._test_condition(self.graph)

    def _test_condition(self, graph):

        # the call
        graph.condition()

        # all must be determined
        symbols = (s for n in graph.nodes for s in n.symbols)
        self.assertFalse(any(s.dependent is None for s in symbols))

        # all nodes must have exactly one (or zero if they're a source) dep. s.
        nodes = list(graph.nodes) + list(graph.super_symbols)
        for node in nodes:
            self.assertFalse(any(s.dependent is None for s in node.symbols))
            count = sum(bool(s.dependent) for s in node.symbols)
            msg = "{} has {} dependent symbols but should have "
            msg = msg.format(str(node), count)
            if node.is_source():
                self.assertEqual(count, 0, msg + "0")
            else:
                self.assertEqual(count, 1, msg + "1")
            if node.is_equation():
                self.assertIsNotNone(node.get_dependent_symbol())

    def test_clean_graph(self):

        # make sure we're safe to begin with
        self.assertTrue(all(sum(s.dependent for s in n.symbols) <= 1
                        for n in self.graph.nodes))

        # destroy the graph at random
        import random
        symbols = set(s for n in self.graph.nodes for s in n.symbols)
        for s in symbols:
            s.dependent = (random.random() > 0.5)

        # make sure it was destroyed
        self.assertFalse(all(sum(bool(s.dependent) for s in n.symbols) <= 1
                             for n in self.graph.nodes))
        self.assertFalse(any(s.dependent is None for s in symbols))

        # clean_graph should remove any errors, replacing them with 'None'
        clean_graph(self.graph)

        # ensure there were corrections
        self.assertTrue(any(s.dependent is None for s in symbols))
        self.assertTrue(all(sum(bool(s.dependent) for s in n.symbols) <= 1
                        for n in self.graph.nodes))


class deactivated(object):

    def test_as_it_should_be(self):
        doc = common.parseFile('../as_it_should_be.xml')
        graph = common.equationgraph_factory(doc.system[0])

        # get differential symbols
        diff = set(s for s in graph.super_symbols if s.is_differential())
        self.assertEqual(len(diff), 4)

        intg = set(s.integral for s in diff)

        self._test_condition(graph)

    def test_remove_loops(self):
        loops = list(self.graph.iter_causal_loops())
        self.assertTrue(loops)
        self.graph.remove_loops()
        loops = list(self.graph.iter_causal_loops())
        self.assertFalse(loops)
